//
//  CategoryModel.swift
//  LaqueueCutomer
//
//  Created by Adityaraj Singh Gaharwar on 05/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

// MARK: - CategoryModel
struct CategoryModel: Codable {
    let version: Version?
    let status, errorcode: Int?
    let message: String?
    let data: [CategoryData]?
}

// MARK: - CategoryData
struct CategoryData: Codable {
    let catID, catName: String?

    enum CodingKeys: String, CodingKey {
        case catID = "cat_id"
        case catName = "cat_name"
    }
}
