//
//  PrefrencesModel.swift
//  LaqueueRetailer
//
//  Created by Adityaraj Singh Gaharwar on 02/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

// MARK: - PrefrencesModel
struct PrefrencesModel: Codable {
    let version: Version?
    let status, errorcode: Int?
    let data: PrefrencesData?
    let message: String?
}

// MARK: - PrefrencesData
struct PrefrencesData: Codable {
    let prefrence: Prefrence?
}

// MARK: - Prefrence
struct Prefrence: Codable {
    let prefreID, ownerRcvNotifi, ownerRcvNotifiQueStart, empRcvNotifiQueStart: String?
    let pushNotification, acknowledge, askUpdate, cancel: String?
    let noshow: String?

    enum CodingKeys: String, CodingKey {
        case prefreID = "prefre_id"
        case ownerRcvNotifi = "owner_rcv_notifi"
        case ownerRcvNotifiQueStart = "owner_rcv_notifi_que_start"
        case empRcvNotifiQueStart = "emp_rcv_notifi_que_start"
        case pushNotification = "push_notification"
        case acknowledge
        case askUpdate = "ask_update"
        case cancel, noshow
    }
}
