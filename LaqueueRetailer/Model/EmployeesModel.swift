//
//  EmployeesModel.swift
//  LaqueueRetailer
//
//  Created by Adityaraj Singh Gaharwar on 06/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation


// MARK: - EmployeesModel
struct EmployeesModel: Codable {
    let version: Version?
    let status: Int?
    let message: String?
    let errorcode: Int?
    let data: [EmployeesData]?
}

// MARK: - EmployeesData
struct EmployeesData: Codable {
    let totalcount: Int?
    let employeeList: [EmployeeList]?

    enum CodingKeys: String, CodingKey {
        case totalcount
        case employeeList = "employee_list"
    }
}

// MARK: - EmployeeList
struct EmployeeList: Codable {
    let userID, firstname, lastname, userEmail: String?
    let userPic, userRole, userPhone: String?
    let businessCode, status, empID, countryCode: String?
    var isSelected:Bool?
    
    enum CodingKeys: String, CodingKey {
        case userID = "user_id"
        case empID = "emp_id"
        case userPhone = "user_phone"
        case firstname, lastname, status
        case userEmail = "user_email"
        case userPic = "user_pic"
        case userRole = "user_role"
        case businessCode = "busi_code"
        case countryCode = "country_code"
        case isSelected
    }
}

