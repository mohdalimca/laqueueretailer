//
//  EmployeeDetailModel.swift
//  LaqueueRetailer
//
//  Created by Adityaraj Singh Gaharwar on 16/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

// MARK: - EmployeeDetailModel
struct EmployeeDetailModel: Codable {
    let version: Version?
    let status: Int?
    let message: String?
    let errorcode: Int?
    let data: EmployeeDetailData?
}

// MARK: - EmployeeDetailData
struct EmployeeDetailData: Codable {
    let employeeDetail: EmployeeDetail?

    enum CodingKeys: String, CodingKey {
        case employeeDetail = "employee_detail"
    }
}

// MARK: - EmployeeDetail
struct EmployeeDetail: Codable {
    let id, firstname, userRole, lastname: String?
    let userEmail, userPhone, countryCode, dob: String?
    let languages: String?
    let userPic: String?
    let queueInfo: [QueueInfo]?

    enum CodingKeys: String, CodingKey {
        case id, firstname
        case userRole = "user_role"
        case lastname
        case userEmail = "user_email"
        case userPhone = "user_phone"
        case countryCode = "country_code"
        case dob, languages
        case userPic = "user_pic"
        case queueInfo = "queue_info"
    }
}

// MARK: - QueueInfo
struct QueueInfo: Codable {
    let queID, queName: String?

    enum CodingKeys: String, CodingKey {
        case queID = "que_id"
        case queName = "que_name"
    }
}
