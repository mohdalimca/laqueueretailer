//
//  QueueListModel.swift
//  LaqueueRetailer
//
//  Created by Adityaraj Singh Gaharwar on 17/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation


// MARK: - QueueListModel
struct QueueListModel: Codable {
    let version: Version?
    let status, errorcode: Int?
    let message: String?
    let data: [QueueListData]?
}

// MARK: - QueueListData
struct QueueListData: Codable {
    let totalcount: String?
    let myqueue: [Myqueue]?
}

// MARK: - Myqueue
struct Myqueue: Codable {
    let queID, queName, queDescription, queImg: String?
    let empInfo: [EmpInfo]?
    let numbering, numberPrefix, numberStart, capacity: String?
    let callTimer, callNextPersons, callNextPlaces, acknowledgeMsg: String?
    let askupdateMsg, callMsg, noshowMsg, status: String?
    let createdOn: String?
    let schedule, customDays, offDays: [CustomDay]?

    enum CodingKeys: String, CodingKey {
        case queID = "que_id"
        case queName = "que_name"
        case queDescription = "que_description"
        case queImg = "que_img"
        case empInfo = "emp_info"
        case numbering
        case numberPrefix = "number_prefix"
        case numberStart = "number_start"
        case capacity
        case callTimer = "call_timer"
        case callNextPersons = "call_next_persons"
        case callNextPlaces = "call_next_places"
        case acknowledgeMsg = "acknowledge_msg"
        case askupdateMsg = "askupdate_msg"
        case callMsg = "call_msg"
        case noshowMsg = "noshow_msg"
        case status
        case createdOn = "created_on"
        case schedule
        case customDays = "custom_days"
        case offDays = "off_days"
    }
}

// MARK: - CustomDay
struct CustomDay: Codable {
    let slotID, isOff, days: String?
    let oneTo, oneFrom, twoTo, twoFrom: String?
    let isCustom: String?
    let customDate: String?

    enum CodingKeys: String, CodingKey {
        case slotID = "slot_id"
        case isOff = "is_off"
        case days
        case oneTo = "one_to"
        case oneFrom = "one_from"
        case twoTo = "two_to"
        case twoFrom = "two_from"
        case isCustom = "is_custom"
        case customDate = "custom_date"
    }
}

enum CustomDate: String, Codable {
    case empty = ""
    case the101020201130Pm = "10-10-2020 11:30 PM"
    case the15102020 = "15-10-2020"
}

enum OneFrom: String, Codable {
    case empty = ""
    case the1130Pm = "11:30 PM"
}

// MARK: - EmpInfo
struct EmpInfo: Codable {
    let empID: String?
    let empName: String?

    enum CodingKeys: String, CodingKey {
        case empID = "emp_id"
        case empName = "emp_name"
    }
}
