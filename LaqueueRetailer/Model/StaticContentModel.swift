//
//  StaticContentModel.swift
//  LaqueueRetailer
//
//  Created by Adityaraj Singh Gaharwar on 30/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

// MARK: - StaticContentModel
struct StaticContentModel: Codable {
    let status, errorcode: Int?
    let message: String?
    let data: StaticContentData?
}

// MARK: - StaticContentData
struct StaticContentData: Codable {
    let url: String?
}
