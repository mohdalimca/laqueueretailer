//
//  ResendOTPModel.swift
//  LaqueueCutomer
//
//  Created by Adityaraj Singh Gaharwar on 02/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

// MARK: - ResendOTPModel
struct ResendOTPModel: Codable {
    let version: Version?
    let status, errorcode: Int?
    let data: ResendOTPData?
    let message: String?
}

// MARK: - ResendOTPData
struct ResendOTPData: Codable {
    let resendOtp: ResendOtp?

    enum CodingKeys: String, CodingKey {
        case resendOtp = "resend_otp"
    }
}

// MARK: - ResendOtp
struct ResendOtp: Codable {
    let userID, otp: String?

    enum CodingKeys: String, CodingKey {
        case userID = "user_id"
        case otp
    }
}
