//
//  SignUpModel.swift
//  LaqueueRetailer
//
//  Created by Adityaraj Singh Gaharwar on 30/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

// MARK: - SignUpModel
struct SignUpModel: Codable {
    let version: Version?
    let status, errorcode: Int?
    let message: String?
    let data: SignUpData?
}

// MARK: - SignUpData
struct SignUpData: Codable {
    let register: Register?
}

// MARK: - Register
struct Register: Codable {
    let userID, otp, busiCode: String?

    enum CodingKeys: String, CodingKey {
        case userID = "user_id"
        case otp
        case busiCode = "busi_code"
    }
}
