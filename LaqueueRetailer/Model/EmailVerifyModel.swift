//
//  EmailVerifyModel.swift
//  LaqueueRetailer
//
//  Created by Adityaraj Singh Gaharwar on 30/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

// MARK: - EmailVerifyModel
struct EmailVerifyModel: Codable {
    let version: Version?
    let status: Int?
    let message: String?
    let errorcode: Int?
    let data: EmailVerifyData?
}

// MARK: - EmailVerifyData
struct EmailVerifyData: Codable {
    let otp: Otp?
}

// MARK: - Otp
struct Otp: Codable {
    let userID: String?

    enum CodingKeys: String, CodingKey {
        case userID = "user_id"
    }
}
