//
//  QueueBookingsModel.swift
//  LaqueueRetailer
//
//  Created by Adityaraj Singh Gaharwar on 04/09/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

// MARK: - QueueBookingsModel
struct QueueBookingsModel: Codable {
    let version: Version?
    let status, errorcode: Int?
    let message: String?
    let data: [QueueBookingsData]?
}

// MARK: - QueueBookingsData
struct QueueBookingsData: Codable {
    let totalcount: String?
    let record: [Record]?
}

// MARK: - Record
struct Record: Codable {
    let queName, callTimer, totalQueueNo, userID: String?
    let firstname, lastname: String?
    let userPic: String?
    let bookingID, queID, queSlotTime, message: String?
    let status, people, bookingTime, queueDescription: String?
    let lastStatusUpdate, waitingTime: String?

    enum CodingKeys: String, CodingKey {
        case queName = "que_name"
        case callTimer = "call_timer"
        case totalQueueNo = "total_queue_no"
        case userID = "user_id"
        case firstname, lastname
        case userPic = "user_pic"
        case bookingID = "booking_id"
        case queID = "que_id"
        case queSlotTime = "que_slot_time"
        case message, status, people
        case bookingTime = "booking_time"
        case queueDescription = "que_description"
        case lastStatusUpdate = "last_status_update"
        case waitingTime = "waiting_time"
    }
}
