//
//  QueuePlaceModel.swift
//  LaqueueCutomer
//
//  Created by Adityaraj Singh Gaharwar on 30/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

// MARK: - QueuePlaceModel
struct QueuePlaceModel: Codable {
    let version: Version?
    let status, errorcode: Int?
    let message: String?
    let data: QueuePlaceData?
}

// MARK: - QueuePlaceData
struct QueuePlaceData: Codable {
    let queName, callTimer, totalQueueNo, userID: String?
    let firstname, lastname: String?
    let userPic: String?
    let bookingID, queID, queSlotTime, message: String?
    let status, people, bookingTime: String?
    let businessId, businessName: String?
    let lastStatusUpdate, waitingTime: String?

    enum CodingKeys: String, CodingKey {
        case queName = "que_name"
        case callTimer = "call_timer"
        case totalQueueNo = "total_queue_no"
        case userID = "user_id"
        case firstname, lastname
        case userPic = "user_pic"
        case bookingID = "booking_id"
        case queID = "que_id"
        case queSlotTime = "que_slot_time"
        case message, status, people
        case bookingTime = "booking_time"
        case businessId = "busi_id"
        case businessName = "busi_name"
        case lastStatusUpdate = "last_status_update"
        case waitingTime = "waiting_time"
    }
}

enum RefreshOption: String {
    case top
    case bottom
}
