//
//  LoginModel.swift
//  LaqueueRetailer
//
//  Created by Adityaraj Singh Gaharwar on 30/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

// MARK: - LoginModel
struct LoginModel: Codable {
    let version: Version?
    let status, errorcode: Int?
    let message: String?
    let data: LoginData?
}

// MARK: - LoginData
struct LoginData: Codable {
    let login: Login?
}

// MARK: - Login
struct Login: Codable {
    let authorization, userid, busiCode, userEmail, firstname: String?
    let lastname, busid: String?
    var userStatus: Int?
    let userRole: String?
    let userPic: String?
    var language:String?
    let otp:JSONAny?

    enum CodingKeys: String, CodingKey {
        case authorization = "Authorization"
        case busiCode = "busi_code"
        case busid = "busi_id"
        case userid = "user_id"
        case userEmail = "user_email"
        case firstname, lastname
        case userStatus = "user_status"
        case userRole = "user_role"
        case userPic = "user_pic"
        case otp = "otp"
        case language
    }
}

// MARK: - Version
struct Version: Codable {
    let status, versioncode: Int?
    let versionmessage, currentVersion: String?

    enum CodingKeys: String, CodingKey {
        case status, versioncode, versionmessage
        case currentVersion = "current_version"
    }
}
