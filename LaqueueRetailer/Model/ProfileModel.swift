//
//  ProfileModel.swift
//  LaqueueRetailer
//
//  Created by Adityaraj Singh Gaharwar on 05/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

enum ProfileUpdateType: String {
    case normal = "0"
    case business = "1"
}

// MARK: - ProfileModel
struct ProfileModel: Codable {
    let version: Version?
    let status, errorcode: Int?
    let data: ProfileData?
    let message: String?
}

// MARK: - ProfileData
struct ProfileData: Codable {
    let profile: Profile?
}

// MARK: - Profile
struct Profile: Codable {
    let id, userEmail, firstname, lastname: String?
    let countryCode, userPhone, userRole, busiID: String?
    let busiName, busiDescription, busiWaitingTime, busiCode: String?
    let busiAddress, busiLat, busiLng, busiType: String?
    let busiImg: String?
    let country, city, adminStatus: String?
    let userPic: String?
    let categaoryID, categoryName:String?

    enum CodingKeys: String, CodingKey {
        case id
        case userEmail = "user_email"
        case firstname, lastname
        case countryCode = "country_code"
        case userPhone = "user_phone"
        case userRole = "user_role"
        case busiID = "busi_id"
        case busiName = "busi_name"
        case busiDescription = "busi_description"
        case busiWaitingTime = "busi_waiting_time"
        case busiCode = "busi_code"
        case busiAddress = "busi_address"
        case busiLat = "busi_lat"
        case busiLng = "busi_lng"
        case busiType = "busi_type"
        case busiImg = "busi_img"
        case country, city
        case adminStatus = "admin_status"
        case userPic = "user_pic"
        case categaoryID = "cat_id"
        case categoryName = "cat_name"
    }
}
