//
//  NotificationModel.swift
//  LaqueueRetailer
//
//  Created by Apple on 24/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

struct NotificationModel: Codable {
    let version: Version?
    let status: Int?
    let message: String?
    let errorcode: Int?
    let data: [NotificationData]?
}

// MARK: - Datum
struct NotificationData: Codable {
    let totalcount: String?
    let notification: [NotificationList]?
}

// MARK: - Notification
struct NotificationList: Codable {
    let id, notificationText, firstname, otheruserid: String?
    let userPic: String?
    let queID, purposeID, read, createdOn: String?
    let queName: String?
    var bookingID: String?

    enum CodingKeys: String, CodingKey {
        case id
        case notificationText = "notification_text"
        case firstname, otheruserid
        case userPic = "user_pic"
        case queID = "que_id"
        case purposeID = "purpose_id"
        case read
        case createdOn = "created_on"
        case queName = "que_name"
        case bookingID = "booking_id"
    }
}

// MARK: - Version
//struct Version: Codable {
//    let status, versioncode: Int?
//    let versionmessage, currentVersion: String?
//
//    enum CodingKeys: String, CodingKey {
//        case status, versioncode, versionmessage
//        case currentVersion = "current_version"
//    }
//}
