//
//  ForgotPasswordModel.swift
//  LaqueueRetailer
//
//  Created by Adityaraj Singh Gaharwar on 30/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

// MARK: - ForgotPasswordModel
struct ForgotPasswordModel: Codable {
    let version: Version?
    let status, errorcode: Int?
    let data: ForgotPasswordData?
    let message: String?
}

// MARK: - ForgotPasswordData
struct ForgotPasswordData: Codable {
    let forgot: Forgot?
}

// MARK: - Forgot
struct Forgot: Codable {
    let userID, otp: String?

    enum CodingKeys: String, CodingKey {
        case userID = "user_id"
        case otp
    }
}
