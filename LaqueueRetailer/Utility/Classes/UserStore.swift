//
//  UserStore.swift
//  Clustry
//
//  Created by cis on 14/11/2019.
//  Copyright © 2019 cis. All rights reserved.
//

import Foundation

struct UserStore {
    private static let authorizationKey = "authorization"
    private static let idkey = "api_id_key"
    private static let pushTokenKey = "push_token"
    private static let fcmtoken_key = "fcmtoken"
    private static let userIdKey = "user_id"
    private static let emailKey = "email_id"
    private static let userNameKey = "user_name"
    private static let userImageKey = "user_image"
    private static let userRoleKey = "user_role"
    private static let busiCodeKey = "business_code"
    private static let businessIDKey = "business_id"
    private static let ownerDetailsKey = "owner_details"
    private static let retailDetailsKey = "retail_details"
    private static let queueKey = "queue_added"
    private static let profileStatusKey = "profileStatus"
    private static let retailerTypeKey = "retail_type"
    private static let numberOfQueuesKey = "no_queues"
    private static let userPicKey = "user_pic"
    private static let businessPicKey = "business_pic"
    
    static var authorization: String? {
        return UserDefaults.standard.string(forKey: authorizationKey)
    }
    
    static var userPic: String? {
        return UserDefaults.standard.string(forKey: userPicKey)
    }
    
    static var businessPic: String? {
        return UserDefaults.standard.string(forKey: businessPicKey)
    }
    
    static var numOfQueues: Int? {
        return UserDefaults.standard.integer(forKey: numberOfQueuesKey)
    }
    
    static var fcmtoken: String? {
        return UserDefaults.standard.string(forKey: fcmtoken_key)
    }
    
    static var userRole: String? {
        return UserDefaults.standard.string(forKey: userRoleKey)
    }
    
    static var id:String? {
        return  UserDefaults.standard.string(forKey: idkey)
    }
    
    static var username:String? {
        return  UserDefaults.standard.string(forKey: userNameKey)
    }
    
    static var pushToken:String? {
        return UserDefaults.standard.string(forKey: pushTokenKey)
    }
    
    static var userID:String? {
        return UserDefaults.standard.string(forKey: userIdKey)
    }
    
    static var email:String? {
        return UserDefaults.standard.string(forKey: emailKey)
    }
    
    static var userImage:String? {
        return UserDefaults.standard.string(forKey: userImageKey)
    }
    
    static var businessId:String? {
        return UserDefaults.standard.string(forKey: businessIDKey)
    }
    
    static var businessCode:String? {
        return UserDefaults.standard.string(forKey: busiCodeKey)
    }
    
    static var ownerDetails:Int? {
        return UserDefaults.standard.integer(forKey: ownerDetailsKey)
    }
    
    static var retailDetails:Int? {
        return UserDefaults.standard.integer(forKey: retailDetailsKey)
    }
    
    static var queueAdded:String? {
        return UserDefaults.standard.string(forKey: queueKey)
    }
    
    static var profileStatus:Int? {
        return UserDefaults.standard.integer(forKey: profileStatusKey)
    }
    
    static var retailerType:String? {
        return UserDefaults.standard.string(forKey: retailerTypeKey)
    }
    
    static func save(noQueue:Int) {
        UserDefaults.standard.set(noQueue, forKey: numberOfQueuesKey)
    }
    
    static func save(fcmtoken:String) {
        UserDefaults.standard.set(fcmtoken, forKey: fcmtoken_key)
    }
    
    static func save(authorization:String?) {
        UserDefaults.standard.set(authorization, forKey: authorizationKey)
    }
    
    static func save(id:String?) {
        UserDefaults.standard.set(id, forKey:idkey)
    }
    
    static func save(username:String) {
        UserDefaults.standard.set(username, forKey:userNameKey)
    }
    
    static func save(userImage:String) {
        UserDefaults.standard.set(userImage, forKey:userImageKey)
    }
    
    static func save(pushToken:String) {
        UserDefaults.standard.set(pushToken, forKey: pushTokenKey)
    }
    
    static func save(userID:String?) {
        UserDefaults.standard.set(userID, forKey: userIdKey)
    }
    
    static func save(email:String) {
        UserDefaults.standard.set(email, forKey: emailKey)
    }
    
    static func save(role:String) {
        UserDefaults.standard.set(role, forKey: userRoleKey)
    }
    
    static func save(businessId:String?) {
        UserDefaults.standard.set(businessId, forKey: businessIDKey)
    }
    
    static func save(businessCode:String?) {
        UserDefaults.standard.set(businessCode, forKey: busiCodeKey)
    }
    
    static func save(userPic:String?) {
        UserDefaults.standard.set(userPic, forKey: userPicKey)
    }
    
    static func save(businessPic:String?) {
        UserDefaults.standard.set(userPic, forKey: businessPicKey)
    }
    
    static func save(ownerDetail:Int) {
        UserDefaults.standard.set(ownerDetail, forKey: ownerDetailsKey)
    }
    
    static func save(retailDetail:Int) {
        UserDefaults.standard.set(retailDetail, forKey: retailDetailsKey)
    }
    
    static func save(queueAdded:String) {
        UserDefaults.standard.set(queueAdded, forKey: queueKey)
    }
    
    static func save(profileStatus:Int) {
        UserDefaults.standard.set(profileStatus, forKey: profileStatusKey)
    }
    
    static func save(retaileType:Int) {
        UserDefaults.standard.set(retaileType, forKey: retailerTypeKey)
    }
}
