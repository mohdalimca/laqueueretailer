//
//  Loader.swift
//  LaqueueCutomer
//
//  Created by Adityaraj Singh Gaharwar on 27/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import ACProgressHUD_Swift

class Loader: NSObject {
    static let shared = Loader()
    
    func showIndicator(view: UIView) {
        ACProgressHUD.shared.progressText = "Loading..."
        ACProgressHUD.shared.progressTextColor = #colorLiteral(red: 0, green: 0.9833222032, blue: 0.9161729813, alpha: 1)
        ACProgressHUD.shared.showHudAnimation = .growIn
        ACProgressHUD.shared.dismissHudAnimation = .growOut
//        ACProgressHUD.shared.enableBlurBackground = true
        ACProgressHUD.shared.enableBackground = false
        ACProgressHUD.shared.indicatorColor = #colorLiteral(red: 0, green: 0.9833222032, blue: 0.9161729813, alpha: 1)
        ACProgressHUD.shared.hudBackgroundColor = .black
        ACProgressHUD.shared.shadowColor = #colorLiteral(red: 0, green: 0.9833222032, blue: 0.9161729813, alpha: 1)
        ACProgressHUD.shared.showHudAnimation = .growIn
        ACProgressHUD.shared.dismissHudAnimation = .growOut
        ACProgressHUD.shared.showHUD()
    }
    
    func hideIndicator() {
        ACProgressHUD.shared.hideHUD()
    }
    
    
}
