//
//  InputViewButton.swift
//  ConsumerApp
//
//  Created by cis on 01/04/20.
//  Copyright © 2020 cis. All rights reserved.
//

import UIKit

class InputViewButton: UIButton {
    var myView = UIView()
    var toolBarView = UIView()

    override var inputView: UIView {
        get {
            return self.myView
        }

        set {
            self.myView = newValue
            self.becomeFirstResponder()
        }
    }

    override var inputAccessoryView: UIView {
        get {
            return self.toolBarView
        }
        set {
            self.toolBarView = newValue
        }
    }

    override var canBecomeFirstResponder: Bool {
        return true
    }
}
