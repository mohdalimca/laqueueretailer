//
//  extensions.swift
//  InternationalMotorcoachGroup
//
//  Created by cis on 24/07/19.
//  Copyright © 2019 cis. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    @IBInspectable var cornerRadius: CGFloat {
        
        get{
            return layer.cornerRadius
        }
        set {
            DispatchQueue.main.async {
                self.layoutIfNeeded()
                self.layer.cornerRadius = self.frame.height/newValue
                self.layer.masksToBounds = newValue > 0
            }
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            DispatchQueue.main.async {
                self.layoutIfNeeded()
                self.layer.borderWidth = newValue
            }
        }
    }
    
    
    @IBInspectable var borderColor: UIColor {
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
        set {
            DispatchQueue.main.async {
                self.layoutIfNeeded()
                self.layer.borderColor = newValue.cgColor
            }
        }
    }
    
    func fadeIn(duration: TimeInterval = 1.0) {
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 1.0
        })
    }
    
    func fadeOut(duration: TimeInterval = 1.0) {
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 0.0
        })
    }
}

//extension UILabel{
//    @IBInspectable var lineSpacing: CGFloat {
//        get {
//            return NSMutableParagraphStyle().lineSpacing
//        }
//        set {
//            DispatchQueue.main.async {
//                let attributedString = NSMutableAttributedString(string: self.text ?? "")
//
//                // *** Create instance of `NSMutableParagraphStyle`
//                let paragraphStyle = NSMutableParagraphStyle()
//
//                // *** set LineSpacing property in points ***
//                paragraphStyle.lineSpacing = newValue // Whatever line spacing you want in points
//
//                // *** Apply attribute to string ***
//                attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
//
//                // *** Set Attributed String to your label ***
//                self.attributedText = attributedString
//            }
//        }
//    }
//}

extension UIViewController {
    
    func endEditing() {
        self.view.endEditing(true)
    }
    
    func navigateToNavigationView() {
        let notification = RetailerStoryBoard.Profile(controller: Controller.Notification) as! NotificationViewController
        self.navigationController?.pushViewController(notification, animated: true)
    }
    
    func func_Alert(message:String) {
        let alert = UIAlertController.init(title: Constants.APP_NAME, message: "\(message)", preferredStyle: .alert)
        
        //        let alertAction = UIAlertAction.init(title: "Test", style: .default, handler: nil)
        //        alert.addAction(alertAction)
        //        let alertAction1 = UIAlertAction.init(title: "Testing", style: .default, handler: nil)
        //        alert.addAction(alertAction1)
        
        alert.addAction(UIAlertAction.init(title: "Ok", style: .default, handler: nil))
        
        //        let array: [UIView] = alert.view.subviews
        //        let subViewsArr = array[0].subviews[0].subviews[0].subviews[1].subviews[0].subviews
        //        print(subViewsArr.count)
        //        array[0].subviews[0].subviews[0].subviews[0].backgroundColor = UIColor.red
        //        array[0].subviews[0].subviews[0].subviews[0].tintColor = UIColor.white
        //        array[0].subviews[0].subviews[0].subviews[1].backgroundColor = UIColor.yellow
        //        array[0].subviews[0].subviews[0].subviews[1].tintColor = UIColor.white
        //        array[0].Subviews[0].Subviews[0].Subviews[0].BackgroundColor = UIColor.FromRGB(0,47,53);
        //        array[0].Subviews[0].Subviews[0].Subviews[0].TintColor = UIColor.White;
        //        array[0].Subviews[0].Subviews[0].Subviews[1].BackgroundColor = UIColor.FromRGB(0, 63, 71);
        //        array[0].Subviews[0].Subviews[0].Subviews[1].TintColor = UIColor.White;
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func func_AlertWithOneOption(message:String, optionOne:String, actionOptionOne:@escaping() -> Void)  {
        let alert = UIAlertController.init(title: Constants.APP_NAME, message: "\(message)", preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: optionOne, style: .default, handler: { (action) in
            actionOptionOne()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func func_AlertWithTwoOption(message:String, optionOne:String, optionTwo:String, actionOne:@escaping ()->Void, actionTwo:@escaping ()->Void) {
        let alert = UIAlertController.init(title: Constants.APP_NAME, message: "\(message)", preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: optionOne, style: .default, handler: { (action) in
            actionOne()
        }))
        alert.addAction(UIAlertAction.init(title: optionTwo, style: .default, handler: { (action) in
            actionTwo()
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func showInputDialog(title:String? = nil,
                         subtitle:String? = nil,
                         actionTitle:String? = "Add",
                         cancelTitle:String? = "Cancel",
                         inputPlaceholder:String? = nil,
                         inputKeyboardType:UIKeyboardType = UIKeyboardType.default,
                         cancelHandler: ((UIAlertAction) -> Swift.Void)? = nil,
                         actionHandler: ((_ text: String?) -> Void)? = nil) {
        
        let alert = UIAlertController(title: title, message: subtitle, preferredStyle: .alert)
        alert.addTextField { (textField:UITextField) in
            textField.placeholder = inputPlaceholder
            textField.keyboardType = inputKeyboardType
        }
        alert.addAction(UIAlertAction(title: actionTitle, style: .destructive, handler: { (action:UIAlertAction) in
            guard let textField =  alert.textFields?.first else {
                actionHandler?(nil)
                return
            }
            actionHandler?(textField.text)
        }))
        alert.addAction(UIAlertAction(title: cancelTitle, style: .cancel, handler: cancelHandler))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func validate(YourEMailAddress: String) -> Bool {
        let REGEX: String
        REGEX = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9-]+\\.[A-Za-z]{2,6}"
        return NSPredicate(format: "SELF MATCHES %@", REGEX).evaluate(with: YourEMailAddress)
    }
    func getTimeNDateFromTimeStamp(stamp:Int, outFormat:String) -> String {
        let dateFormatter = DateFormatter()
        let sdate = Date(timeIntervalSince1970: TimeInterval(stamp))
        // change to a readable time format and change to local time zone
        dateFormatter.dateFormat = outFormat//"hh:mm a"//"yyyy, MMM d, h:mm a"
        let timeStamp = dateFormatter.string(from: sdate)
        return timeStamp
    }
    func isValidPhone(phone: String) -> Bool {
        
        let phoneRegex = "^[0-9]{6,14}$";
        let valid = NSPredicate(format: "SELF MATCHES %@", phoneRegex).evaluate(with: phone)
        return valid
    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
}

extension Date {
    func adding(minutes: Int) -> Date {
        return Calendar.current.date(byAdding: .minute, value: minutes, to: self)!
    }
    
    func dayOfWeek() -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        return dateFormatter.string(from: self).capitalized
        // or use capitalized(with: locale) if you want
    }
    
    var currentDay: Int {
        let calendar = Calendar.current
        let compomnents = calendar.dateComponents([.weekday], from: self)
        return (compomnents.weekday ?? 0)
    }

    func timePassed(from date: Date) -> String {
        var time = ""
        var str = ""
        let interval = Calendar.current.dateComponents([.year, .month, .weekOfMonth, .day, .hour, .minute], from: date, to: self)
        if let year = interval.year, year > 0 {
            str = (year > 1 ) ? "yr" : "yr"
            time = "\(year)" + " " + "\(str)" + " "
        } else if let month = interval.month, month > 0 {
            str = (month > 1 ) ? "mn" : "mn"
            time =  "\(month)" + " " + "\(str)" + " "
        } else if let week = interval.weekOfMonth, week > 0 {
            str = (week > 1 ) ? "wk" : "wk"
            time =  "\(week)" + " " + "\(str)" + " "
        } else if let day = interval.day, day > 0 {
            str = (day > 1 ) ? "d" : "d"
            time =  "\(day)" + " " + "\(str)" + " "
        } else if let hour = interval.hour, hour > 0 {
            str = (hour > 1 ) ? "h" : "h"
            time =  "\(hour)" + " " + "\(str)" + " "
        } else if let minute = interval.minute, minute > 0 {
            str = (minute > 1 ) ? "min" : "min"
            time =  "\(minute)" + " " + "\(str)" + " "
        } else {
            time = "now"
        }
        return time
    }
}

extension Data {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}

extension String {
    var html2AttributedString: NSAttributedString? {
        return Data(utf8).html2AttributedString
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
    
    var toDouble: Double {
        return NumberFormatter().number(from: self)?.doubleValue ?? 0
    }
    
    var toInt: Int {
        return NumberFormatter().number(from: self)?.intValue ?? 0
    }
}


extension JSONAny{
    func toString() -> String{
        if let val = self.value as? String{
            return val
        }
        
        if let val = self.value as? Int64{
            return "\(val)"
        }
        
        if let val = self.value as? Double{
            return "\(val)"
        }
        
        return ""
    }
}


class JSONAny: Codable {
    let value: Any
    
    static func decodingError(forCodingPath codingPath: [CodingKey]) -> DecodingError {
        let context = DecodingError.Context(codingPath: codingPath, debugDescription: "Cannot decode JSONAny")
        return DecodingError.typeMismatch(JSONAny.self, context)
    }
    
    static func encodingError(forValue value: Any, codingPath: [CodingKey]) -> EncodingError {
        let context = EncodingError.Context(codingPath: codingPath, debugDescription: "Cannot encode JSONAny")
        return EncodingError.invalidValue(value, context)
    }
    
    static func decode(from container: SingleValueDecodingContainer) throws -> Any {
        if let value = try? container.decode(Bool.self) {
            return value
        }
        if let value = try? container.decode(Int64.self) {
            return value
        }
        if let value = try? container.decode(Double.self) {
            return value
        }
        if let value = try? container.decode(String.self) {
            return value
        }
        if container.decodeNil() {
            return JSONNull()
        }
        throw decodingError(forCodingPath: container.codingPath)
    }
    
    static func decode(from container: inout UnkeyedDecodingContainer) throws -> Any {
        if let value = try? container.decode(Bool.self) {
            return value
        }
        if let value = try? container.decode(Int64.self) {
            return value
        }
        if let value = try? container.decode(Double.self) {
            return value
        }
        if let value = try? container.decode(String.self) {
            return value
        }
        if let value = try? container.decodeNil() {
            if value {
                return JSONNull()
            }
        }
        if var container = try? container.nestedUnkeyedContainer() {
            return try decodeArray(from: &container)
        }
        if var container = try? container.nestedContainer(keyedBy: JSONCodingKey.self) {
            return try decodeDictionary(from: &container)
        }
        throw decodingError(forCodingPath: container.codingPath)
    }
    
    static func decode(from container: inout KeyedDecodingContainer<JSONCodingKey>, forKey key: JSONCodingKey) throws -> Any {
        if let value = try? container.decode(Bool.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(Int64.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(Double.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(String.self, forKey: key) {
            return value
        }
        if let value = try? container.decodeNil(forKey: key) {
            if value {
                return JSONNull()
            }
        }
        if var container = try? container.nestedUnkeyedContainer(forKey: key) {
            return try decodeArray(from: &container)
        }
        if var container = try? container.nestedContainer(keyedBy: JSONCodingKey.self, forKey: key) {
            return try decodeDictionary(from: &container)
        }
        throw decodingError(forCodingPath: container.codingPath)
    }
    
    static func decodeArray(from container: inout UnkeyedDecodingContainer) throws -> [Any] {
        var arr: [Any] = []
        while !container.isAtEnd {
            let value = try decode(from: &container)
            arr.append(value)
        }
        return arr
    }
    
    static func decodeDictionary(from container: inout KeyedDecodingContainer<JSONCodingKey>) throws -> [String: Any] {
        var dict = [String: Any]()
        for key in container.allKeys {
            let value = try decode(from: &container, forKey: key)
            dict[key.stringValue] = value
        }
        return dict
    }
    
    static func encode(to container: inout UnkeyedEncodingContainer, array: [Any]) throws {
        for value in array {
            if let value = value as? Bool {
                try container.encode(value)
            } else if let value = value as? Int64 {
                try container.encode(value)
            } else if let value = value as? Double {
                try container.encode(value)
            } else if let value = value as? String {
                try container.encode(value)
            } else if value is JSONNull {
                try container.encodeNil()
            } else if let value = value as? [Any] {
                var container = container.nestedUnkeyedContainer()
                try encode(to: &container, array: value)
            } else if let value = value as? [String: Any] {
                var container = container.nestedContainer(keyedBy: JSONCodingKey.self)
                try encode(to: &container, dictionary: value)
            } else {
                throw encodingError(forValue: value, codingPath: container.codingPath)
            }
        }
    }
    
    static func encode(to container: inout KeyedEncodingContainer<JSONCodingKey>, dictionary: [String: Any]) throws {
        for (key, value) in dictionary {
            let key = JSONCodingKey(stringValue: key)!
            if let value = value as? Bool {
                try container.encode(value, forKey: key)
            } else if let value = value as? Int64 {
                try container.encode(value, forKey: key)
            } else if let value = value as? Double {
                try container.encode(value, forKey: key)
            } else if let value = value as? String {
                try container.encode(value, forKey: key)
            } else if value is JSONNull {
                try container.encodeNil(forKey: key)
            } else if let value = value as? [Any] {
                var container = container.nestedUnkeyedContainer(forKey: key)
                try encode(to: &container, array: value)
            } else if let value = value as? [String: Any] {
                var container = container.nestedContainer(keyedBy: JSONCodingKey.self, forKey: key)
                try encode(to: &container, dictionary: value)
            } else {
                throw encodingError(forValue: value, codingPath: container.codingPath)
            }
        }
    }
    
    static func encode(to container: inout SingleValueEncodingContainer, value: Any) throws {
        if let value = value as? Bool {
            try container.encode(value)
        } else if let value = value as? Int64 {
            try container.encode(value)
        } else if let value = value as? Double {
            try container.encode(value)
        } else if let value = value as? String {
            try container.encode(value)
        } else if value is JSONNull {
            try container.encodeNil()
        } else {
            throw encodingError(forValue: value, codingPath: container.codingPath)
        }
    }
    
    public required init(from decoder: Decoder) throws {
        if var arrayContainer = try? decoder.unkeyedContainer() {
            self.value = try JSONAny.decodeArray(from: &arrayContainer)
        } else if var container = try? decoder.container(keyedBy: JSONCodingKey.self) {
            self.value = try JSONAny.decodeDictionary(from: &container)
        } else {
            let container = try decoder.singleValueContainer()
            self.value = try JSONAny.decode(from: container)
        }
    }
    
    public func encode(to encoder: Encoder) throws {
        if let arr = self.value as? [Any] {
            var container = encoder.unkeyedContainer()
            try JSONAny.encode(to: &container, array: arr)
        } else if let dict = self.value as? [String: Any] {
            var container = encoder.container(keyedBy: JSONCodingKey.self)
            try JSONAny.encode(to: &container, dictionary: dict)
        } else {
            var container = encoder.singleValueContainer()
            try JSONAny.encode(to: &container, value: self.value)
        }
    }
}


class JSONNull: Codable, Hashable {
    
    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
        return true
    }
    
    public var hashValue: Int {
        return 0
    }
    
    public init() {}
    
    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}

class JSONCodingKey: CodingKey {
    let key: String
    
    required init?(intValue: Int) {
        return nil
    }
    
    required init?(stringValue: String) {
        key = stringValue
    }
    
    var intValue: Int? {
        return nil
    }
    
    var stringValue: String {
        return key
    }
}

extension Collection where Iterator.Element == [String:AnyObject] {
    func toJSONString(options: JSONSerialization.WritingOptions = .prettyPrinted) -> String {
        if let arr = self as? [[String:AnyObject]],
            let dat = try? JSONSerialization.data(withJSONObject: arr, options: options),
            let str = String(data: dat, encoding: String.Encoding.utf8) {
            return str
        }
        return ""
    }
}


extension TimeInterval {
    var unixToDatabase: String {
        let date = Date(timeIntervalSince1970: self)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        return dateFormatter.string(from: date)
    }
    
    var dateMonth: String {
        let date = Date(timeIntervalSince1970: self)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM"
        return dateFormatter.string(from: date)
    }
    
    var placeDetailDate: String {
        let date = Date(timeIntervalSince1970: self)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy - h:mm a"
        dateFormatter.amSymbol = "AM"
        dateFormatter.pmSymbol = "PM"
        return dateFormatter.string(from: date)
    }
    
    var taskRemainingTime: String {
        let date = Date(timeIntervalSince1970: self)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "d HH:mm"
        dateFormatter.timeZone = .current
        return dateFormatter.string(from: date)
    }
    
    func daysDiffernce(end:TimeInterval) -> Int {
        let startDate = Date(timeIntervalSince1970: self)
        let endDate = Date(timeIntervalSince1970: end)
        let component = Set<Calendar.Component>([.day, .month, .year])
        let difference = Calendar.current.dateComponents(component, from: startDate, to: endDate)
        return difference.day ?? 0
    }
    
    
    var daysPassed: Int {
        let createDate = Date(timeIntervalSince1970: self)
        let component = Set<Calendar.Component>([.day, .month, .year])
        let difference = Calendar.current.dateComponents(component, from: Date(), to: createDate)
        return difference.day ?? 0
    }
    
    func dateFromTimestamp() -> Date {
        return Date(timeIntervalSince1970: self)
    }
}

extension UIColor {
    
    convenience init(hex: String, alpha:CGFloat = 1.0) {
        var hexInt: UInt32 = 0
        let scanner = Scanner(string: hex)
        scanner.charactersToBeSkipped = CharacterSet(charactersIn: "#")
        scanner.scanHexInt32(&hexInt)
        
        let red = CGFloat((hexInt & 0xff0000) >> 16) / 255.0
        let green = CGFloat((hexInt & 0xff00) >> 8) / 255.0
        let blue = CGFloat((hexInt & 0xff) >> 0) / 255.0
        let alpha = alpha
        
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
    
    convenience init(r: CGFloat, g:CGFloat, b:CGFloat) {
        self.init(red: r/255, green: g/255, blue: b/255, alpha: 1)
    }
    
    func isEqualTo(_ color: UIColor) -> Bool {
        var red1: CGFloat = 0, green1: CGFloat = 0, blue1: CGFloat = 0, alpha1: CGFloat = 0
        getRed(&red1, green:&green1, blue:&blue1, alpha:&alpha1)
        
        var red2: CGFloat = 0, green2: CGFloat = 0, blue2: CGFloat = 0, alpha2: CGFloat = 0
        color.getRed(&red2, green:&green2, blue:&blue2, alpha:&alpha2)
        
        return red1 == red2 && green1 == green2 && blue1 == blue2 && alpha1 == alpha2
    }
}

class PaddingLabel: UILabel {

   @IBInspectable var topInset: CGFloat = 0
   @IBInspectable var bottomInset: CGFloat = 0
   @IBInspectable var leftInset: CGFloat = 5.0
   @IBInspectable var rightInset: CGFloat = 5.0

   override func drawText(in rect: CGRect) {
      let insets = UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
    super.drawText(in: rect.inset(by: insets))
   }

   override var intrinsicContentSize: CGSize {
      get {
         var contentSize = super.intrinsicContentSize
         contentSize.height += topInset + bottomInset
         contentSize.width += leftInset + rightInset
         return contentSize
      }
   }
}

extension Notification.Name {
    public static let updateUserProfile = Notification.Name("updateUserProfile")
    public static let reloadEmployeeList = Notification.Name("reloadEmployeeList")
}

extension UIButton {
    func setProfileButtonImage() {
        self.kf.setImage(with: URL(string: UserStore.userPic ?? ""), for: .normal)
    }
}
