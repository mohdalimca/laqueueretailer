//
//  Global.swift
//  FastAide
//
//  Created by cis on 10/10/19.
//  Copyright © 2019 cis. All rights reserved.
//

import Foundation

var dev_Token = ""
var isSkipTapped = false
var userData = "LoginData"
//let GOOGLE_API_KEY = "AIzaSyAQHwHJpmNMxENAfjGcsmcXxDzmvYoNm6I"
let GOOGLE_API_KEY = "AIzaSyC3lraGkWQ0pfMAaZXu1vm5hq0z4A9kQew"
let securityToken: [String: String] = ["SecurityToken": "n2Y0Acd6Yq6oo21sGDVTALRFz"]

struct Constants {
    static let APP_NAME = "LaQueue Retailer"
    static let NO_INTERNET = "No Internet Connection"
    static let SOMETHING_WENT_WRONG = "Something went wrong"
    static let ENTER_EMAIL = "Enter E-mail address"
    static let ENTER_VALID_EMAIL = "Enter Valid E-mail address"
    static let ENTER_PASSWORD = "Enter Password"
    static let ENTER_FULLNAME = "Enter your full name"
    static let ENTER_VALID_NUMBER = "Enter valid mobile number"
    static let TICK_CHECKBOX = "Please Accept terms and condition"
    static let ENTER_OLD_PASSWORD = "Enter your current password"
    static let ENTER_NEW_PASSWORD = "Enter new password"
    static let ENTER_CONFIRM_PASSWORD = "Enter confirm password"
    static let MATCH_CONFIRM_PASSWORD = "Confirm password should be same as new password"
    static let ENTER_VALID_COMPLAIN = "Enter valid complain or suggestions"
    static let ALERT_OK_OPTION = "Ok"
}

struct Controller {
    static let Splash = "SplashOptionsVC"
    static let Login = "LoginVC"
    static let SignUp = "SignUpVC"
    static let ForgotPassword = "ForgotPasswordVC"
    static let Home = "HomeVC"
    static let Search = "SearchVC"
    static let Menu = "MenuVC"
    static let StaticContent = "StaticContentVC"
    static let MyProfile = "MyProfileVC"
    static let ContactUs = "ContactUsVC"
    static let NewPassword = "NewPasswordVC"
    static let RecoverCode = "RecoverCodeVC"
    static let Prefrences = "PrefrencesVC"
    static let MainContainer = "MainContainerVC"
    static let EmployeeList = "EmployeeListVC"
    static let NewEmployee = "NewEmployeeVC"
    static let Employee = "EmployeeVC"
    static let QueueList = "QueueListVC"
    static let NewQueue = "NewQueueVC"
    static let QueueStatus = "QueueStatusVC"
    static let PauseQueue = "PauseQueueVC"
    static let ChangePassword = "ChangePasswordVC"
    static let CustomDate = "CustomDateVC"
    static let Offdays = "OffDaysVC"
    static let Queue = "QueueVC"
    static let Picker = "PickerViewController"
    static let SelectEmp = "SelectEmpVC"
    static let EditQueueVC = "EditQueueVC"
    static let Acknowledge = "AcknowledgeVC"
    static let PlaceDetail = "PlaceDetailsVC"
    static let Notification = "NotificationViewController"
}

struct Cell {
    static let Home = "HomeCell"
    static let Search = "SearchCell"
    static let EmployeeList = "EmployeeListCell"
    static let QueueList = "QueueListCell"
    static let CustomDays = "CustomdaysCell"
    static let OffDays = "OffDaysCell"
    static let Tab = "TabCell"
    static let Filter = "FilterCell"
    static let HomeQueue = "HomeQueueCell"
    static let EmployeeNameCell = "EmployeeNameCell"
    static let NotificationCell = "NotificationTableViewCell"
}


