//
//  CustomShadowView.swift
//  Zvonr
//
//  Created by cis on 17/11/18.
//  Copyright © 2018 cis. All rights reserved.
//

import UIKit

class CustomShadowView: UIView {
    override func layoutSubviews() {
        super.layoutSubviews()
        if self.tag == 95 {
            DispatchQueue.main.async {
                
                let shadowSize : CGFloat = 0.1
                let shadowPath = UIBezierPath(roundedRect:CGRect(x: -shadowSize / 2,
                                                                 y: -shadowSize / 2,
                                                                 width: self.frame.size.width + shadowSize,
                                                                 height: self.frame.size.height + shadowSize),  cornerRadius: self.bounds.height/2)
                self.layer.cornerRadius = self.frame.height/2
                self.layer.masksToBounds = false
                self.layer.shadowColor = UIColor.darkGray.cgColor
                self.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
                self.layer.shadowOpacity = 0.5
                self.layer.shadowPath = shadowPath.cgPath
            }
        } else if self.tag == 94 {
            DispatchQueue.main.async {
                           
                           let shadowSize : CGFloat = 0.1
                           let shadowPath = UIBezierPath(roundedRect:CGRect(x: -shadowSize / 6,
                                                                            y: -shadowSize / 6,
                                                                            width: self.frame.size.width + shadowSize,
                                                                            height: self.frame.size.height + shadowSize),  cornerRadius: self.bounds.height/6)
                           self.layer.cornerRadius = self.frame.height/6
                           self.layer.masksToBounds = false
                           self.layer.shadowColor = UIColor.darkGray.cgColor
                           self.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
                           self.layer.shadowOpacity = 0.5
                           self.layer.shadowPath = shadowPath.cgPath
                       }
        } else if self.tag == 96 {
            DispatchQueue.main.async {
                
                let shadowSize : CGFloat = 0.1
                let shadowPath = UIBezierPath(roundedRect:CGRect(x: -shadowSize / 16,
                                                                 y: -shadowSize / 16,
                                                                 width: self.frame.size.width + shadowSize,
                                                                 height: self.frame.size.height + shadowSize),  cornerRadius: self.bounds.height/16)
                self.layer.cornerRadius = self.frame.height/16
                self.layer.masksToBounds = false
                self.layer.shadowColor = UIColor.darkGray.cgColor
                self.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
                self.layer.shadowOpacity = 0.5
                self.layer.shadowPath = shadowPath.cgPath
            }
        } else {
            self.layer.shadowColor = UIColor.gray.cgColor
            self.layer.shadowOpacity = 0.3
            self.layer.shadowOffset = CGSize.zero
            self.layer.shadowRadius = 6
        }
    }

}
