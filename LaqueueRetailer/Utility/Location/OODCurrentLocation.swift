//
//  HSLocation.swift
//  Blast
//
//  Created by cis on 02/08/18.
//  Copyright © 2018 cis. All rights reserved.
//

import UIKit
import Foundation
import SystemConfiguration
import CoreLocation
import GoogleMaps
import GooglePlaces

class OODCurrentLocation: NSObject, CLLocationManagerDelegate {
    //Variables
    static let sharedInstance: OODCurrentLocation = OODCurrentLocation()
    var locationManager:CLLocationManager! = nil
    typealias completionHanlder = (_ lat: String, _ long: String) -> Void
    var completion: completionHanlder! = nil
    
    //Functions
    func function_GetCurrentLocation(location:@escaping (String,String)->Void) {
        if CLLocationManager.locationServicesEnabled() {
            locationManager = CLLocationManager()
            self.locationManager.requestWhenInUseAuthorization()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
            self.completion =  { (lat, lng) in
                location(lat,lng)
            }
        }
    }
    //Delegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let isLocation = locations.first {
            completion("\(isLocation.coordinate.latitude)","\(isLocation.coordinate.longitude)")
            locationManager.stopUpdatingLocation()
            locationManager.delegate = nil
            locationManager = nil
        }
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        if let isLocation = manager.location {
            completion("\(isLocation.coordinate.latitude)","\(isLocation.coordinate.longitude)")
        } else {
            completion("\(0.0)","\(0.0)")
        }
    }
    func function_GetAddress(success: @escaping ((String,CLLocationCoordinate2D))->Void, failure: @escaping ()->Void) {
        self.function_GetCurrentLocation { (lat, long) in
            if let isLat = CLLocationDegrees.init(lat), let isLong = CLLocationDegrees.init(long) {
                let geocoder = GMSGeocoder()
                geocoder.reverseGeocodeCoordinate(CLLocationCoordinate2D.init(latitude: isLat, longitude: isLong)) { response , error in
                    if let address = response?.firstResult() {
                        if let isAddress = address.lines {
                            success(("\(isAddress.joined(separator: "\n"))", CLLocationCoordinate2D.init(latitude: isLat, longitude: isLong)))
                        } else {
                            failure()
                        }
                    } else {
                        failure()
                    }
                }
            }
        }
    }
    func function_GetAddressFromCoordinates(coordinates: CLLocationCoordinate2D , success: @escaping ((String,CLLocationCoordinate2D))->Void, failure: @escaping ()->Void) {
        let geocoder = GMSGeocoder()
    geocoder.reverseGeocodeCoordinate(coordinates) { response , error in
            if let address = response?.firstResult() {
                if let isAddress = address.lines {
                    success(("\(isAddress.joined(separator: "\n"))", CLLocationCoordinate2D.init(latitude: coordinates.latitude, longitude: coordinates.longitude)))
                } else {
                    failure()
                }
            } else {
                failure()
            }
        }
    }
}
