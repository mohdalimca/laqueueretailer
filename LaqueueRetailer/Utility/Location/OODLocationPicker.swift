//
//  HSLocationPicker.swift
//  FoodOrderCustomer
//
//  Created by cis on 07/01/19.
//  Copyright © 2019 Cis. All rights reserved.
//

import Foundation
import GoogleMaps
import GooglePlaces

class OODLocationPicker: NSObject, GMSAutocompleteViewControllerDelegate {
    static let location:OODLocationPicker = OODLocationPicker()
    typealias completionHanlder = (_ isError: Bool, _ address: GMSPlace?) -> Void
    var completion: completionHanlder! = nil
    func function_ShowPicker(_ controller: UIViewController, returnAddress: @escaping (_ isError: Bool, _ address: GMSPlace?)->Void) {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        if let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.name.rawValue) |
            UInt(GMSPlaceField.coordinate.rawValue) | UInt(GMSPlaceField.formattedAddress.rawValue)) {
            autocompleteController.placeFields = fields
        }
        controller.present(autocompleteController, animated: true, completion: nil)
        self.completion = { (isError,address) in
            returnAddress(isError,address)
        }
    }
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        viewController.dismiss(animated: true) {
            self.completion(false,place)
        }
    }
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        viewController.dismiss(animated: true) {
            self.completion(true, nil)
        }
    }
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        viewController.dismiss(animated: true) {
            self.completion(true, nil)
        }
    }
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}
