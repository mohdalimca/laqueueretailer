
//
//  AppConstant.swift
//  LaqueueRetailer
//
//  Created by Apple on 30/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation
import UIKit

enum QueueStatus:String, Codable {
    case Play = "Play"
    case Pause = "Pause"
}

enum BookingStatus:String, Codable {
    case New = "New"
    case Acknowledge = "Acknowledge"
    case Noshow = "Noshow"
    case Call = "Call"
    case Update = "Update"
    case Cancel = "Cancel"
    case Accept = "Accept"
}

let userPlaceholder:UIImage = UIImage(named: "user-placeholder")!
let profileUser:UIImage = UIImage(named: "profile-user")!
