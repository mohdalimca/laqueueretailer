//
//  LoginSession.swift
//  FastAide
//
//  Created by cis on 11/10/19.
//  Copyright © 2019 cis. All rights reserved.
//

import Foundation
import UIKit

class LoginSession: NSObject {
    static let shared:LoginSession = LoginSession()
    func checkLoginStatus() -> Bool  {
        if let isLogin = UserDefaults.standard.value(forKey: "login") as? Bool {
            if isLogin {
                return isLogin
            } else {
                return false
            }
        } else {
            UserDefaults.standard.setValue(false, forKey: "login")
            UserDefaults.standard.synchronize()
            return false
        }
    }
    
    func updateLoginStatus(_ isLogin: Bool) {
        UserDefaults.standard.setValue(isLogin, forKey: "login")
        UserDefaults.standard.synchronize()
    }
    
    func saveUserDefault(userModel:LoginModel,keyName:String) -> Void {
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(userModel) {
            let defaults = UserDefaults.standard
            defaults.set(encoded, forKey: keyName)
            UserDefaults.standard.synchronize()
        }
    }
    
    func getUserModelDefault(keyName:String) -> LoginModel?{
        if let userModel = UserDefaults.standard.object(forKey: keyName) as? Data {
            let decoder = JSONDecoder()
            if let loadedUserModel = try? decoder.decode(LoginModel.self, from: userModel) {
                return loadedUserModel
            }
        }
        return nil
    }
    
    func clearUserDefault(){
        let userDefault = UserDefaults.standard
//        userDefault.removeObject(forKey: "userData")
        userDefault.removeObject(forKey: userData)
        userDefault.synchronize()
    }
}
