//
//  UITextView+DynamicFontSize.h
//  Cook4Me
//
//  Created by cis on 20/07/18.
//  Copyright © 2018 cis. All rights reserved.
//

#ifndef UITextView_DynamicFontSize_h
#define UITextView_DynamicFontSize_h

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UITextView (DynamicFontSize)
@property (nonatomic) IBInspectable BOOL adjustFontSize;

@end

#endif /* UITextView_DynamicFontSize_h */
