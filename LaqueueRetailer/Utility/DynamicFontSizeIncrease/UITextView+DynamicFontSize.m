//
//  UITextView+DynamicFontSize.m
//  Cook4Me
//
//  Created by cis on 20/07/18.
//  Copyright © 2018 cis. All rights reserved.
//

#import "UITextView+DynamicFontSize.h"

@implementation UITextView (DynamicFontSize)
@dynamic adjustFontSize;
-(void)setAdjustFontSize:(BOOL)adjustFontSize{
    if (adjustFontSize)
    {
        CGRect screenBounds = [[UIScreen mainScreen] bounds];
        self.font = [self.font fontWithSize:self.font.pointSize*(screenBounds.size.width/320)]; // 320 for iPhone 5(320x568) storyboard design
        // if you design with iphone 6(375x667) in storyboard, use 375 instead of 320 and iphone 6 plus(414x736), use 414
    }
}

@end
