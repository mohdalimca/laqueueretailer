//
//  UIButton+DynaymicFontSize.h
//  Cook4Me
//
//  Created by cis on 20/07/18.
//  Copyright © 2018 cis. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIButton (DynamicFontSize)
@property (nonatomic) IBInspectable BOOL adjustFontSize;

@end
