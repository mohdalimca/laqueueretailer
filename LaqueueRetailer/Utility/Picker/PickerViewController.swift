//
//  PickerViewController.swift
//  LaqueueRetailer
//
//  Created by Apple on 10/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

protocol GetPickerValueDelagate {
    func getPickerValue(_ value:String, _ index:Int)
}

class PickerViewController: UIViewController {
    
    var delegate:GetPickerValueDelagate?
    var index = 0
    var selectedValue = ""
    var data = [String]()
    var isType: Bool!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if data.count > 0 {
            self.selectedValue = data[self.index]
        }
    }
    
    @IBAction func btnCancelAction(_ sender: UIBarButtonItem) {
        self.delegate?.getPickerValue("", -1)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnDoneAction(_ sender: UIBarButtonItem) {
        self.delegate?.getPickerValue(self.selectedValue, self.index)
        self.dismiss(animated: true, completion: nil)
    }
}

extension PickerViewController:UIPickerViewDelegate,UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return data.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return data[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if data.count > 0 {
            self.index = row
            self.selectedValue = data[row]
        }
    }
}

