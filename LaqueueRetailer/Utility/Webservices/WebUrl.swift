//
//  WebUrl.swift
//  FastAide
//
//  Created by cis on 21/10/19.
//  Copyright © 2019 cis. All rights reserved.
//

import Foundation

struct WebUrl {
    static let baseUrl            = "http://knowledgerevise.com/Laqueue/webservices/"
    static let emailVerify        = baseUrl + "users/signup_otp"
    static let login              = baseUrl + "users/business_login"
    static let registration       = baseUrl + "users/register"
    static let forgotPassword     = baseUrl + "users/forgotpassword"
    static let newPassword        = baseUrl + "users/reset_password"
    static let userProfile        = baseUrl + "users/get_profile"
    static let updateProfile      = baseUrl + "users/profile_update"
    static let changePassword     = baseUrl + "users/changepassword"
    static let logout             = baseUrl + "users/logout"
    static let privacy            = baseUrl + "static_page/privacy_policy"
    static let aboutUs            = baseUrl + "static_page/about_app"
    static let terms              = baseUrl + "static_page/terms_condition"
    static let addEmployee        = baseUrl + "employees/employee_add"
    static let updateEmployee     = baseUrl + "employees/employee_update"
    static let getprefrence       = baseUrl + "users/get_prefrence_setting"
    static let updatePrefrences   = baseUrl + "users/prefrence_update"
    static let addQueue           = baseUrl + "queues/add"
    static let updateQueue        = baseUrl + "queues/update"
    static let getEmployeeList    = baseUrl + "employees/employee_list"
    static let getQueues          = baseUrl + "queues/myqueue"
    static let employeeDetail     = baseUrl + "employees/employee_detail"
    static let categories         = baseUrl + "users/category"
    static let resendOTP          = baseUrl + "users/resend_otp"
    static let changeQueueStatus  = baseUrl + "queues/status_update"
    static let removeQueue        = baseUrl + "queues/queue_remove"
    static let queueBookings      = baseUrl + "users/bookingbyqueue"
    static let removeEmployee     = baseUrl + "employees/employee_remove"
    static let bookingStatus      = baseUrl + "queues/booking_status"
    static let notifications      = baseUrl + "users/notification_list"
    static let notifyEmployee     = baseUrl + "employees/notify"
    static let placeInQueue       = baseUrl + "users/order_single"
    
}
