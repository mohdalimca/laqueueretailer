//
//  K3Webservices.swift
//  CityWall
//
//  Created by Karan Kumar Kanathe on 03/04/19.
//  Copyright © 2019 Karan Kanathe. All rights reserved.
//

import Foundation
import Alamofire
import UIKit
import ACProgressHUD_Swift

class K3Webservice : NSObject{

    class func run<T:Decodable>(isMultipart:Bool = false,urlString:String,method : HTTPMethod,Param:[String:Any]?,header:[String:String]?,file:[String:Any]? = nil,type:T.Type,viewController : UIViewController?,fullAccess : Bool = false,encoding:K3encoding = K3encoding.JSON,Completion:@escaping (T?) -> ()) {
        print("Hitting Api => \(urlString)")
        print("With params => \(String(describing: Param))")
        if Connectivity.isConnectedToInternet() == false{
            
            ACProgressHUD.shared.hideHUD()
            AlertView.instance.show(Constants.APP_NAME, message: Constants.NO_INTERNET, handler: nil)
            return
        }

        if isMultipart{
            if viewController != nil{
//
//  ManagerSharedInstance.showIndicator()
//                Loader.instance.show(view: (viewController?.view)!)
            }
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                if file != nil{
                    let keys = file?.keys
                    for key in keys! {
                        if (file!["\(key)"] as? UIImage) != nil {
                            if let imageData = (file!["\(key)"] as! UIImage).jpegData(compressionQuality: 0.8) {
                                print("\(key)--\(imageData)")
                                multipartFormData.append(imageData, withName: "\(key)",fileName: "file.jpg", mimeType: "image/jpeg")
                            }
                        }

                        if let imagess  = file!["\(key)"] as? Data {
                            print("\(key)--\(imagess)")
                            multipartFormData.append(imagess, withName: "\(key)",fileName: "file.mov", mimeType: "video/*")
                        }

                        for key in keys! {
                            if let imagess  = file!["\(key)"] as? [UIImage] {
                                for img in imagess {
                                    if let imageData = img.jpegData(compressionQuality: 0.8) {
                                        print("\(key)--\(imageData)")
                                        multipartFormData.append(imageData, withName: "\(key)",fileName: "file.jpg", mimeType: "image/jpeg")
                                    }
                                }
                            }
                        }
                    }
                }


                if Param != nil{

                    for (key, value) in Param! {
                        if let isString = value as? String {
                            print("\(key)--\(value)")

                            multipartFormData.append(isString.data(using: String.Encoding.utf8)!, withName: key)
                        }
                        if let value = value as? Int {
                            print("\(key)--\(value)")

                            multipartFormData.append(String(format:"%d",value).data(using: String.Encoding.utf8)!, withName: key)
                        }

                        if let value = value as? [[String:Int]] {
                            print("\(key)--\(value)")

                            multipartFormData.append(String(format:"%d",value).data(using: String.Encoding.utf8)!, withName: key)
                        }

                        if let value = value as? [String:Any] {
                            print("\(key)--\(value)")
                            multipartFormData.append(String(format:"%d",value).data(using: String.Encoding.utf8)!, withName: key)
                        }
                    }
                }

            }, to: urlString, method:method, headers : header, encodingCompletion: { (results) in
                DispatchQueue.main.async(execute: {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    switch results {
                    case .success(let upload,_,_):
                        upload.responseData(completionHandler: { (data) in
                            guard let isData = data.value else {return}
                            let strData = NSString(data: isData, encoding: String.Encoding.utf8.rawValue)
                            print("\(strData ?? "")")
                        })
                        upload.responseJSON(completionHandler: { (response) in
                            print(response)
                            switch response.result{
                            case .success:
                                if let isValue = response.value as? [String:Any]{
                                    
                                    if fullAccess{
                                        let jsonDecoder = JSONDecoder()
                                        do{
                                            let data = try jsonDecoder.decode(T.self, from: response.data!)
                                            Completion(data)
                                        }catch let error{
                                            print(error)
                                            
                                            ACProgressHUD.shared.hideHUD()
                                            AlertView.instance.show(Constants.APP_NAME, message: Constants.SOMETHING_WENT_WRONG, handler: nil)
                                        }
                                    }else{
                                        if isValue["status"] as? Bool == true{
                                            let jsonDecoder = JSONDecoder()
                                            do{
                                                let data = try jsonDecoder.decode(T.self, from: response.data!)
                                                Completion(data)
                                            }catch let error{
                                                print(error)
                                                
//                                                ManagerSharedInstance.hideIndicator()
                                                AlertView.instance.show(Constants.APP_NAME, message: Constants.SOMETHING_WENT_WRONG, handler: nil)
                                            }
                                        }else{
                                            var message = ""
                                            
                                            if let isMessage = isValue["msg"] as? String{
                                                message = isMessage
                                            }
                                            
                                            if let isArrMessage = isValue["msg"] as? [String]{
                                                isArrMessage.forEach({
                                                    message.append("\($0)\n")
                                                })
                                            }
                                            let jsonDecoder = JSONDecoder()
                                            do{
                                                let data = try jsonDecoder.decode(T.self, from: response.data!)
                                                Completion(data)
                                            }catch let error{
                                                print(error)
                                                
                                                ACProgressHUD.shared.hideHUD()
                                                AlertView.instance.show(Constants.APP_NAME, message: message, handler: nil)
                                            }
                                        }
                                    }

                                }else{

                                }
                                if viewController != nil{
                                    ACProgressHUD.shared.hideHUD()
//                                    Loader.instance.hide()
                                }
                                break
                            case .failure(let error):
                                print(error)
                                if viewController != nil{
                                    ACProgressHUD.shared.hideHUD()
//                                    Loader.instance.hide()
                                }
                                AlertView.instance.show(Constants.APP_NAME, message: error.localizedDescription, handler: nil)

                            }
                        })
                    case .failure(let err):
                        print(err.localizedDescription)
                        
                        ACProgressHUD.shared.hideHUD()
                        AlertView.instance.show(Constants.APP_NAME, message: err.localizedDescription, handler: nil)



                    }
                })
            })
        }else{
            let url = URL(string: urlString)
            if viewController != nil{
//                ManagerSharedInstance.showIndicator()
//                Loader.instance.show(view: (viewController?.view!)!)
            }
            Alamofire.request(url!, method: method, parameters: Param, encoding: encoding == K3encoding.URL ? URLEncoding.default : JSONEncoding.default, headers : header).responseJSON { (resonse) in
                    if viewController != nil{
                        ACProgressHUD.shared.hideHUD()
//                        Loader.instance.hide()
                    }

                switch resonse.result{
                case .success:
                    //                print("Response Value =>>> \(NSString(data: resonse.value as? Data ?? Data(), encoding: String.Encoding.utf8.rawValue) ?? "")")
                    print(resonse)

                    if let isValue = resonse.value as? [String:Any]{
                        
                        if fullAccess{
                            let jsonDecoder = JSONDecoder()
                            do{
                                let data = try jsonDecoder.decode(T.self, from: resonse.data!)
                                Completion(data)
                                
                            }catch let error{
                                print(error)
                                
                                ACProgressHUD.shared.hideHUD()
                                AlertView.instance.show(Constants.APP_NAME, message:  Constants.SOMETHING_WENT_WRONG, handler: nil)
                                
                            }
                        }else{
                            if isValue["status"] as? Bool == true{

                                let jsonDecoder = JSONDecoder()
                                do{
                                    let data = try jsonDecoder.decode(T.self, from: resonse.data!)
                                    Completion(data)
                                    
                                }catch let error{
                                    print(error)
                                    
                                    ACProgressHUD.shared.hideHUD()
                                    AlertView.instance.show(Constants.APP_NAME, message:  Constants.SOMETHING_WENT_WRONG, handler: nil)
                                    
                                }
                            }else{
                                var message = ""
                                
                                if let isMessage = isValue["msg"] as? String{
                                    message = isMessage
                                }
                                
                                if let isArrMessage = isValue["msg"] as? [String]{
                                    isArrMessage.forEach({
                                        message.append("\($0)\n")
                                    })
                                }
                                let jsonDecoder = JSONDecoder()
                                do{
                                    let data = try jsonDecoder.decode(T.self, from: resonse.data!)
                                    Completion(data)
                                }catch let error{
                                    print(error)
                                    
                                    ACProgressHUD.shared.hideHUD()
                                    AlertView.instance.show(Constants.APP_NAME, message: message, handler: nil)
                                }
                                
                            }
                        }
                    }else{
                        AlertView.instance.show(Constants.APP_NAME, message: Constants.SOMETHING_WENT_WRONG, handler: nil)

                    }
                    break
                case .failure(let error):
                    print(error)
                    // Show alert here
                    
                    ACProgressHUD.shared.hideHUD()
                    AlertView.instance.show(Constants.APP_NAME, message:  error.localizedDescription, handler: nil)

                }
            }
        }

    }
}

class Connectivity {
    class func isConnectedToInternet() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}

enum K3encoding {
    case JSON
    case URL
}




class Loader1 : NSObject{
    static var instance  = Loader1()
    
    var activityIndicator : UIActivityIndicatorView!
    var viewActivityIndicator : UIView!
    
    func show(view : UIView){
        viewActivityIndicator = UIView(frame: view.frame)
        viewActivityIndicator.backgroundColor = .black
        viewActivityIndicator.alpha = 0.4
        activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.whiteLarge)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        viewActivityIndicator.addSubview(activityIndicator)
        activityIndicator.centerXAnchor.constraint(equalTo: viewActivityIndicator.centerXAnchor).isActive = true
        activityIndicator.centerYAnchor.constraint(equalTo: viewActivityIndicator.centerYAnchor).isActive = true
        activityIndicator.startAnimating()
        view.addSubview(viewActivityIndicator)
    }
    
    func hide(){
        viewActivityIndicator.removeFromSuperview()
    }
}


struct AlertControllerInfo {
    var presentingVC : UIViewController
    var alertController : UIAlertController
}

public class AlertView : NSObject {
    
    public class var instance : AlertView {
        struct Static {
            static let instanceAlert : AlertView = AlertView()
        }
        return Static.instanceAlert
    }
    
    fileprivate var alertCtrls = [AlertControllerInfo]()
    
    //Displays Alert Message with the OK button
    public func show(_ title: String?, message: String?, handler: ((UIAlertAction) -> Void)?) {
        let okAction = UIAlertAction.init(title: "OK", style: UIAlertAction.Style.cancel, handler: handler)
        
        self.showAlert(title, message: message, actions: [okAction], style: UIAlertController.Style.alert, view: nil)
    }
    
    //Displays Alert Message from the ERROR object
    public func showErrorAlert(error: Error) {
        if error._code == NSURLErrorNotConnectedToInternet {
            self.showOfflineAlert()
        } else {
            self.show("Error!", message: error.localizedDescription, handler: nil)
        }
    }
    
    //Displays Logout Alert Message
    public func showLogoutAlert() {
        AlertView.instance.show("User Logout!", message: "Someone has used your credentials to login into HealthePets. Please click on OK to login again.") { (response) in
            //Do your custom actions here
        }
    }
    
    //Displays OFFLINE Alert Message
    public func showOfflineAlert() {
        let dismissAction = UIAlertAction.init(title: "Dismiss", style: UIAlertAction.Style.cancel, handler: nil)
        let alertController = UIAlertController(title: "Error!", message: "Device is offline. Please check the network connection and try again.", preferredStyle: .alert)
        alertController.addAction(dismissAction)
        
        if let presentingViewController = UIApplication.shared.keyWindow?.rootViewController {
            if let presentedModalVC = presentingViewController.presentedViewController {
                presentedModalVC.present(alertController, animated: true, completion: nil)
            }
            else {
                presentingViewController.present(alertController, animated: true, completion: nil)
            }
        }
        
        // change to desired number of seconds (in this case 3 seconds)
        let when = DispatchTime.now() + 3
        DispatchQueue.main.asyncAfter(deadline: when){
            alertController.dismiss(animated: true, completion: nil)
        }
    }
    
    //Dismisses all alerts
    func cancelAllAlerts() {
        for alertCtrl in alertCtrls {
            if alertCtrl.alertController.presentingViewController != nil {
                alertCtrl.presentingVC.dismiss(animated: false, completion: {
                    
                    if let index = self.findAlertController(alertCtrl.alertController) {
                        self.alertCtrls.remove(at: index)
                    }
                })
            } else {
                self.alertCtrls.remove(at: self.findAlertController(alertCtrl.alertController)!)
            }
        }
    }
    
    fileprivate func showActions(_ title: String?, message: String?, actions: [UIAlertAction], view: AnyObject) {
        self.showAlert(title, message: message, actions: actions, style: UIAlertController.Style.actionSheet, view: view)
    }
    
    fileprivate func showAlert(_ title: String?, message: String?, actions: [UIAlertAction]) {
        self.showAlert(title, message: message, actions: actions, style: UIAlertController.Style.alert, view: nil)
    }
    
    fileprivate func showAlert(_ title: String?, message: String?, actions: [UIAlertAction], style: UIAlertController.Style, view: AnyObject?) {
        
        let alertController = UIAlertController.init(title: title, message: message == "" ? nil : message, preferredStyle: style) as UIAlertController
        
        for action: UIAlertAction in actions {
            alertController.addAction(action)
        }
        
        if let app = UIApplication.shared.delegate, let window = app.window, let presentingViewController = window?.rootViewController {
            
            let popOverPresentationVC = alertController.popoverPresentationController
            
            if let popover = popOverPresentationVC {
                if let view = view as? UIBarButtonItem {
                    popover.barButtonItem = view
                }
                else if let view = view as? UIView {
                    popover.sourceView = view;
                    popOverPresentationVC?.sourceRect = view.bounds;
                } else {
                    popOverPresentationVC?.sourceView = presentingViewController.view;
                    popOverPresentationVC?.sourceRect = presentingViewController.view.bounds;
                }
                
                popOverPresentationVC?.permittedArrowDirections = UIPopoverArrowDirection.any;
            }
            
            self.presentAlertController(alertController, presentingViewController: presentingViewController)
        }
    }
    
    fileprivate func showAlertWithController(_ alertController : UIAlertController) {
        if let presentingViewController = UIApplication.shared.keyWindow?.rootViewController?.presentedViewController {
            self.presentAlertController(alertController, presentingViewController: presentingViewController)
        }
    }
    
    fileprivate func presentAlertController(_ alertController : UIAlertController, presentingViewController : UIViewController) {
        DispatchQueue.main.async { () -> Void in
            
            // Handle modal VC scenarios
            if let presentedModalVC = presentingViewController.presentedViewController {
                presentedModalVC.present(alertController, animated: true, completion: nil)
                self.alertCtrls.append(AlertControllerInfo(presentingVC:presentedModalVC, alertController: alertController))
            }
            else {
                presentingViewController.present(alertController, animated: true, completion: nil)
                self.alertCtrls.append(AlertControllerInfo(presentingVC:presentingViewController, alertController: alertController))
            }
        }
    }
    
    fileprivate func findAlertController(_ alertCtrl: UIAlertController) -> Int? {
        for i in 0..<alertCtrls.count {
            if alertCtrls[i].alertController == alertCtrl {
                return i
            }
        }
        return nil
    }
}
