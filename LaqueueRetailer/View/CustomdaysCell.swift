//
//  CustomdaysCell.swift
//  LaqueueRetailer
//
//  Created by Adityaraj Singh Gaharwar on 05/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class CustomdaysCell: UITableViewCell {

    @IBOutlet weak var lbl_date: UILabel!
    @IBOutlet weak var lbl_timming: UILabel!
    @IBOutlet weak var btnRemove: UIButton!
    
    var didRemoveItem:(() -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func btn_removeDayTap(_ sender: UIButton) {
        didRemoveItem?()
    }
}
