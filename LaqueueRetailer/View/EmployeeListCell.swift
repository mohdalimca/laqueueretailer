//
//  EmployeeListCell.swift
//  LaqueueRetailer
//
//  Created by Adityaraj Singh Gaharwar on 03/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class EmployeeListCell: UITableViewCell {

    @IBOutlet weak var img_profile: UIImageView!
    @IBOutlet weak var lbl_empName: UILabel!
    @IBOutlet weak var lbl_empEmail: UILabel!
    @IBOutlet weak var lbl_queues: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
    
    func configure(_ employee:EmployeeList) {
        lbl_empName.text = "\(employee.firstname ?? "") \(employee.lastname ?? "")"
        lbl_empEmail.text = employee.userEmail
//        lbl_queues.text = "Q1"
        if employee.userRole == "2" {
            lbl_queues.text = "Owner"
        } else {
            lbl_queues.text = "Employee"
        }
        img_profile.kf.setImage(with: URL(string: employee.userPic ?? ""), options: [.transition(.fade(0.5))])
        lblStatus.backgroundColor = UIColor(hex: "33C851")
        if lbl_empName.text == "" {
           lbl_empName.text = "--"
            lblStatus.backgroundColor = UIColor(hex: "EEA11A")
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
