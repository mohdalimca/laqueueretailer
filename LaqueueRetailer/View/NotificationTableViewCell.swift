//
//  NotificationTableViewCell.swift
//  LaqueueRetailer
//
//  Created by Apple on 17/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblQueueStatus: UILabel!
    @IBOutlet weak var lblQueueName: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imageUser: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(notificaiton:NotificationList) {
        let status = setStatusTitle(notificaiton.purposeID ?? "")
        lblQueueStatus.textColor = setStatusColor(status)
        lblQueueStatus.text = status + "  ●"
        lblQueueName.text = notificaiton.queName
        lblTime.text = Date().timePassed(from: notificaiton.createdOn?.toDouble.dateFromTimestamp() ?? Date())
        lblName.text = notificaiton.notificationText
        imageUser.kf.indicatorType = .activity
        imageUser.kf.setImage(with: URL(string: notificaiton.userPic ?? ""), options: [.transition(.fade(0.5))])
    }
    
    private func setStatusColor(_ status:String) -> UIColor {
        let bookingStatus = BookingStatus(rawValue: status)
        switch bookingStatus {
        case .Acknowledge:
            return #colorLiteral(red: 1, green: 0.7529411765, blue: 0, alpha: 1)
        case .Update:
            return #colorLiteral(red: 0.9294117647, green: 0.631372549, blue: 0.1019607843, alpha: 1)
        case .Call, .Accept:
            return #colorLiteral(red: 0.2, green: 0.7843137255, blue: 0.3176470588, alpha: 1)
        case .Cancel, .Noshow:
            return #colorLiteral(red: 0.937254902, green: 0.1607843137, blue: 0, alpha: 1)
        default: // New
            return #colorLiteral(red: 0.231372549, green: 0.2705882353, blue: 0.3098039216, alpha: 1)
        }
    }
    
    private func setStatusTitle(_ id:String) -> String {
        switch id {
        case "1": return "New"
        case "2": return "Update"
        case "3": return "Acknowledge"
        case "4": return "Noshow"
        case "5": return "Call"
        case "6": return "Cancel"
        case "7": return "Accept"
        default: return ""
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
