//
//  QueueListCell.swift
//  LaqueueRetailer
//
//  Created by Adityaraj Singh Gaharwar on 04/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class QueueListCell: UITableViewCell {

    @IBOutlet weak var lbl_queueNum: UILabel!
    @IBOutlet weak var lbl_queueDescp: UILabel!
    @IBOutlet weak var lbl_queueStatus: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }    
}
