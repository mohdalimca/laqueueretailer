//
//  EmployeeNameCell.swift
//  LaqueueRetailer
//
//  Created by Apple on 17/09/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class EmployeeNameCell: UITableViewCell {
    
    @IBOutlet weak var lbl_empName: UILabel!
    
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(_ employee:EmployeeList?) {
        if let emp = employee {
            lbl_empName.text = "\(emp.firstname ?? "") \(emp.lastname ?? "")"
            self.accessoryType = (emp.isSelected == true) ? .checkmark : .none
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
//        self.accessoryType = selected ? .checkmark : .none
    }
}
