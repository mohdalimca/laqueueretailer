//
//  HomeQueueCell.swift
//  LaqueueRetailer
//
//  Created by Adityaraj Singh Gaharwar on 01/09/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class HomeQueueCell: UITableViewCell {

    @IBOutlet weak var img_profile: UIImageView!
    @IBOutlet weak var lbl_time: UILabel!
    @IBOutlet weak var lbl_waiting: UILabel!
    @IBOutlet weak var lbl_persons: UILabel!
    @IBOutlet weak var btn_top: UIButton!
    @IBOutlet weak var btn_bottom: UIButton!
    
    var btnTop:(() -> ())?
    var btnBottom:(() -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func btn_topTap(_ sender: UIButton) {
        btnTop?()
    }
    
    @IBAction func btn_bottomTap(_ sender: UIButton) {
        btnBottom?()
    }
    
}
