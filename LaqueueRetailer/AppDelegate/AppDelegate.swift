//
//  AppDelegate.swift
//  LaqueueRetailer
//
//  Created by Apple on 18/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import IQKeyboardManagerSwift
import UserNotifications
import Firebase
import FirebaseCore

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        Thread.sleep(forTimeInterval: 0)
        configureIQKeyboard()
        configureGoogleMap()
        configureMessaging()
        application.applicationIconBadgeNumber = 0
        UIApplication.shared.registerForRemoteNotifications()
        requestNotificationAuthorization(application: application)
        setNotification(application, launchOptions)
        setRootViewController()
        return true
    }
    
    private func setRootViewController() {
        if UserStore.userID != nil {
            let vc = RetailerStoryBoard.Authenticate(controller: Controller.MainContainer) as! MainContainerVC
            let rootViewController = RetailerStoryBoard.Authenticate(controller: "RootController") as! UINavigationController
            rootViewController.setViewControllers([vc], animated: false)
            self.window?.rootViewController = rootViewController
            
        } else {
            let vc = RetailerStoryBoard.Authenticate(controller: Controller.Login) as! LoginVC
            let rootViewController = RetailerStoryBoard.Authenticate(controller: "RootController") as! UINavigationController
            rootViewController.setViewControllers([vc], animated: false)
            self.window?.rootViewController = rootViewController
        }
    }
    
    private func configureIQKeyboard() {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = false
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.previousNextDisplayMode = .alwaysHide
    }
    
    private func configureMessaging() {
        Messaging.messaging().delegate = self
        Messaging.messaging().isAutoInitEnabled = true
    }
    
    private func configureGoogleMap() {
        GMSPlacesClient.provideAPIKey(GOOGLE_API_KEY)
        GMSServices.provideAPIKey(GOOGLE_API_KEY)
    }
    
    private func setNotification(_ application: UIApplication, _ launchOptions: [UIApplication.LaunchOptionsKey: Any]?) {
        if launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification] != nil {
            let data = launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification]
            print(data as Any)
        }
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(options: authOptions) { (_, _) in
                
            }
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        let topWindow: UIWindow? = UIWindow(frame: UIScreen.main.bounds)
        topWindow?.rootViewController = UIViewController()
        topWindow?.windowLevel = UIWindow.Level.alert + 1
        application.registerForRemoteNotifications()
    }
    
    // MARK: UISceneSession Lifecycle
    
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
}

extension AppDelegate: UNUserNotificationCenterDelegate {
    
    func requestNotificationAuthorization(application: UIApplication) {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(options: authOptions) { (_, _) in }
        } else {
            let settings: UIUserNotificationSettings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications()
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let token = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        print("Devcie token for apns ==== \(token)")
        //        Messaging.messaging().apnsToken = deviceToken
        UserStore.save(pushToken: token)
    }
    
    // iOS9, called when presenting notification in foreground
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        // Print notification payload data
        print("Push notification received: \(userInfo)")
        if let queueID = userInfo["que_id"] as? String {
            print("queue id is: \(queueID)")
            if application.applicationState != .background || application.applicationState !=  .inactive  {
                if let viewController = RetailerStoryBoard.Authenticate(controller: Controller.MainContainer) as? MainContainerVC {
                    viewController.notificationQueueID = queueID
                    self.window?.rootViewController =  UINavigationController.init(rootViewController: viewController)
                }
            }
        }
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        NSLog("Couldn't Register \(error)")
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .badge, .sound])
    }
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("Notification received")
        UIApplication.shared.applicationIconBadgeNumber = 0
        if #available(iOS 13.0, *) {
            guard let _ = (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.window?.rootViewController else {
                return
            }
            self.window = (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.window
            if let queueID = response.notification.request.content.userInfo["que_id"] as? String {
                print("queue id is: \(queueID)")
                navigateToPlaceDetail(queueID)
            }
        } else {
            if let queueID = response.notification.request.content.userInfo["que_id"] as? String {
                print("queue id is: \(queueID)")
                navigateToPlaceDetail(queueID)
            }
        }
        completionHandler()
    }
    
    private func navigateToPlaceDetail(_ queueID:String) {
        if let viewController = RetailerStoryBoard.Main(controller: Controller.PlaceDetail) as? PlaceDetailsVC {
            viewController.bookingId = queueID
            let rootViewController = UINavigationController(rootViewController: viewController)
            rootViewController.modalPresentationStyle = .fullScreen
            self.window?.rootViewController = rootViewController
            self.window?.makeKeyAndVisible()
        }
    }
}


extension AppDelegate: MessagingDelegate {
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        UserStore.save(fcmtoken: fcmToken)
        let dataDict:[String: String] = ["token": fcmToken]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
    }
}
