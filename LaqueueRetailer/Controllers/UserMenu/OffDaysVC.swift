//
//  OffDaysVC.swift
//  LaqueueRetailer
//
//  Created by Adityaraj Singh Gaharwar on 09/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class OffDaysVC: UIViewController {

    @IBOutlet weak var txt_date: UITextField!
    
    var datePicker = UIDatePicker()
    var dateCompletion:((_ date:String) -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    private func setup() {
        datePicker.minimumDate = Date()
        txt_date.delegate = self
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        view.addGestureRecognizer(tap)
    }
    
    @IBAction func btn_cancelTap(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btn_saveTap(_ sender: UIButton) {
        dateCompletion?(txt_date.text ?? "")
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension OffDaysVC: UITextFieldDelegate {
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        // handling code
//        self.dismiss(animated: true, completion: nil)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        createDatePicker()
    }
    
    // DatePicker
    func createDatePicker() {
        //toolbar
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        // bar button
        let flexButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let doneBtn = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donePressedDate))
        toolBar.setItems([flexButton, flexButton, doneBtn], animated: true)
        //assign toolbar
        txt_date.inputAccessoryView = toolBar
        //assign picker to textfield
        txt_date.inputView = datePicker
        //timepicker mode
        datePicker.datePickerMode = .date
        //        datePicker.locale = Locale(identifier: "en_gb")
    }
    
    @objc func donePressedDate() {
        //formatter
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.dateFormat = "dd-MM-yyyy"
        txt_date.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
}
