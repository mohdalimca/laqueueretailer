//
//  NewEmployeeVC.swift
//  LaqueueRetailer
//
//  Created by Adityaraj Singh Gaharwar on 04/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire
import Kingfisher

class NewEmployeeVC: UIViewController {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var img_profile: UIImageView!
    @IBOutlet weak var lbl_email: UILabel!
    @IBOutlet weak var txt_fName: UITextField!
    @IBOutlet weak var txt_lName: UITextField!
    @IBOutlet weak var txt_email: UITextField!
    @IBOutlet weak var txt_password: UITextField!
    @IBOutlet weak var txt_assignedQ: UITextField!
    @IBOutlet weak var txt_phoneNumber: UITextField!
    @IBOutlet weak var txt_dialCode: UITextField!
    @IBOutlet weak var view_email: UIView!
    @IBOutlet weak var view_queues: UIView!
    @IBOutlet weak var btn_empRole: UIButton!
    @IBOutlet weak var btn_ownerRole: UIButton!
    @IBOutlet weak var btnProfile: UIButton!
    var didUpdateUserInfo: ((_ image:UIImage) -> Void)?
    
    var imagePicker: ImagePicker!
    var isEdit = false
    var empId = ""
    var email = ""
    var name = ""
    var userPic = ""
    var firstName = ""
    var lastName = ""
    var jsonArray: [Any] = []
    var arrCountry: [String] = []
    var countryList = CountryList()
    var selectedCountry: Country!
    var role = ""
    var employee: EmployeeList!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        btnProfile.setProfileButtonImage()
    }
    
    private func setup() {
        txt_dialCode.delegate = self
        txt_email.isUserInteractionEnabled = true
        selectedCountry = Country(countryCode: "FR", phoneExtension: "33")
        txt_dialCode.text = countryCodeText()
        countryList.delegate = self
        loadCountryJsonList()
        if UserStore.profileStatus != 1 {
            lblTitle.text = "Edit Profile"
            getEmployeeDetail()
            return
        }
        //
        //        if UserStore.userRole == "3" { // for employee
        //            [view_email, view_queues].forEach {
        //                $0?.isHidden = true
        //            }
        //            getEmployeeDetail()
        //        }
        if isEdit {
            txt_email.isUserInteractionEnabled = false
            lblTitle.text = "Edit Profile"
            lbl_email.text = email
            lbl_name.text = name
            img_profile.kf.setImage(with: URL(string: userPic), options: [.transition(.fade(0.5))])
            txt_fName.text = firstName
            txt_lName.text = lastName
            txt_email.text = email
            populateDetails()
            return
        }
        NotificationCenter.default.addObserver(self, selector: #selector(updateProfile(_:)), name: .updateUserProfile, object: nil)
    }

    @objc func updateProfile(_ notification:Notification) {
        btnProfile.setProfileButtonImage()
    }
    
    private func populateDetails() {
        empId = employee.userID ?? ""
        selectedCountry = Country(countryCode: "", phoneExtension: employee.countryCode ?? "")
        txt_dialCode.text = countryCodeText()
        lbl_name.text = "\(employee.firstname ?? "") \(employee.lastname ?? "")"
        lbl_email.text = employee.userEmail
        img_profile.kf.setImage(with: URL(string: employee.userPic ?? ""), options: [.transition(.fade(0.5))])
        txt_fName.text = employee.firstname
        txt_lName.text = employee.lastname
        txt_email.text = employee.userEmail
        txt_phoneNumber.text = employee.userPhone
        (employee.userRole == "3") ? btn_roleTap(btn_empRole) : btn_roleTap(btn_ownerRole)
    }
    
    private func populateDetailsFromUserDefault() {
        let user = LoginSession.shared.getUserModelDefault(keyName: userData)
        lbl_name.text = "\(user?.data?.login?.firstname ?? "") \(user?.data?.login?.lastname ?? "")"
        lbl_email.text = user?.data?.login?.userEmail ?? ""
        img_profile.kf.setImage(with: URL(string: user?.data?.login?.userPic ?? ""), options: [.transition(.fade(0.5))])
        txt_fName.text = user?.data?.login?.firstname ?? ""
        txt_lName.text = user?.data?.login?.lastname ?? ""
        txt_email.text = user?.data?.login?.userEmail ?? ""
        txt_phoneNumber.text = ""
        (user?.data?.login?.userRole == "3") ? btn_roleTap(btn_empRole) : btn_roleTap(btn_ownerRole)
    }
    
    @IBAction func btn_profileTap(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btn_notificationTap(_ sender: UIButton) {
        navigateToNavigationView()
    }
    
    @IBAction func btn_profilePicTap(_ sender: UIButton) {
        imagePicker = ImagePicker(presentationController: self, delegate: self)
    }
    
    @IBAction func btn_saveTap(_ sender: UIButton) {
        if validateInputs() {
            if isEdit {
                updateEmployee()
            } else {
                addEmployee()
            }
        }
    }
    
    @IBAction func btn_cancelTap(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btn_roleTap(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            sender.setImage(#imageLiteral(resourceName: "radioBtn"), for: .normal)
            btn_ownerRole.setImage(#imageLiteral(resourceName: "radioUnchck"), for: .normal)
            role = "3"
        default:
            sender.setImage(#imageLiteral(resourceName: "radioBtn"), for: .normal)
            btn_empRole.setImage(#imageLiteral(resourceName: "radioUnchck"), for: .normal)
            role = "2"
        }
    }
    
    @IBAction func btnEyeAction(_ sender: UIButton) {
        sender.isSelected = (!sender.isSelected) ? true : false
        txt_password.isSecureTextEntry = !sender.isSelected
    }
}

extension NewEmployeeVC: ImagePickerDelegate, UITextFieldDelegate, CountryListDelegate {
    func selectedCountry(country: Country) {
        txt_dialCode.resignFirstResponder()
        selectedCountry = country
        txt_dialCode.text = countryCodeText()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
        present(countryList, animated: true, completion: nil)
    }
    
    private func loadCountryJsonList() {
        guard let path = Bundle.main.path(forResource: "country_list", ofType:"json") else {
            debugPrint( "path not found")
            return
        }
        do{
            let data = try Data(contentsOf: URL(fileURLWithPath: path))
            let json = try JSONSerialization.jsonObject(with: data, options: [])
            if let object = json as? [String : Any]  {
                print(object)
            }
        } catch {
            print(error.localizedDescription)
        }
    }
    
    private func countryCodeText() -> String {
        var strCountry = ""
        strCountry.append(selectedCountry.flag! + " ")
        strCountry.append(selectedCountry.countryCode + " +")
        strCountry.append(selectedCountry.phoneExtension)
        return strCountry
    }
    
    
    func didSelect(image: UIImage?) {
        guard let img = image else { return }
        img_profile.image = img
    }
    
    func validateInputs() -> Bool {
        var msg = ""
        if txt_fName.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            msg = "Please enter first name"
        } else if txt_lName.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            msg = "Please enter last name"
        } else if self.validate(YourEMailAddress: txt_email.text!) == false {
            msg = "Please enter email"
        } else if txt_phoneNumber.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            msg = "Please enter phone number"
        } else if txt_dialCode.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            msg = "Please enter dial code"
        } else if txt_password.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 && !isEdit {
            msg = "Please enter password"
        } else if img_profile.image == nil {
            msg = "Please select image"
        } else if role == "" {
            msg = "Please select role"
        }
        if msg == "" {
            return true
        } else {
            self.func_Alert(message: msg)
            return false
        }
    }
}

// MARK:- API Call
extension NewEmployeeVC {
    func updateEmployee() {
        var authorization = ""
        var busID = ""
        if let auth = LoginSession.shared.getUserModelDefault(keyName: userData)?.data?.login?.authorization, let id = LoginSession.shared.getUserModelDefault(keyName: userData)?.data?.login?.userid {
            authorization = auth
            busID = id
        }
        let header:[String:String] = [
            "Authorization":authorization,
            "device_type":"ios",
            "device_id":"111",
            "current_time_zone":TimeZone.current.identifier,
            "language":"en",
            "Content-Type":"application/json",
            "version":"1.0.0",
            "current_country":"India",
            "lat":"22.12541",
            "lng":"22.12541"
        ]
        
        let param:[String:Any] = [
            "firstname":txt_fName.text ?? "",
            "lastname":txt_lName.text ?? "",
            "country_code":selectedCountry.phoneExtension,
            "email":txt_email.text ?? "",
            "user_phone":txt_phoneNumber.text ?? "",
            "emp_id": empId
        ]
        Loader.shared.showIndicator(view: self.view)
        K3Webservice.run(isMultipart: true, urlString: WebUrl.updateEmployee, method: .post, Param: param, header: header, file: ["user_pic" : img_profile.image!], type: NewPasswordModel.self, viewController: self, fullAccess: true, encoding: K3encoding.JSON) { (response) in
            if let isResponse = response {
                if isResponse.status == 1 {
                    self.didUpdateUserInfo?(self.img_profile.image ?? UIImage())
                    self.func_AlertWithOneOption(message: isResponse.message ?? "", optionOne: "OK") {
                        NotificationCenter.default.post(name: .reloadEmployeeList, object: nil)
                        self.navigationController?.popViewController(animated: true)
                    }
                } else {
                    self.func_Alert(message: isResponse.message ?? "")
                }
            }
        }
    }
    
    func addEmployee() {
        var authorization = ""
        var busID = ""
        if let auth = LoginSession.shared.getUserModelDefault(keyName: userData)?.data?.login?.authorization, let id = LoginSession.shared.getUserModelDefault(keyName: userData)?.data?.login?.userid {
            authorization = auth
            busID = id
        }
        let header:[String:String] = [
            "Authorization":authorization,
            "device_type":"ios",
            "device_id":"111",
            "current_time_zone":TimeZone.current.identifier,
            "language":"en",
            "Content-Type":"application/json",
            "version":"1.0.0",
            "current_country":"India",
            "lat":"22.12541",
            "lng":"22.12541"
        ]
        let param:[String:Any] = [
            "firstname":txt_fName.text ?? "",
            "lastname":txt_lName.text ?? "",
            "country_code":selectedCountry.phoneExtension,
            "email":txt_email.text ?? "",
            "password": txt_password.text ?? "",
            "user_phone":txt_phoneNumber.text ?? "",
            "user_id": busID,
            "type":role
        ]
        Loader.shared.showIndicator(view: self.view)
        K3Webservice.run(isMultipart: true, urlString: WebUrl.addEmployee, method: .post, Param: param, header: header, file: ["user_pic" : img_profile.image!], type: NewPasswordModel.self, viewController: self, fullAccess: true, encoding: K3encoding.JSON) { (response) in
            if let isResponse = response {
                if isResponse.status == 1 {
                    self.didUpdateUserInfo?(self.img_profile.image ?? UIImage())
                    self.func_AlertWithOneOption(message: isResponse.message ?? "", optionOne: "OK") {
                        UserStore.save(profileStatus: 1)
                        NotificationCenter.default.post(name: .reloadEmployeeList, object: nil)
                        self.navigationController?.popViewController(animated: true)
                    }
                } else {
                    self.func_Alert(message: isResponse.message ?? "")
                }
            }
        }
    }
    
    func getEmployeeDetail() {
        var authorization = ""
        var userId = ""
        var busCode = ""
        if let auth = LoginSession.shared.getUserModelDefault(keyName: userData)?.data?.login?.authorization, let uid = LoginSession.shared.getUserModelDefault(keyName: userData)?.data?.login?.userid, let bCode = LoginSession.shared.getUserModelDefault(keyName: userData)?.data?.login?.busiCode {
            authorization = auth
            userId = uid
            busCode = bCode
        }
        let header:[String:String] = [
            "Authorization":authorization,
            "device_type":"ios",
            "device_id":"111",
            "current_time_zone":TimeZone.current.identifier,
            "language":"en",
            "Content-Type":"application/json",
            "version":"1.0.0",
            "current_country":"India",
            "lat":"22.12541",
            "lng":"22.12541"
        ]
        let param:[String:Any] = [
            "user_id": userId,
            "busi_code": busCode
        ]
        Loader.shared.showIndicator(view: self.view)
        K3Webservice.run(urlString: WebUrl.employeeDetail, method: HTTPMethod.post, Param: param, header: header , type: EmployeeDetailModel.self, viewController: self) { (response) in
            if let isResponse = response {
                Loader.shared.hideIndicator()
                if isResponse.status == 1 {
                    self.showProfileDetails(isResponse)
                } else {
                    self.func_Alert(message: isResponse.message ?? "")
                }
            }
        }
    }
    
    private func showProfileDetails(_ isResponse:EmployeeDetailModel) {
        empId = isResponse.data?.employeeDetail?.id ?? ""
        lbl_name.text = "\(isResponse.data?.employeeDetail?.firstname ?? "") \(isResponse.data?.employeeDetail?.lastname ?? "")"
        lbl_email.text = isResponse.data?.employeeDetail?.userEmail ?? ""
        img_profile.kf.setImage(with: URL(string: isResponse.data?.employeeDetail?.userPic ?? ""), options: [.transition(.fade(0.5))])
        txt_fName.text = isResponse.data?.employeeDetail?.firstname ?? ""
        txt_lName.text = isResponse.data?.employeeDetail?.lastname ?? ""
        txt_email.text = isResponse.data?.employeeDetail?.userEmail ?? ""
        txt_phoneNumber.text = isResponse.data?.employeeDetail?.userPhone ?? ""
        (isResponse.data?.employeeDetail?.userRole == "3") ? btn_roleTap(btn_empRole) : btn_roleTap(btn_ownerRole)
    }
}
