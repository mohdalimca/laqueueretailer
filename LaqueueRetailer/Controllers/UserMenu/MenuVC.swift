//
//  MenuVC.swift
//  LaqueueRetailer
//
//  Created by Adityaraj Singh Gaharwar on 03/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire

class MenuVC: UIViewController {

    @IBOutlet weak var viewEmployee: UIView!
    @IBOutlet weak var viewPreference: UIView!
    @IBOutlet weak var lbl_details: UILabel!
    @IBOutlet weak var view_details: UIView!
    @IBOutlet weak var btnProfile: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        btnProfile.setProfileButtonImage()
    }
        
    private func setup() {
        view_details.isHidden = true
        lbl_details.text = "Details"
        if UserStore.userRole == "3" {
            [viewPreference].forEach {
                $0?.isHidden = true
            }
//            view_details.isHidden = false
//            lbl_details.text = "Business Details"
        }
        NotificationCenter.default.addObserver(self, selector: #selector(updateProfile(_:)), name: .updateUserProfile, object: nil)
    }
    
    @objc func updateProfile(_ notification:Notification) {
        btnProfile.setProfileButtonImage()
    }

    @IBAction func btn_profileTap(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btn_notificationTap(_ sender: UIButton) {
        navigateToNavigationView()
    }
    
    @IBAction func btn_menuOptTap(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            self.navigationController?.pushViewController(RetailerStoryBoard.Profile(controller: Controller.MyProfile), animated: true)
        case 1:
            self.navigationController?.pushViewController(RetailerStoryBoard.Profile(controller: Controller.QueueList), animated: true)
        case 2:
            self.navigationController?.pushViewController(RetailerStoryBoard.Profile(controller: Controller.EmployeeList), animated: true)
        case 4:
            self.navigationController?.pushViewController(RetailerStoryBoard.Profile(controller: Controller.NewEmployee), animated: true)
//            self.navigationController?.pushViewController(RetailerStoryBoard.Profile(controller: Controller.ChangePassword), animated: true)
        default:
            self.navigationController?.pushViewController(RetailerStoryBoard.Profile(controller: Controller.Prefrences), animated: true)
        }
    }
    
    @IBAction func btn_logoutTap(_ sender: UIButton) {
        logoutUser()
    }
    
}

extension MenuVC {
    func logoutUser() {
        var authorization = ""
        if let auth = LoginSession.shared.getUserModelDefault(keyName: userData)?.data?.login?.authorization {
            authorization = auth
        }
        let header:[String:String] = [
            "Authorization":authorization,
            "device_type":"ios",
            "device_id":"111",
            "current_time_zone":TimeZone.current.identifier,
            "language":"en",
            "Content-Type":"application/json",
            "version":"1.0.0",
            "current_country":"India",
            "lat":"22.12541",
            "lng":"22.12541"
        ]
        Loader.shared.showIndicator(view: self.view)
        K3Webservice.run(urlString: WebUrl.logout, method: HTTPMethod.get, Param: nil, header: header , type: NewPasswordModel.self, viewController: self) { (response) in
            if let isResponse = response {
                if isResponse.status == 1 {
                    Loader.shared.hideIndicator()
                    self.func_AlertWithOneOption(message: isResponse.message ?? "", optionOne: "Ok") {
                        LoginSession.shared.updateLoginStatus(false)
                        LoginSession.shared.clearUserDefault()
                        if let wndw = UIApplication.shared.windows.first {
                            if let isNc = RetailerStoryBoard.Authenticate(controller: "RootController") as? UINavigationController {
                                let oldState = UIView.areAnimationsEnabled
                                UIView.setAnimationsEnabled(false)
                                UIView.transition(with: wndw, duration: 0.5, options: [.transitionCrossDissolve], animations: {
                                    wndw.rootViewController = isNc
                                }, completion: { (isDone) in
                                    UIView.setAnimationsEnabled(oldState)
                                })
                            }
                        }
                    }
                } else {
                    Loader.shared.hideIndicator()
                    self.func_Alert(message: isResponse.message ?? "")
                }
            }
        }
    }
}
