//
//  QueueListVC.swift
//  LaqueueRetailer
//
//  Created by Adityaraj Singh Gaharwar on 04/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire

class QueueListVC: UIViewController {
    
    @IBOutlet weak var tblView_queueList: UITableView!
    @IBOutlet weak var btnProfile: UIButton!
    @IBOutlet weak var viewNoData: UIView! {
        didSet {
            viewNoData.isHidden = true
        }
    }
    
    var spinner: UIActivityIndicatorView!
    lazy var refresh: UIRefreshControl = {
        let control = UIRefreshControl()
        control.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        return control
    }()
    
    var currentPageNo: Int = 0
    var totalCount: Int = 0
    var queueData = [Myqueue]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        btnProfile.setProfileButtonImage()
    }
    
    private func setup() {
        configureTableView()
        registerXib()
        getQueuelist()
        NotificationCenter.default.addObserver(self, selector: #selector(updateProfile(_:)), name: .updateUserProfile, object: nil)
    }
    
    @objc func updateProfile(_ notification:Notification) {
        btnProfile.setProfileButtonImage()
    }
    
    private func showTableView(_ show:Bool) {
        tblView_queueList.isHidden = !show
        viewNoData.isHidden = show
    }
    
    private func didUpdateQueueList() {
        getQueuelist()
    }
    
    private func refreshTable() {
        tblView_queueList.refreshControl?.endRefreshing()
        currentPageNo = 0
        getQueuelist()
    }
    
    private func loadMore() {
        //currentPageNo += 1
        //getQueuelist()
    }
    
    @IBAction func btn_profileTap(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btn_notificationTap(_ sender: UIButton) {
        navigateToNavigationView()
    }
    
    @IBAction func btn_addQueueTap(_ sender: UIButton) {
        let newQueueVC = RetailerStoryBoard.Profile(controller: Controller.NewQueue) as! NewQueueVC
        newQueueVC.didUpdateQueueList = didUpdateQueueList
        self.navigationController?.pushViewController(newQueueVC, animated: true)
    }
    
    private func navigateToQueueDetail(_ index:Int) {
        let queueStatusVC = RetailerStoryBoard.Profile(controller: Controller.QueueStatus) as! QueueStatusVC
        queueStatusVC.didUpdateQueueList = didUpdateQueueList
        queueStatusVC.myQueue = queueData[index]
        self.navigationController?.pushViewController(queueStatusVC, animated: true)
    }
}
extension QueueListVC {
    @objc func refresh(_ control:UIRefreshControl) {
        self.refreshTable()
    }
    
    private func configureTableView() {
        tblView_queueList.dataSource = self
        tblView_queueList.delegate = self
        tblView_queueList.estimatedRowHeight = 70.0
        tblView_queueList.rowHeight = UITableView.automaticDimension
        tblView_queueList.refreshControl = refresh
        self.addSpinner()
    }
    
    private func addSpinner() {
        spinner = UIActivityIndicatorView(style: .gray)
        spinner.frame = CGRect(x: 0, y: 0, width: tblView_queueList.bounds.size.width, height: 44)
        tblView_queueList.tableFooterView = spinner
        tblView_queueList.tableFooterView?.isHidden = true
    }
}

extension QueueListVC: UITableViewDelegate, UITableViewDataSource {
    func registerXib() {
        tblView_queueList.register(UINib(nibName: Cell.QueueList, bundle: nil), forCellReuseIdentifier: Cell.QueueList)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return queueData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Cell.QueueList) as! QueueListCell
        cell.lbl_queueNum.text = queueData[indexPath.row].queName ?? ""
        cell.lbl_queueDescp.text = queueData[indexPath.row].queDescription ?? ""
        if queueData[indexPath.row].status == "Play" {
            cell.lbl_queueStatus.backgroundColor = #colorLiteral(red: 0.2, green: 0.7843137255, blue: 0.3176470588, alpha: 1)
        } else {
            cell.lbl_queueStatus.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UIScreen.main.bounds.width / 5
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        navigateToQueueDetail(indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if queueData.count < totalCount && indexPath.row == queueData.count - 1 && totalCount > 10 {
            spinner.startAnimating()
            tableView.tableFooterView?.isHidden = false
        } else {
            spinner.stopAnimating()
            tableView.tableFooterView?.isHidden = true
        }
    }
}

extension QueueListVC {
    func getQueuelist() {
        var authorization = ""
        if let auth = LoginSession.shared.getUserModelDefault(keyName: userData)?.data?.login?.authorization {
            authorization = auth
            
        }
        let header:[String:String] = [
            "Authorization":authorization,
            "device_type":"ios",
            "device_id":"111",
            "current_time_zone":TimeZone.current.identifier,
            "language":"en",
            "Content-Type":"application/json",
            "version":"1.0.0",
            "current_country":"India",
            "lat":"22.12541",
            "lng":"22.12541"
        ]
        Loader.shared.showIndicator(view: self.view)
        K3Webservice.run(urlString: WebUrl.getQueues, method: HTTPMethod.get, Param: nil, header: header , type: QueueListModel.self, viewController: self) { (response) in
            if let isResponse = response {
                if isResponse.status == 1 {
                    Loader.shared.hideIndicator()
                    self.showTableView(true)
                    self.queueData = isResponse.data?[0].myqueue ?? []
                    self.totalCount = isResponse.data?[0].totalcount?.toInt ?? self.totalCount
                    self.tblView_queueList.reloadSections(IndexSet(integer: 0), with: .fade)
                } else {
                    Loader.shared.hideIndicator()
                    self.showTableView(false)
                    self.func_Alert(message: isResponse.message ?? "")
                }
            }
        }
    }
}
