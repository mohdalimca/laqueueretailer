//
//  NotificationViewController.swift
//  LaqueueRetailer
//
//  Created by Apple on 17/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class NotificationViewController: UIViewController {

    @IBOutlet weak var btnProfile:UIButton!
    @IBOutlet weak var btnNotification:UIButton!
    @IBOutlet weak var tableView:UITableView!
    
    var spinner: UIActivityIndicatorView!
    lazy var refresh: UIRefreshControl = {
        let control = UIRefreshControl()
        control.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        return control
    }()
    
    var notificationList = [NotificationList]()
    var currentPageNo: Int = 0
    var totalCount: Int = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        btnProfile.setProfileButtonImage()
    }
    
    // MARK:- Instance Methods
    private func setup() {
        Loader.shared.showIndicator(view: self.view)
        [btnProfile, btnNotification].forEach {
            $0?.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
        }
        tableView.register(UINib(nibName: Cell.NotificationCell, bundle: nil), forCellReuseIdentifier: Cell.NotificationCell)
        configureTableView()
        getNotificationList()
    }
    
    @objc func refresh(_ control:UIRefreshControl) {
        self.refreshTable()
    }
    
    private func configureTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.estimatedRowHeight = 70.0
        tableView.rowHeight = UITableView.automaticDimension
        tableView.refreshControl = refresh
        self.addSpinner()
    }
    
    private func addSpinner() {
        spinner = UIActivityIndicatorView(style: .gray)
        spinner.frame = CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 44)
        tableView.tableFooterView = spinner
        tableView.tableFooterView?.isHidden = true
    }
    
    private func refreshTable() {
        tableView.refreshControl?.endRefreshing()
        currentPageNo = 0
        getNotificationList()
    }
    
    private func loadMore() {
        currentPageNo += 1
        getNotificationList()
    }
    
    @objc func buttonPressed(_ sender:UIButton) {
        switch sender {
        case btnProfile:
            self.navigationController?.popViewController(animated: true)
        case btnNotification:
            break
        default:
            break
        }
    }
    
    fileprivate func navigateToDetailView(_ index:Int) {
        if let detailVC = RetailerStoryBoard.Main(controller: Controller.PlaceDetail) as? PlaceDetailsVC {
            detailVC.bookingId = notificationList[index].bookingID ?? ""
            self.navigationController?.pushViewController(detailVC, animated: true)
        }
    }
}

extension NotificationViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notificationList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Cell.NotificationCell) as! NotificationTableViewCell
        cell.configure(notificaiton: notificationList[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        navigateToDetailView(indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if notificationList.count < totalCount && indexPath.row == notificationList.count - 1 && totalCount > 20 {
            spinner.startAnimating()
            tableView.tableFooterView?.isHidden = false
            loadMore()
        } else {
            spinner.stopAnimating()
            tableView.tableFooterView?.isHidden = true
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

// MARK:- API Call
extension NotificationViewController {
    func getNotificationList() {
        var authorization = ""
        if let auth = LoginSession.shared.getUserModelDefault(keyName: userData)?.data?.login?.authorization {
            authorization = auth
            
        }
        let header:[String:String] = [
            "Authorization":authorization,
            "device_type":"ios",
            "device_id":"111",
            "current_time_zone":TimeZone.current.identifier,
            "language":"en",
            "Content-Type":"application/json",
            "version":"1.0.0",
            "current_country":"India",
            "lat":"22.12541",
            "lng":"22.12541"
        ]
        
        let param:[String:Any] = [
            "page_no": String(currentPageNo)
        ]
        K3Webservice.run(urlString: WebUrl.notifications, method: .post, Param: param, header: header , type: NotificationModel.self, viewController: self) { (response) in
            if let isResponse = response {
                if isResponse.status == 1 {
                    Loader.shared.hideIndicator()
                    self.notificationList = isResponse.data?.first?.notification ?? self.notificationList
                    self.totalCount = isResponse.data?.first?.totalcount?.toInt ?? self.totalCount
                    self.tableView.reloadSections(IndexSet(integer: 0), with: .fade)
                } else {
                    Loader.shared.hideIndicator()
                    self.func_Alert(message: isResponse.message ?? "")
                }
            }
        }
    }
}

