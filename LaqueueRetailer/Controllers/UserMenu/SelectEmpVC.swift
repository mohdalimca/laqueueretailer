//
//  SelectEmpVC.swift
//  LaqueueRetailer
//
//  Created by Adityaraj Singh Gaharwar on 27/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire

class SelectEmpVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tblView_emp: UITableView!
    
    var selectedEmployeesID = ""
    var arrEmployess = [EmployeeList]()
    var arrSelectEmp:[(id: String, name: String)] = []
    var sendData:(([(id: String, name: String)]) -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    private func setup() {
        tblView_emp.tableFooterView = UIView(frame: CGRect.zero)
        registerXib()
        getEmployees()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        for emp in arrEmployess {
            if emp.isSelected == true {
                arrSelectEmp.append((id: emp.empID ?? "" , name: "\(emp.firstname ?? "") \(emp.lastname ?? "")"))
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        sendData?(arrSelectEmp)
    }
    
    func registerXib() {
        tblView_emp.register(UINib(nibName: Cell.EmployeeNameCell, bundle: nil), forCellReuseIdentifier: Cell.EmployeeNameCell)
    }

    
    @IBAction func btn_cancelTap(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrEmployess.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EmployeeNameCell") as! EmployeeNameCell
        cell.selectionStyle = .none
        cell.configure(arrEmployess[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UIScreen.main.bounds.width / 6
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
        if arrEmployess[indexPath.row].isSelected == nil ||
            arrEmployess[indexPath.row].isSelected == false {
            arrEmployess[indexPath.row].isSelected = true
        } else {
            arrEmployess[indexPath.row].isSelected = false
        }
        
        tableView.reloadRows(at: [indexPath], with: .fade)
        
//        arrSelectEmp.append((id: arrEmployess[indexPath.row].empID ?? "" , name: "\(arrEmployess[indexPath.row].firstname ?? "") \(arrEmployess[indexPath.row].lastname ?? "")"))
//        print(arrSelectEmp)
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        print(indexPath.row)
//        arrSelectEmp.remove(at: arrSelectEmp.firstIndex(where: { $0 == (id: arrEmployess[indexPath.row].empID ?? "" , name: "\(arrEmployess[indexPath.row].firstname ?? "") \(arrEmployess[indexPath.row].lastname ?? "")")})!)
    }
}

// MARK:- API Call
extension SelectEmpVC {
    func getEmployees() {
        var authorization = ""
        var busCode = ""
        if let auth = LoginSession.shared.getUserModelDefault(keyName: userData)?.data?.login?.authorization, let bCode = LoginSession.shared.getUserModelDefault(keyName: userData)?.data?.login?.busiCode {
            authorization = auth
            busCode = bCode
        }
        let header:[String:String] = [
            "Authorization":authorization,
            "device_type":"ios",
            "device_id":"111",
            "current_time_zone":TimeZone.current.identifier,
            "language":"en",
            "Content-Type":"application/json",
            "version":"1.0.0",
            "current_country":"India",
            "lat":"22.12541",
            "lng":"22.12541"
        ]
        let param:[String:Any] = [
            "page_no":"0",
            "busi_code":busCode
        ]
        Loader.shared.showIndicator(view: self.view)
        K3Webservice.run(urlString: WebUrl.getEmployeeList, method: HTTPMethod.post, Param: param, header: header , type: EmployeesModel.self, viewController: self) { (response) in
            if let isResponse = response {
                if isResponse.status == 1 {
                    Loader.shared.hideIndicator()
                    self.arrEmployess = isResponse.data?[0].employeeList ?? []
                    self.reloadEmployeeList()
                } else {
                    Loader.shared.hideIndicator()
                    self.func_Alert(message: isResponse.message ?? "")
                }
            }
        }
    }
    
    private func reloadEmployeeList() {
        if selectedEmployeesID != "" || !arrEmployess.isEmpty {
            let arrID = selectedEmployeesID.components(separatedBy: ",")
            for i in 0..<arrEmployess.count {
                var emp = arrEmployess[i]
                for selectedID in arrID {
                    if emp.empID == selectedID {
                        emp.isSelected = true
                        arrEmployess[i] = emp
                    }
                }
                if emp.userID == UserStore.userID {
                    emp.isSelected = true
                    arrEmployess[i] = emp
                }
            }
        }
        self.tblView_emp.reloadSections(IndexSet(integer: 0), with: .fade)
    }
}
