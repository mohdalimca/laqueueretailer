//
//  EmployeeListVC.swift
//  LaqueueRetailer
//
//  Created by Adityaraj Singh Gaharwar on 04/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire
import Kingfisher

class EmployeeListVC: UIViewController {
    
    @IBOutlet weak var tblView_empList: UITableView!
    @IBOutlet weak var btnProfile: UIButton!
    
    var spinner: UIActivityIndicatorView!
    lazy var refresh: UIRefreshControl = {
        let control = UIRefreshControl()
        control.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        return control
    }()
    
    var currentPageNo: Int = 0
    var totalCount: Int = 0
    
    var arrEmployess = [EmployeeList]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        btnProfile.setProfileButtonImage()
    }
    
    private func setup() {
        configureTableView()
        registerXib()
        getEmployeeList()
        NotificationCenter.default.addObserver(self, selector: #selector(updateProfile(_:)), name: .updateUserProfile, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadEmployeeList(_:)), name: .reloadEmployeeList, object: nil)
    }
        
    private func didUpdateEmployeeList() { }
    
    @objc func reloadEmployeeList(_ notification:Notification) {
        refreshTable()
    }
    
    @objc func updateProfile(_ notification:Notification) {
        btnProfile.setProfileButtonImage()
    }
    
    private func refreshTable() {
        tblView_empList.refreshControl?.endRefreshing()
        currentPageNo = 0
        getEmployeeList()
    }
    
    private func loadMore() {
        currentPageNo += 1
        getEmployeeList()
    }
    
    @IBAction func btn_profileTap(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btn_notificationTap(_ sender: UIButton) {
        navigateToNavigationView()
    }
    
    @IBAction func btn_addEmpTap(_ sender: UIButton) {
        guard let vc = RetailerStoryBoard.Profile(controller: Controller.NewEmployee) as? NewEmployeeVC else{ return }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    private func navigateEmployeeDetails(_ index:Int) {
        guard let vc = RetailerStoryBoard.Profile(controller: Controller.Employee) as? EmployeeVC else{return}
        vc.employee = self.arrEmployess[index]
        vc.didUpdateEmployeeList = didUpdateEmployeeList
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension EmployeeListVC {
    @objc func refresh(_ control:UIRefreshControl) {
        self.refreshTable()
    }
    
    private func configureTableView() {
        tblView_empList.dataSource = self
        tblView_empList.delegate = self
        tblView_empList.estimatedRowHeight = 70.0
        tblView_empList.rowHeight = UITableView.automaticDimension
        tblView_empList.refreshControl = refresh
        self.addSpinner()
    }
    
    private func addSpinner() {
        spinner = UIActivityIndicatorView(style: .gray)
        spinner.frame = CGRect(x: 0, y: 0, width: tblView_empList.bounds.size.width, height: 44)
        tblView_empList.tableFooterView = spinner
        tblView_empList.tableFooterView?.isHidden = true
    }
}

extension EmployeeListVC: UITableViewDataSource, UITableViewDelegate {
    
    func registerXib() {
        tblView_empList.register(UINib(nibName: Cell.EmployeeList, bundle: nil), forCellReuseIdentifier: Cell.EmployeeList)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrEmployess.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Cell.EmployeeList) as! EmployeeListCell
        if arrEmployess[indexPath.row].userID == UserStore.userID {
            UserStore.save(userPic: arrEmployess[indexPath.row].userPic)
        }
        cell.lbl_empName.text = "\(arrEmployess[indexPath.row].firstname ?? "") \(arrEmployess[indexPath.row].lastname ?? "")"
        cell.lbl_empEmail.text = arrEmployess[indexPath.row].userEmail
        cell.lbl_queues.text = (arrEmployess[indexPath.row].userRole == "2") ? "Owner" : "Employee"
        cell.img_profile.kf.setImage(with: URL(string: arrEmployess[indexPath.row].userPic ?? ""), options: [.transition(.fade(0.5))])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UIScreen.main.bounds.width / 3.5
//        return 85
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if UserStore.userRole == "2" { // for admin
            if UserStore.userRole == arrEmployess[indexPath.row].userRole &&
                UserStore.userID == arrEmployess[indexPath.row].userID {
                self.navigateEmployeeDetails(indexPath.row)
            } else if UserStore.userRole == arrEmployess[indexPath.row].userRole &&
                UserStore.userID != arrEmployess[indexPath.row].userID {
                self.func_Alert(message: "Can not access other owner profile")
            } else {
                self.navigateEmployeeDetails(indexPath.row)
            }
        } else { // for employee
            if UserStore.userRole == arrEmployess[indexPath.row].userRole &&
                UserStore.userID == arrEmployess[indexPath.row].userID {
                self.navigateEmployeeDetails(indexPath.row)
            } else {
                self.func_Alert(message: "Can not access others profile")
            }
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if arrEmployess.count < totalCount && indexPath.row == arrEmployess.count - 1 && totalCount > 10 {
            spinner.startAnimating()
            tableView.tableFooterView?.isHidden = false
        } else {
            spinner.stopAnimating()
            tableView.tableFooterView?.isHidden = true
        }
    }
}

// MARK:- API Call
extension EmployeeListVC {
    func getEmployeeList() {
        var authorization = ""
        
        if let auth = LoginSession.shared.getUserModelDefault(keyName: userData)?.data?.login?.authorization {
            authorization = auth
            
        }
        let header:[String:String] = [
            "Authorization":authorization,
            "device_type":"ios",
            "device_id":"111",
            "current_time_zone":TimeZone.current.identifier,
            "language":"en",
            "Content-Type":"application/json",
            "version":"1.0.0",
            "current_country":"India",
            "lat":"22.12541",
            "lng":"22.12541"
        ]
        let param:[String:Any] = [
            "page_no":String(currentPageNo),
            "busi_code": UserStore.businessCode!
        ]
        Loader.shared.showIndicator(view: self.view)
        K3Webservice.run(urlString: WebUrl.getEmployeeList, method: HTTPMethod.post, Param: param, header: header , type: EmployeesModel.self, viewController: self) { (response) in
            self.tblView_empList.refreshControl?.endRefreshing()
            if let isResponse = response {
                if isResponse.status == 1 {
                    Loader.shared.hideIndicator()
                    self.arrEmployess = isResponse.data?[0].employeeList ?? []
                    self.totalCount = isResponse.data?[0].totalcount ?? self.totalCount
                    self.tblView_empList.reloadSections(IndexSet(integer: 0), with: .fade)
                } else {
                    Loader.shared.hideIndicator()
                    self.func_Alert(message: isResponse.message ?? "")
                }
            }
        }
    }
}
