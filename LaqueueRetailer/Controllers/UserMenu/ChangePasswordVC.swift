//
//  ChangePasswordVC.swift
//  LaqueueRetailer
//
//  Created by Adityaraj Singh Gaharwar on 06/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire

class ChangePasswordVC: UIViewController {

    @IBOutlet weak var btnProfile: UIButton!
    @IBOutlet weak var txt_oldPassword: UITextField!
    @IBOutlet weak var txt_newPassword: UITextField!
    @IBOutlet weak var txt_confirmPass: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        btnProfile.setProfileButtonImage()
    }
    
    private func setup() {
        NotificationCenter.default.addObserver(self, selector: #selector(updateProfile(_:)), name: .updateUserProfile, object: nil)
    }

    @objc func updateProfile(_ notification:Notification) {
          btnProfile.setProfileButtonImage()
    }
    
    @IBAction func btn_saveTap(_ sender: UIButton) {
        if validateInputs() {
            changePassword()
        }
    }
    
    @IBAction func btn_profileTap(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btn_notificationTap(_ sender: UIButton) {
        navigateToNavigationView()
    }
}

extension ChangePasswordVC {
    
    func validateInputs() -> Bool {
        var msg = ""
        if txt_oldPassword.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            msg = "Please enter old password"
        } else if txt_newPassword.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            msg = "Please enter new password"
        } else if txt_confirmPass.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            msg = "Please enter confirm password"
        } else if txt_confirmPass.text != txt_newPassword.text {
            msg = "Passwords doesn't match"
        }
        if msg == "" {
            return true
        } else {
            self.func_Alert(message: msg)
            return false
        }
    }
    
    func changePassword() {
        var authorization = ""
        if let auth = LoginSession.shared.getUserModelDefault(keyName: userData)?.data?.login?.authorization {
            authorization = auth
        }
        let header:[String:String] = [
            "Authorization":authorization,
            "device_type":"ios",
            "device_id":"111",
            "current_time_zone":TimeZone.current.identifier,
            "language":"en",
            "Content-Type":"application/json",
            "version":"1.0.0",
            "current_country":"India",
            "lat":"22.12541",
            "lng":"22.12541"
        ]
        let param:[String:Any] = [
            "old_password" : txt_oldPassword.text ?? "",
            "new_password" : txt_confirmPass.text ?? ""
        ]
        Loader.shared.showIndicator(view: self.view)
        K3Webservice.run(urlString: WebUrl.changePassword, method: HTTPMethod.post, Param: param, header: header , type: NewPasswordModel.self, viewController: self) { (response) in
            if let isResponse = response {
                if isResponse.status == 1 {
                    Loader.shared.hideIndicator()
                    self.func_AlertWithOneOption(message: isResponse.message ?? "", optionOne: "Ok") {
                        self.navigationController?.popViewController(animated: true)
                    }
                } else {
                    Loader.shared.hideIndicator()
                    self.func_Alert(message: isResponse.message ?? "")
                }
            }
        }
    }
}
