//
//  EditQueueVC.swift
//  LaqueueRetailer
//
//  Created by Apple on 31/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class EditQueueVC: UIViewController {
    
    @IBOutlet weak var lblEmployees: UILabel!
    @IBOutlet weak var txt_name: UITextField!
    @IBOutlet weak var txtView_descp: UITextView!
    @IBOutlet var btn_days: [UIButton]!
    @IBOutlet var txt_timmings: [UITextField]!
    @IBOutlet weak var tblView_customDays: UITableView!
    @IBOutlet weak var tblView_offdays: UITableView!
    @IBOutlet weak var btn_check: UIButton!
    //    @IBOutlet weak var btn_numbering: UIButton!
    //    @IBOutlet weak var btn_limit: UIButton!
    //    @IBOutlet weak var btn_callTimer: UIButton!
    @IBOutlet weak var txtView_ack: UITextView!
    @IBOutlet weak var txtView_askUpdate: UITextView!
    @IBOutlet weak var txt_numbering: UITextField!
    @IBOutlet weak var txt_limit: UITextField!
    @IBOutlet weak var txt_calltimer: UITextField!
    @IBOutlet weak var txtNextPerson: UITextField!
    @IBOutlet weak var txtNextPlace: UITextField!
    
    
    @IBOutlet weak var txtView_call: UITextView!
    @IBOutlet weak var txt_noSHow: UITextView!
    @IBOutlet weak var btn_callNextCheck: UIButton!
    @IBOutlet weak var btn_callNexPerson: UIButton!
    @IBOutlet weak var btn_callNexPlace: UIButton!
    @IBOutlet weak var btn_persons: UIButton!
    @IBOutlet weak var btn_places: UIButton!
    @IBOutlet weak var btnProfile: UIButton!
    
    @IBOutlet weak var cnsCustomDaysTableHeight: NSLayoutConstraint!
    @IBOutlet weak var cnsOffDaysTableHeight: NSLayoutConstraint!
    let tableCellHeight: CGFloat = 45
    var didUpdateQueueList: (() -> Void)?
    
    let numbering = ["1","2","3","4","5"]
    let timer = ["1","2","3","4","5","6"]
    var arrDays = [String]()
    var schedule:[(slotID:String, days:String, isOff:String, oneTo:String, oneFrom:String, twoTo:String, twoFrom:String)] = []
    var customDay:[(date:String , day:String , oneTo:String , oneFrom:String, twoTo:String, twoFrom:String, slot_id:String, custom_date:String)] = []
    
    var offDay:[(date:String, day:String, slot_id:String)] = []
    let dropDown = DropDown()
    let dropDownNumbering = DropDown()
    let dropDownTimer = DropDown()
    var timePicker = UIDatePicker()
    var currentTextField = UITextField()
    var numberingCheck = 0
    var callFuncTap = 0
    var callPlaceTap = 0
    var callPersonTap = 0
    var customDays = 0
    var offDays = 0
    var myQueue:Myqueue!
    var selectedEmployeesID:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.btnProfile.setProfileButtonImage()
        self.populateQueueDetails()
        self.selectedEmployeesID = UserStore.userID ?? ""
    }
    
    private func setup() {
        btnProfile.setProfileButtonImage()
        registerXib()
        txt_timmings.forEach { (txt) in
            txt.isEnabled = false
            txt.delegate = self
        }
        btn_callNexPerson.isEnabled = false
        btn_callNexPlace.isEnabled = false
        btn_persons.isEnabled = false
        btn_places.isEnabled = false
        NotificationCenter.default.addObserver(self, selector: #selector(updateProfile(_:)), name: .updateUserProfile, object: nil)
    }
    
    @objc func updateProfile(_ notification:Notification) {
          btnProfile.setProfileButtonImage()
    }

    
    private func populateQueueDetails() {
        
        txt_name.text = myQueue.queName
        txtView_descp.text = myQueue.queDescription
        btn_check.setImage(UIImage(named: "uncheck"), for: .normal)
        numberingCheck = 0
        if myQueue.numbering != nil && myQueue.numbering != "" && myQueue.numbering != "0" {
            numberingCheck = 1
            btn_check.setImage(UIImage(named: "check"), for: .normal)
        }
        
        txt_limit.text = myQueue.capacity
        txt_numbering.text = myQueue.numberStart
        txt_calltimer.text = myQueue.callTimer
        txtView_ack.text = myQueue.acknowledgeMsg
        txtView_askUpdate.text = myQueue.askupdateMsg
        txtView_call.text = myQueue.callMsg
        txt_noSHow.text = myQueue.noshowMsg
        
        if (myQueue.callNextPersons != nil && myQueue.callNextPersons != "") ||
            (myQueue.callNextPlaces != nil && myQueue.callNextPlaces != "") {
            btn_callFuncTap(btn_callNextCheck)
        }
        if (myQueue.callNextPersons != nil && myQueue.callNextPersons != "") {
            btn_callFuncTap(btn_persons)
        }
        if (myQueue.callNextPlaces != nil && myQueue.callNextPlaces != "") {
            btn_callFuncTap(btn_places)
        }
        
        txtNextPerson.text = myQueue.callNextPersons
        txtNextPlace.text = myQueue.callNextPlaces
        customDay.removeAll()
        var buttonImage: UIImage!
        var strFrom = ""
        var strTo = ""
        if myQueue.schedule != nil {
            for temp in myQueue.schedule! {
                schedule.append((slotID:temp.slotID ?? "", days: temp.days ?? "", isOff: temp.isOff ?? "1", oneTo: temp.oneTo ?? "", oneFrom: temp.oneFrom ?? "", twoTo: temp.twoTo ?? "", twoFrom: temp.twoFrom ?? ""))

                if temp.isOff != nil && temp.isOff != "1" {
                    strFrom = temp.oneTo ?? ""
                    strTo = temp.oneFrom ?? ""
                    buttonImage = #imageLiteral(resourceName: "radioBtn")
                } else {
                    strFrom = ""
                    strTo = ""
                    buttonImage = #imageLiteral(resourceName: "radioUnchck")
                }
                
                switch temp.days {
                case "0": // 0 is Sunday and onwards
                    if strFrom != "" {
                        arrDays.append("0")
//                        btn_dayTap(btn_days[6])
                    }

                    txt_timmings[25].text = strFrom
                    txt_timmings[26].text = strTo
                    btn_days[6].setImage(buttonImage, for: .normal)
                    
                case "1":
                    if strFrom != "" {
                        arrDays.append("1")
//                        btn_dayTap(btn_days[0])
                    }
                    txt_timmings[0].text = strFrom
                    txt_timmings[1].text = strTo
                    btn_days[0].setImage(buttonImage, for: .normal)
                    
                case "2":
                    if strFrom != "" {
                        arrDays.append("2")
//                        btn_dayTap(btn_days[1])
                    }
                    txt_timmings[4].text = strFrom
                    txt_timmings[5].text = strTo
                    btn_days[1].setImage(buttonImage, for: .normal)
                    
                case "3":
                    if strFrom != "" {
                        arrDays.append("3")
//                        btn_dayTap(btn_days[2])
                    }
                    txt_timmings[8].text = strFrom
                    txt_timmings[9].text = strTo
                    btn_days[2].setImage(buttonImage, for: .normal)
                    
                case "4":
                    if strFrom != "" {
                        arrDays.append("4")
//                        btn_dayTap(btn_days[3])
                    }
                    txt_timmings[12].text = strFrom
                    txt_timmings[13].text = strTo
                    btn_days[3].setImage(buttonImage, for: .normal)
                    
                case "5":
                    if strFrom != "" {
                        arrDays.append("5")
//                        btn_dayTap(btn_days[4])
                    }
                    txt_timmings[16].text = strFrom
                    txt_timmings[17].text = strTo
                    btn_days[4].setImage(buttonImage, for: .normal)
                    
                case "6":
                    if strFrom != "" {
                        arrDays.append("6")
//                        btn_dayTap(btn_days[5])
                    }
                    txt_timmings[20].text = strFrom
                    txt_timmings[21].text = strTo
                    btn_days[5].setImage(buttonImage, for: .normal)
                default:
                    strFrom = ""
                    strTo = ""
                }
            }
        }
        
        if myQueue.customDays?.count != 0  {
            for custom in myQueue.customDays! {
                self.customDay.append((date: custom.customDate ?? "", day: custom.days ?? "", oneTo: custom.oneTo ?? "", oneFrom: custom.oneFrom ?? "", twoTo: custom.twoTo ?? "", twoFrom: custom.twoFrom ?? "", slot_id:"", custom_date:""))
            }
        }
        
        if myQueue.offDays?.count != 0  {
            for off in myQueue.offDays! {
                self.offDay.append((date: off.customDate ?? "", day: off.days ?? "", slot_id: off.slotID ?? ""))
//                self.offDay.append((date: off.customDate, day: off.days, slot_id: off.slotID))
            }
        }
        schedule.sort { $0.days < $1.days }
    

        DispatchQueue.main.async {
            self.customDays = self.myQueue.customDays?.count ?? 0
            self.cnsCustomDaysTableHeight.constant =
                CGFloat(CGFloat(self.customDays) * self.tableCellHeight)
            self.tblView_customDays.reloadData()
            self.offDays = self.myQueue.offDays?.count ?? 0
            self.cnsOffDaysTableHeight.constant =
                CGFloat(CGFloat(self.offDays) * self.tableCellHeight)
            self.tblView_offdays.reloadData()
        }
    }
    
    @IBAction func btn_employeeTap(_ sender: UIButton) {
        let nc = self.storyboard?.instantiateViewController(withIdentifier: Controller.SelectEmp) as! SelectEmpVC
        nc.sendData = { (arrData) in
            print(arrData)
            if !arrData.isEmpty {
                var emps = ""
                arrData.forEach { (data) in
                    emps.append(data.name + ",")
                    self.selectedEmployeesID.append(data.id + ",")
                }
                emps.removeLast(1)
                self.selectedEmployeesID.removeLast(1)
                self.lblEmployees.text = emps
            }
        }
        nc.selectedEmployeesID = selectedEmployeesID
        self.present(nc, animated: true, completion: nil)
    }
    
    @IBAction func btn_profileTap(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btn_notificationTap(_ sender: UIButton) {
        navigateToNavigationView()
    }
    
    @IBAction func btn_dayTap(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            if arrDays.contains("1") {
                arrDays.remove(at: arrDays.firstIndex(of: "1")!)
                sender.setImage(#imageLiteral(resourceName: "radioUnchck"), for: .normal)
                txt_timmings.forEach { (btn) in
                    if btn.tag >= 0 && btn.tag <= 3 {
                        btn.isEnabled = false
                        btn.text = ""
                    }
                }
                schedule[1].days = "1"
                schedule[1].isOff = "1"
                schedule[1].oneTo = ""
                schedule[1].oneFrom = ""
                schedule[1].twoTo = ""
                schedule[1].twoFrom = ""
            } else {
                arrDays.append("1")
                sender.setImage(#imageLiteral(resourceName: "radioBtn"), for: .normal)
                txt_timmings.forEach { (btn) in
                    if btn.tag >= 0 && btn.tag <= 3 {
                        btn.isEnabled = true
                        schedule[1].days = "1"
                        schedule[1].isOff = "0"
                    }
                }
            }
        case 1:
            if arrDays.contains("2") {
                arrDays.remove(at: arrDays.firstIndex(of: "2")!)
                sender.setImage(#imageLiteral(resourceName: "radioUnchck"), for: .normal)
                txt_timmings.forEach { (btn) in
                    if btn.tag >= 4 && btn.tag <= 7 {
                        btn.isEnabled = false
                        btn.text = ""
                    }
                }
                schedule[2].days = "2"
                schedule[2].isOff = "1"
                schedule[2].oneTo = ""
                schedule[2].oneFrom = ""
                schedule[2].twoTo = ""
                schedule[2].twoFrom = ""
            } else {
                arrDays.append("2")
                sender.setImage(#imageLiteral(resourceName: "radioBtn"), for: .normal)
                txt_timmings.forEach { (btn) in
                    if btn.tag >= 4 && btn.tag <= 7 {
                        btn.isEnabled = true
                        schedule[2].days = "2"
                        schedule[2].isOff = "0"
                    }
                }
            }
        case 2:
            if arrDays.contains("3") {
                arrDays.remove(at: arrDays.firstIndex(of: "3")!)
                sender.setImage(#imageLiteral(resourceName: "radioUnchck"), for: .normal)
                txt_timmings.forEach { (btn) in
                    if btn.tag >= 8 && btn.tag <= 11 {
                        btn.isEnabled = false
                        btn.text = ""
                    }
                }
                schedule[3].days = "3"
                schedule[3].isOff = "1"
                schedule[3].oneTo = ""
                schedule[3].oneFrom = ""
                schedule[3].twoTo = ""
                schedule[3].twoFrom = ""
            } else {
                arrDays.append("3")
                sender.setImage(#imageLiteral(resourceName: "radioBtn"), for: .normal)
                txt_timmings.forEach { (btn) in
                    if btn.tag >= 8 && btn.tag <= 11 {
                        btn.isEnabled = true
                        schedule[3].days = "3"
                        schedule[3].isOff = "0"
                    }
                }
            }
        case 3:
            if arrDays.contains("4") {
                arrDays.remove(at: arrDays.firstIndex(of: "4")!)
                sender.setImage(#imageLiteral(resourceName: "radioUnchck"), for: .normal)
                txt_timmings.forEach { (btn) in
                    if btn.tag >= 12 && btn.tag <= 15 {
                        btn.isEnabled = false
                        btn.text = ""
                    }
                }
                schedule[4].days = "4"
                schedule[4].isOff = "1"
                schedule[4].oneTo = ""
                schedule[4].oneFrom = ""
                schedule[4].twoTo = ""
                schedule[4].twoFrom = ""
            } else {
                arrDays.append("4")
                sender.setImage(#imageLiteral(resourceName: "radioBtn"), for: .normal)
                txt_timmings.forEach { (btn) in
                    if btn.tag >= 12 && btn.tag <= 15 {
                        btn.isEnabled = true
                        schedule[4].days = "4"
                        schedule[4].isOff = "0"
                    }
                }
            }
        case 4:
            if arrDays.contains("5") {
                arrDays.remove(at: arrDays.firstIndex(of: "5")!)
                sender.setImage(#imageLiteral(resourceName: "radioUnchck"), for: .normal)
                txt_timmings.forEach { (btn) in
                    if btn.tag >= 16 && btn.tag <= 19 {
                        btn.isEnabled = false
                        btn.text = ""
                    }
                }
                schedule[5].days = "5"
                schedule[5].isOff = "1"
                schedule[5].oneTo = ""
                schedule[5].oneFrom = ""
                schedule[5].twoTo = ""
                schedule[5].twoFrom = ""
            } else {
                arrDays.append("5")
                sender.setImage(#imageLiteral(resourceName: "radioBtn"), for: .normal)
                txt_timmings.forEach { (btn) in
                    if btn.tag >= 16 && btn.tag <= 19 {
                        btn.isEnabled = true
                        schedule[5].days = "5"
                        schedule[5].isOff = "0"
                    }
                }
            }
        case 5:
            if arrDays.contains("6") {
                arrDays.remove(at: arrDays.firstIndex(of: "6")!)
                sender.setImage(#imageLiteral(resourceName: "radioUnchck"), for: .normal)
                txt_timmings.forEach { (btn) in
                    if btn.tag >= 20 && btn.tag <= 23 {
                        btn.isEnabled = false
                        btn.text = ""
                    }
                }
                schedule[6].days = "6"
                schedule[6].isOff = "1"
                schedule[6].oneTo = ""
                schedule[6].oneFrom = ""
                schedule[6].twoTo = ""
                schedule[6].twoFrom = ""
            } else {
                arrDays.append("6")
                sender.setImage(#imageLiteral(resourceName: "radioBtn"), for: .normal)
                txt_timmings.forEach { (btn) in
                    if btn.tag >= 20 && btn.tag <= 23 {
                        btn.isEnabled = true
                        schedule[6].days = "6"
                        schedule[6].isOff = "0"
                    }
                }
            }
        default:
            if arrDays.contains("0") {
                arrDays.remove(at: arrDays.firstIndex(of: "0")!)
                sender.setImage(#imageLiteral(resourceName: "radioUnchck"), for: .normal)
                txt_timmings.forEach { (btn) in
                    if btn.tag >= 24 && btn.tag <= 27 {
                        btn.isEnabled = false
                        btn.text = ""
                    }
                }
                schedule[0].days = "0"
                schedule[0].isOff = "1"
                schedule[0].oneTo = ""
                schedule[0].oneFrom = ""
                schedule[0].twoTo = ""
                schedule[0].twoFrom = ""
            } else {
                arrDays.append("0")
                sender.setImage(#imageLiteral(resourceName: "radioBtn"), for: .normal)
                txt_timmings.forEach { (btn) in
                    if btn.tag >= 24 && btn.tag <= 27 {
                        btn.isEnabled = true
                        schedule[0].days = "0"
                        schedule[0].isOff = "0"
                    }
                }
            }
        }
    }
    
    @IBAction func btn_addDaysTap(_ sender: UIButton) {
        if sender.tag == 0 {
            guard let nc = RetailerStoryBoard.Profile(controller: Controller.CustomDate) as? CustomDateVC else {return}
            if #available(iOS 13.0, *) {
                nc.isModalInPresentation = true
                nc.modalPresentationStyle = .overCurrentContext
            }
            nc.dateCompletion = { date, oneTo, onFrom, twoTo, twoFrom in
                let dateFormater = DateFormatter()
                dateFormater.dateFormat = "dd-MM-yyyy"
                let ndate = dateFormater.date(from: date)
                var dayNum = ""
                switch ndate?.dayOfWeek() {
                case "Monday":
                    dayNum = "1"
                case "Tuesday":
                    dayNum = "2"
                case "Wednesday":
                    dayNum = "3"
                case "Thursday":
                    dayNum = "4"
                case "Friday":
                    dayNum = "5"
                case "Saturday":
                    dayNum = "6"
                default:
                    dayNum = "0"
                }
                self.customDay.append((date: date, day: dayNum, oneTo: oneTo, oneFrom: onFrom, twoTo: twoTo, twoFrom: twoFrom, slot_id:"", custom_date:""))
                self.cnsCustomDaysTableHeight.constant = CGFloat(CGFloat(self.customDay.count) * self.tableCellHeight)
                self.tblView_customDays.reloadData()
            }
            self.present(nc, animated: true, completion: nil)
        } else {
            guard let nc = RetailerStoryBoard.Profile(controller: Controller.Offdays) as? OffDaysVC else {return}
            if #available(iOS 13.0, *) {
                nc.isModalInPresentation = true
                nc.modalPresentationStyle = .overCurrentContext
            }
            nc.dateCompletion = { date in
                let dateFormater = DateFormatter()
                dateFormater.dateFormat = "dd-MM-yyyy"
                let ndate = dateFormater.date(from: date)
                var dayNum = ""
                switch ndate?.dayOfWeek() {
                case "Monday":
                    dayNum = "1"
                case "Tuesday":
                    dayNum = "2"
                case "Wednesday":
                    dayNum = "3"
                case "Thursday":
                    dayNum = "4"
                case "Friday":
                    dayNum = "5"
                case "Saturday":
                    dayNum = "6"
                default:
                    dayNum = "0"
                }
                self.offDay.append((date: date, day: dayNum, slot_id: ""))
                self.cnsOffDaysTableHeight.constant = CGFloat(CGFloat(self.offDay.count) * self.tableCellHeight)
                self.tblView_offdays.reloadData()
            }
            self.present(nc, animated: true, completion: nil)
        }
    }
    
    @IBAction func btn_numberCheckTap(_ sender: UIButton) {
        numberingCheck += 1
        if numberingCheck%2 == 0 {
            sender.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            txt_numbering.isEnabled = false
            //            btn_numbering.setTitle("", for: .normal)
        } else {
            sender.setImage(#imageLiteral(resourceName: "check"), for: .normal)
            txt_numbering.isEnabled = true
        }
    }
    
    @IBAction func btn_numberingTap(_ sender: UIButton) {
        //        dropDownNumbering.anchorView = btn_numbering
        //        dropDownNumbering.dataSource = numbering
        //        dropDownNumbering.bottomOffset = CGPoint(x: 0, y:(dropDownNumbering.anchorView?.plainView.bounds.height)!)
        //        dropDownNumbering.direction = .bottom
        //        dropDownNumbering.selectionAction = { (index, item) in
        //            print(index,item)
        //            sender.setTitle(item, for: .normal)
        //        }
        //        dropDownNumbering.show()
    }
    
    @IBAction func btn_limitTap(_ sender: UIButton) {
        //        dropDownNumbering.anchorView = btn_numbering
        //        dropDownNumbering.dataSource = numbering
        //        dropDownNumbering.bottomOffset = CGPoint(x: 0, y:(dropDownNumbering.anchorView?.plainView.bounds.height)!)
        //        dropDownNumbering.direction = .bottom
        //        dropDownNumbering.selectionAction = { (index, item) in
        //            print(index,item)
        //            sender.setTitle(item, for: .normal)
        //        }
        //        dropDownNumbering.show()
    }
    
    @IBAction func btn_timerTap(_ sender: UIButton) {
        //        dropDownNumbering.anchorView = btn_callTimer
        //        dropDownNumbering.dataSource = timer
        //        dropDownNumbering.bottomOffset = CGPoint(x: 0, y:(dropDownNumbering.anchorView?.plainView.bounds.height)!)
        //        dropDownNumbering.direction = .bottom
        //        dropDownNumbering.selectionAction = { (index, item) in
        //            print(index,item)
        //            sender.setTitle(item, for: .normal)
        //        }
        //        dropDownNumbering.show()
    }
    
    @IBAction func btn_callFuncTap(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            callFuncTap += 1
            if callFuncTap%2 == 0 {
                sender.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
                btn_callNexPerson.setTitle("", for: .normal)
                btn_callNexPlace.setTitle("", for: .normal)
                btn_callNexPerson.isEnabled = false
                btn_callNexPlace.isEnabled = false
            } else {
                sender.setImage(#imageLiteral(resourceName: "check"), for: .normal)
                btn_callNexPerson.isEnabled = true
                btn_callNexPlace.isEnabled = true
            }
        case 1:
            callPersonTap += 1
            if callPersonTap%2 == 0 {
                sender.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
                btn_persons.isEnabled = false
            } else {
                sender.setImage(#imageLiteral(resourceName: "check"), for: .normal)
                btn_persons.isEnabled = true
            }
        default:
            callPlaceTap += 1
            if callPlaceTap%2 == 0 {
                sender.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
                btn_places.isEnabled = false
            } else {
                sender.setImage(#imageLiteral(resourceName: "check"), for: .normal)
                btn_places.isEnabled = true
            }
        }
    }
    
    @IBAction func btn_personTap(_ sender: UIButton) {
        dropDownNumbering.anchorView = btn_persons
        dropDownNumbering.dataSource = timer
        dropDownNumbering.bottomOffset = CGPoint(x: 0, y:(dropDownNumbering.anchorView?.plainView.bounds.height)!)
        dropDownNumbering.direction = .bottom
        dropDownNumbering.selectionAction = { (index, item) in
            print(index,item)
            sender.setTitle(item, for: .normal)
        }
        dropDownNumbering.show()
    }
    
    @IBAction func btn_placeTap(_ sender: UIButton) {
        dropDownNumbering.anchorView = btn_places
        dropDownNumbering.dataSource = timer
        dropDownNumbering.bottomOffset = CGPoint(x: 0, y:(dropDownNumbering.anchorView?.plainView.bounds.height)!)
        dropDownNumbering.direction = .bottom
        dropDownNumbering.selectionAction = { (index, item) in
            print(index,item)
            sender.setTitle(item, for: .normal)
        }
        dropDownNumbering.show()
    }
    
    @IBAction func btn_cancelTap(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btn_saveTap(_ sender: UIButton) {
        if validateInputs() {
            updateQueue()
        }
    }
}


extension EditQueueVC: UITextFieldDelegate {
    
    func registerXib() {
        tblView_customDays.register(UINib(nibName: Cell.CustomDays, bundle: nil), forCellReuseIdentifier: Cell.CustomDays)
        tblView_offdays.register(UINib(nibName: Cell.OffDays, bundle: nil), forCellReuseIdentifier: Cell.OffDays)
    }
    
    func validateInputs() -> Bool {
        var msg = ""
        if txt_name.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            msg = "Please enter queue name"
        } else if txtView_descp.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            msg = "Please enter queue description"
        } else if schedule.count == 0 {
            msg = "Please select queue schedule"
        } else if numberingCheck%2 != 0 && txt_numbering.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            msg = "Please check place numbering"
        } else if txt_numbering.text == nil {
            msg = "Please enter numbering"
        } else if txt_limit.text == nil {
            msg = "Please enter queue limit"
        } else if txt_calltimer.text == nil {
            msg = "Please select call timer"
        } else if txtView_ack.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            msg = "Please enter acknowledge message"
        } else if txtView_askUpdate.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            msg = "Please enter ask update message"
        } else if txtView_call.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            msg = "Please enter call message"
        } else if txt_noSHow.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            msg = "Please enter no show message"
        }
        if msg == "" {
            return true
        } else {
            self.func_Alert(message: msg)
            return false
        }
    }
    
    // TimePicker
    func createTimePicker(txtField: UITextField) {
        //toolbar
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        // bar button
        let doneBtn = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donePressed))
        toolBar.setItems([doneBtn], animated: true)
        //assign toolbar
        txtField.inputAccessoryView = toolBar
        //assign picker to textfield
        txtField.inputView = timePicker
        //timepicker mode
        timePicker.datePickerMode = .time
        timePicker.locale = Locale(identifier: "en_gb")
    }
    
    @objc func donePressed() {
        //formatter
        let formatter = DateFormatter()
        formatter.timeStyle = .short
        formatter.dateFormat = "HH:mm"
        currentTextField.text = formatter.string(from: timePicker.date)
        self.view.endEditing(true)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        currentTextField = textField
        createTimePicker(txtField: textField)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField.tag {
        case 0:
            schedule[1].oneTo = textField.text ?? ""
        case 1:
            schedule[1].oneFrom = textField.text ?? ""
        case 2:
            schedule[1].twoTo = textField.text ?? ""
        case 3:
            schedule[1].twoFrom = textField.text ?? ""
        case 4:
            schedule[2].oneTo = textField.text ?? ""
        case 5:
            schedule[2].oneFrom = textField.text ?? ""
        case 6:
            schedule[2].twoTo = textField.text ?? ""
        case 7:
            schedule[2].twoFrom = textField.text ?? ""
        case 8:
            schedule[3].oneTo = textField.text ?? ""
        case 9:
            schedule[3].oneFrom = textField.text ?? ""
        case 10:
            schedule[3].twoTo = textField.text ?? ""
        case 11:
            schedule[3].twoFrom = textField.text ?? ""
        case 12:
            schedule[4].oneTo = textField.text ?? ""
        case 13:
            schedule[4].oneFrom = textField.text ?? ""
        case 14:
            schedule[4].twoTo = textField.text ?? ""
        case 15:
            schedule[4].twoFrom = textField.text ?? ""
        case 16:
            schedule[5].oneTo = textField.text ?? ""
        case 17:
            schedule[5].oneFrom = textField.text ?? ""
        case 18:
            schedule[5].twoTo = textField.text ?? ""
        case 19:
            schedule[5].twoFrom = textField.text ?? ""
        case 20:
            schedule[6].oneTo = textField.text ?? ""
        case 21:
            schedule[6].oneFrom = textField.text ?? ""
        case 22:
            schedule[6].twoTo = textField.text ?? ""
        case 23:
            schedule[6].twoFrom = textField.text ?? ""
        case 24:
            schedule[0].oneTo = textField.text ?? ""
        case 25:
            schedule[0].oneFrom = textField.text ?? ""
        case 26:
            schedule[0].twoTo = textField.text ?? ""
        case 27:
            schedule[0].twoFrom = textField.text ?? ""
        default:
            break
        }
    }
}


extension EditQueueVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblView_customDays {
            return self.customDay.count
        } else {
            return self.offDay.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tblView_customDays {
            let cell = tableView.dequeueReusableCell(withIdentifier: Cell.CustomDays) as! CustomdaysCell
            cell.lbl_date.text = customDay[indexPath.row].date
            cell.lbl_timming.text = "\(customDay[indexPath.row].oneTo)h To \(customDay[indexPath.row].oneFrom)h And \(customDay[indexPath.row].twoTo)h To \(customDay[indexPath.row].twoFrom)"
            cell.didRemoveItem = {
                self.customDay.remove(at: indexPath.row)
                self.cnsCustomDaysTableHeight.constant = self.cnsCustomDaysTableHeight.constant - self.tableCellHeight
                self.tblView_customDays.reloadData()
            }
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: Cell.OffDays) as! OffDaysCell
            cell.lbl_offDate.text = self.offDay[indexPath.row].date
            cell.didRemoveItem = {
                self.offDay.remove(at: indexPath.row)
                self.cnsOffDaysTableHeight.constant = self.cnsOffDaysTableHeight.constant - self.tableCellHeight
                self.tblView_offdays.reloadData()
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableCellHeight
    }
}

// MARK:- API Call
extension EditQueueVC {
    func updateQueue() {
        var authorization = ""
        var empID = ""
        var busId = ""
        var busCode = ""
        if let auth = LoginSession.shared.getUserModelDefault(keyName: userData)?.data?.login?.authorization, let id = LoginSession.shared.getUserModelDefault(keyName: userData)?.data?.login?.userid, let bid = LoginSession.shared.getUserModelDefault(keyName: userData)?.data?.login?.busid, let bCode = LoginSession.shared.getUserModelDefault(keyName: userData)?.data?.login?.busiCode {
            authorization = auth
            empID = id
            busId = bid
            busCode = bCode
        }
        var arrSchedule = [[String:AnyObject]]()
        for schdl in schedule {
            let dic = [
                "slot_id": schdl.slotID,
                "is_off": schdl.isOff,
                "day": schdl.days,
                "one_to": schdl.oneTo,
                "one_from": schdl.oneFrom,
                "two_to": schdl.twoTo,
                "two_from": schdl.twoFrom
//                "is_custom": schdl.is_custom,
//                "custom_date": schdl.custom_date
            ]
            arrSchedule.append(dic as [String : AnyObject])
        }
//        let schdlDict = JSON(arrSchedule)
        var arrCustomDay = [[String:AnyObject]]()
        for custDay in customDay {
            
            let dic = [
                "slot_id": custDay.slot_id,
                "custom_date": custDay.date,
                "day": custDay.day,
                "one_to": custDay.oneTo,
                "one_from": custDay.oneFrom,
                "two_to": custDay.twoTo,
                "two_from": custDay.twoFrom
            ]
            arrCustomDay.append(dic as [String : AnyObject])
        }
//        let custDict = JSON(arrCustomDay)
        var arrOffDay = [[String:AnyObject]]()
        for ofDay in offDay {
            let dic = [
                "custom_date": ofDay.date,
                "day": ofDay.day,
                "slot_id":ofDay.slot_id
            ]
            arrOffDay.append(dic as [String : AnyObject])
        }
                
        let header:[String:String] = [
            "Authorization":authorization,
            "device_type":"ios",
            "device_id":"111",
            "current_time_zone":TimeZone.current.identifier,
            "language":"en",
            "Content-Type":"application/json",
            "version":"1.0.0",
            "current_country":"India",
            "lat":"22.12541",
            "lng":"22.12541"
        ]
        
        let param:[String:Any] = [
            "que_id" : myQueue.queID!,
            "que_name":txt_name.text ?? "",
            "que_desc":txtView_descp.text ?? "",
            "call_timer":txt_calltimer.text ?? "",
            "number_start":txt_numbering.text ?? "",
            "numbering":txt_numbering.text ?? "",
            "number_prefix":"",
            "capacity":txt_limit.text ?? "",
            "call_next_persons":btn_persons.titleLabel?.text ?? "",
            "call_next_places":btn_places.titleLabel?.text ?? "",
            "emp_id": empID,
            "busi_code":busCode,
            "busi_id":busId,
            "acknowledge_msg":txtView_ack.text ?? "",
            "askupdate_msg":txtView_askUpdate.text ?? "",
            "call_msg": txtView_call.text ?? "",
            "noshow_msg": txt_noSHow.text ?? "",
            "schedule": arrSchedule, //arrSchedule ,
            "custom_days":arrCustomDay, //arrCustomDay ,
            "off_days":arrOffDay //arrOffDay
        ]
        
        Loader.shared.showIndicator(view: self.view)
        K3Webservice.run(urlString: WebUrl.updateQueue, method: HTTPMethod.post, Param: param, header: header , type: NewPasswordModel.self, viewController: self) { (response) in
            if let isResponse = response {
                Loader.shared.hideIndicator()
                if isResponse.status == 1 {
                    self.func_AlertWithOneOption(message: isResponse.message ?? "", optionOne: "Ok") {
                        self.didUpdateQueueList?()
                    }
                } else {
                    self.func_Alert(message: isResponse.message ?? "")
                }
            }
        }
    }
}
