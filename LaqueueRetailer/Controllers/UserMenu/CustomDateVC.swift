//
//  CustomDateVC.swift
//  LaqueueRetailer
//
//  Created by Adityaraj Singh Gaharwar on 09/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class CustomDateVC: UIViewController {

    @IBOutlet weak var txt_selectDate: UITextField!
    @IBOutlet weak var txt_oneTo: UITextField!
    @IBOutlet weak var txt_oneFrom: UITextField!
    @IBOutlet weak var txt_twoTo: UITextField!
    @IBOutlet weak var txt_twoFrom: UITextField!
    
    var timePicker = UIDatePicker()
    var datePicker = UIDatePicker()
    var currentTextField = UITextField()
    var dateCompletion:((_ date:String,_ oneTo:String,_ oneFrom:String,_ twoTo:String,_ twoFrom:String) -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    private func setup() {
        datePicker.minimumDate = Date()
        [txt_oneTo, txt_twoTo, txt_oneFrom, txt_twoFrom, txt_selectDate].forEach {
            $0?.delegate = self
        }
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        view.addGestureRecognizer(tap)
    }
    
    @IBAction func btn_cancelTap(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btn_saveTap(_ sender: UIButton) {
        dateCompletion?(txt_selectDate.text ?? "", txt_oneTo.text ?? "", txt_oneFrom.text ?? "", txt_twoTo.text ?? "", txt_twoFrom.text ?? "")
        self.dismiss(animated: true, completion: nil)
    }
}

extension CustomDateVC: UITextFieldDelegate {
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        // handling code
//        self.dismiss(animated: true, completion: nil)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        currentTextField = textField
        if textField == txt_selectDate {
            createDatePicker(txtField: textField)
        } else {
            createTimePicker(txtField: textField)
        }
    }
    
    // DatePicker
    func createDatePicker(txtField: UITextField) {
        //toolbar
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        // bar button
        let doneBtn = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donePressedDate))
        toolBar.setItems([doneBtn], animated: true)
        //assign toolbar
        txtField.inputAccessoryView = toolBar
        //assign picker to textfield
        txtField.inputView = datePicker
        //timepicker mode
        datePicker.datePickerMode = .date
//        datePicker.locale = Locale(identifier: "en_gb")
    }
    
    // TimePicker
    func createTimePicker(txtField: UITextField) {
        //toolbar
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        // bar button
        let flexButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let doneBtn = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donePressed))
        toolBar.setItems([flexButton, doneBtn], animated: true)
        //assign toolbar
        txtField.inputAccessoryView = toolBar
        //assign picker to textfield
        txtField.inputView = timePicker
        //timepicker mode
        timePicker.datePickerMode = .time
        timePicker.locale = Locale(identifier: "en_gb")
    }
    
    @objc func donePressed() {
        //formatter
        let formatter = DateFormatter()
        formatter.timeStyle = .short
        formatter.dateFormat = "HH:mm"
        currentTextField.text = formatter.string(from: timePicker.date)
        self.view.endEditing(true)
    }
    
    @objc func donePressedDate() {
        //formatter
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.dateFormat = "dd-MM-yyyy"
        currentTextField.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
}
