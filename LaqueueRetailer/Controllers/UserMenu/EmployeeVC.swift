//
//  EmployeeVC.swift
//  LaqueueRetailer
//
//  Created by Adityaraj Singh Gaharwar on 04/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Kingfisher
import Alamofire

class EmployeeVC: UIViewController {

    @IBOutlet weak var lbl_empStatus: UILabel!
    @IBOutlet weak var img_profile: UIImageView!
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var lbl_email: UILabel!
    @IBOutlet weak var lbl_dob: UILabel!
    @IBOutlet weak var lbl_phone: UILabel!
    @IBOutlet weak var lbl_type: UILabel!
    @IBOutlet weak var lbl_role: UILabel!
    @IBOutlet weak var btnProfile: UIButton!
    
    var didUpdateEmployeeList: (() -> Void)?
    var employee: EmployeeList!
    var userProfile = ""
    var name = ""
    var email = ""
    var emplyID = ""
    var fName = ""
    var lName = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        btnProfile.setProfileButtonImage()
    }
    
    private func setup() {
        self.populateDetails()
        NotificationCenter.default.addObserver(self, selector: #selector(updateProfile(_:)), name: .updateUserProfile, object: nil)
    }

    @objc func updateProfile(_ notification:Notification) {
        btnProfile.setProfileButtonImage()
    }
    
    private func didUpdateUserInfo(_ image:UIImage) {
        getEmployeeDetail()
        DispatchQueue.main.async {
            self.img_profile.image = image
            self.btnProfile.setImage(image, for: .normal)
        }
    }

    private func populateDetails() {
        lbl_name.text = "\(employee.firstname ?? "") \(employee.lastname ?? "")"
        lbl_email.text = employee.userEmail
        lbl_role.text = (employee.userRole == "2") ? "Owner" : "Employee"
        lbl_phone.text = employee.userPhone ?? "--"
        img_profile.kf.setImage(with: URL(string: employee.userPic ?? ""), options: [.transition(.fade(0.5))])
        lbl_empStatus.backgroundColor = UIColor(hex: "EEA11A")
        lbl_empStatus.text = "Active"
        lbl_empStatus.backgroundColor = UIColor(hex: "33C851")
        if lbl_name.text?.trimmingCharacters(in: .whitespaces).count == 0 {
            lbl_empStatus.backgroundColor = UIColor(hex: "EEA11A")
            lbl_empStatus.text = "Inactive"
        }
    }
    
    private func showConfirmationAlert() {
        let alert = UIAlertController(title: "Laqueue", message: "Do you want to proceed?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "No", style: .destructive, handler: { (_) in }))
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (_) in
            self.removeEmployee()
        }))
        present(alert, animated: true, completion: nil)
    }
    
    private func dismissAfetrRemove() {
        didUpdateEmployeeList?()
        NotificationCenter.default.post(name: .reloadEmployeeList, object: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    private func showProfileDetails(_ isResponse:EmployeeDetailModel) {
        UserStore.save(userPic: isResponse.data?.employeeDetail?.userPic)
//        btnProfile.setProfileButtonImage()
//        img_profile.kf.setImage(with: URL(string: isResponse.data?.employeeDetail?.userPic ?? ""), options: [.transition(.fade(0.5))])
        lbl_name.text = "\(isResponse.data?.employeeDetail?.firstname ?? "") \(isResponse.data?.employeeDetail?.lastname ?? "")"
        lbl_email.text = isResponse.data?.employeeDetail?.userEmail ?? ""
        lbl_phone.text = isResponse.data?.employeeDetail?.userPhone ?? ""
        lbl_role.text = (isResponse.data?.employeeDetail?.userRole == "2") ? "Owner" : "Employee"
    }
    
    // MARK:- Button Actions
    @IBAction func btn_profileTap(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btn_editTap(_ sender: UIButton) {
        guard let vc = RetailerStoryBoard.Profile(controller: Controller.NewEmployee) as? NewEmployeeVC else {return}
        vc.employee = employee
        vc.isEdit = true
        vc.didUpdateUserInfo = didUpdateUserInfo
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btn_notifyTap(_ sender: UIButton) {
        self.notifyEmployee()
    }
    
    @IBAction func btn_removeTap(_ sender: UIButton) {
        self.showConfirmationAlert()
    }
    
    @IBAction func btn_notificationTap(_ sender: UIButton) {
        navigateToNavigationView()
    }
}


extension EmployeeVC {
    func removeEmployee() {
        var authorization = ""
        if let auth = LoginSession.shared.getUserModelDefault(keyName: userData)?.data?.login?.authorization {
            authorization = auth
        }
        let header:[String:String] = [
            "Authorization":authorization,
            "device_type":"ios",
            "device_id":"111",
            "current_time_zone":TimeZone.current.identifier,
            "language":"en",
            "Content-Type":"application/json",
            "version":"1.0.0",
            "current_country":"India",
            "lat":"22.12541",
            "lng":"22.12541"
        ]
        
        let param:[String:Any] = [
            "emp_id": employee.empID!
        ]
        Loader.shared.showIndicator(view: self.view)
        K3Webservice.run(urlString: WebUrl.removeEmployee, method: HTTPMethod.post, Param: param, header: header  , type: ForgotPasswordModel.self, viewController: self) { (response) in
            if let isResponse = response {
                if isResponse.status == 1 {
                    Loader.shared.hideIndicator()
                    self.func_Alert(message: isResponse.message ?? "")
                    self.dismissAfetrRemove()
                } else {
                    Loader.shared.hideIndicator()
                    self.func_Alert(message: isResponse.message ?? "")
                }
            }
        }
    }
    
    func notifyEmployee() {
        var authorization = ""
        if let auth = LoginSession.shared.getUserModelDefault(keyName: userData)?.data?.login?.authorization {
            authorization = auth
        }
        let header:[String:String] = [
            "Authorization":authorization,
            "device_type":"ios",
            "device_id":"111",
            "current_time_zone":TimeZone.current.identifier,
            "language":"en",
            "Content-Type":"application/json",
            "version":"1.0.0",
            "current_country":"India",
            "lat":"22.12541",
            "lng":"22.12541"
        ]
        
        let param:[String:Any] = [
            "user_id": employee.userID!,
            "busi_code": employee.businessCode!
        ]
        Loader.shared.showIndicator(view: self.view)
        K3Webservice.run(urlString: WebUrl.notifyEmployee, method: HTTPMethod.post, Param: param, header: header  , type: ForgotPasswordModel.self, viewController: self) { (response) in
            if let isResponse = response {
                if isResponse.status == 1 {
                    Loader.shared.hideIndicator()
                    self.func_Alert(message: isResponse.message ?? "")
                    self.dismissAfetrRemove()
                } else {
                    Loader.shared.hideIndicator()
                    self.func_Alert(message: isResponse.message ?? "")
                }
            }
        }
    }
    
    func getEmployeeDetail() {
        var authorization = ""
        var userId = ""
        var busCode = ""
        if let auth = LoginSession.shared.getUserModelDefault(keyName: userData)?.data?.login?.authorization, let uid = LoginSession.shared.getUserModelDefault(keyName: userData)?.data?.login?.userid, let bCode = LoginSession.shared.getUserModelDefault(keyName: userData)?.data?.login?.busiCode {
            authorization = auth
            userId = uid
            busCode = bCode
        }
        let header:[String:String] = [
            "Authorization":authorization,
            "device_type":"ios",
            "device_id":"111",
            "current_time_zone":TimeZone.current.identifier,
            "language":"en",
            "Content-Type":"application/json",
            "version":"1.0.0",
            "current_country":"India",
            "lat":"22.12541",
            "lng":"22.12541"
        ]
        let param:[String:Any] = [
            "user_id": userId,
            "busi_code": busCode
        ]
        Loader.shared.showIndicator(view: self.view)
        K3Webservice.run(urlString: WebUrl.employeeDetail, method: HTTPMethod.post, Param: param, header: header , type: EmployeeDetailModel.self, viewController: self) { (response) in
            if let isResponse = response {
                Loader.shared.hideIndicator()
                if isResponse.status == 1 {
                    self.showProfileDetails(isResponse)
                } else {
                    self.func_Alert(message: isResponse.message ?? "")
                }
            }
        }
    }
}

