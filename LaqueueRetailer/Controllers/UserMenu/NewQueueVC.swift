//
//  NewQueueVC.swift
//  LaqueueRetailer
//
//  Created by Adityaraj Singh Gaharwar on 05/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class NewQueueVC: UIViewController {
    
    @IBOutlet weak var txt_name: UITextField!
    @IBOutlet weak var txtView_descp: UITextView!
    @IBOutlet var btn_days: [UIButton]!
    @IBOutlet var txt_timmings: [UITextField]!
    @IBOutlet weak var tblView_customDays: UITableView!
    @IBOutlet weak var tblView_offdays: UITableView!
    @IBOutlet weak var btn_check: UIButton!
    //    @IBOutlet weak var btn_numbering: UIButton!
    //    @IBOutlet weak var btn_limit: UIButton!
    //    @IBOutlet weak var btn_callTimer: UIButton!
    @IBOutlet weak var txtView_ack: UITextView!
    @IBOutlet weak var txtView_askUpdate: UITextView!
    @IBOutlet weak var txt_numbering: UITextField!
    @IBOutlet weak var txt_limit: UITextField!
    @IBOutlet weak var txt_calltimer: UITextField!
    
    @IBOutlet weak var txtView_call: UITextView!
    @IBOutlet weak var txt_noSHow: UITextView!
    @IBOutlet weak var btn_callNextCheck: UIButton!
    @IBOutlet weak var btn_callNexPerson: UIButton!
    @IBOutlet weak var btn_callNexPlace: UIButton!
    @IBOutlet weak var btn_persons: UIButton!
    @IBOutlet weak var btn_places: UIButton!
    @IBOutlet weak var lbl_empNames: UILabel!
    @IBOutlet weak var btnProfile: UIButton!
    
    @IBOutlet weak var cnsCustomDaysTableHeight: NSLayoutConstraint!
    @IBOutlet weak var cnsOffDaysTableHeight: NSLayoutConstraint!
    
    var didUpdateQueueList: (() -> Void)?
    let tableCellHeight: CGFloat = 45
    
    let numbering = ["1","2","3","4","5"]
    let timer = ["1","2","3","4","5","6"]
    var arrDays = [String]()
    var schedule:[(day:String,isOff:String,oneTo:String,oneFrom:String,twoTo:String,twoFrom:String)] = []
    var customDay:[(date:String,day:String,oneTo:String,oneFrom:String,twoTo:String,twoFrom:String)] = []
    var offDay:[(date:String,day:String)] = []
    let dropDown = DropDown()
    let dropDownNumbering = DropDown()
    let dropDownTimer = DropDown()
    var timePicker = UIDatePicker()
    var currentTextField = UITextField()
    var numberingCheck = 0
    var callFuncTap = 0
    var callPlaceTap = 0
    var callPersonTap = 0
    var showWarning: Bool = false
    var selectedEmployeesID:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        btnProfile.setProfileButtonImage()
    }
    
    private func setup() {
        btnProfile.setProfileButtonImage()
        registerXib()
        for i in 0...6 {
            schedule.append((day: String(i), isOff: "1", oneTo: "", oneFrom: "", twoTo: "", twoFrom: ""))
        }
        txt_timmings.forEach { (txt) in
            txt.isEnabled = false
            txt.delegate = self
        }
        selectedEmployeesID = UserStore.userID ?? ""
        lbl_empNames.text = UserStore.username
        btn_callNexPerson.isEnabled = false
        btn_callNexPlace.isEnabled = false
        btn_persons.isEnabled = false
        btn_places.isEnabled = false
        txtView_descp.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(updateProfile(_:)), name: .updateUserProfile, object: nil)
    }
    
    @objc func updateProfile(_ notification:Notification) {
        btnProfile.setProfileButtonImage()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    private func checkValuesForALert() {
        if txt_name.text?.trimmingCharacters(in: .whitespacesAndNewlines).count != 0 {
            showWarning = true
        } else if txtView_descp.text?.trimmingCharacters(in: .whitespacesAndNewlines).count != 0 {
            showWarning = true
        } else if schedule.count != 0 {
            showWarning = true
        } else if numberingCheck%2 != 0 {
            showWarning = true
        } else if txt_numbering.text?.trimmingCharacters(in: .whitespacesAndNewlines).count != 0 {
            showWarning = true
        } else if txt_limit.text?.trimmingCharacters(in: .whitespacesAndNewlines).count != 0 {
            showWarning = true
        } else if txt_calltimer.text?.trimmingCharacters(in: .whitespacesAndNewlines).count != 0 {
            showWarning = true
        } else if txtView_ack.text?.trimmingCharacters(in: .whitespacesAndNewlines).count != 0 {
            showWarning = true
        } else if txtView_askUpdate.text?.trimmingCharacters(in: .whitespacesAndNewlines).count != 0 {
            showWarning = true
        } else if txtView_call.text?.trimmingCharacters(in: .whitespacesAndNewlines).count != 0 {
            showWarning = true
        } else if txt_noSHow.text?.trimmingCharacters(in: .whitespacesAndNewlines).count != 0 {
            showWarning = true
        }
    }
    
    private func showWarningAlert() {
        let alert = UIAlertController(title: "Warning", message: "Do you want to proceed, all data will be lost?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
            self.view.endEditing(true)
            self.navigationController?.popViewController(animated: true)
        }))
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func btn_employeeTap(_ sender: UIButton) {
        let nc = self.storyboard?.instantiateViewController(withIdentifier: Controller.SelectEmp) as! SelectEmpVC
        nc.sendData = { (arrData) in
            print(arrData)
            if !arrData.isEmpty {
                var emps = ""
                arrData.forEach { (data) in
                    emps.append(data.name + ",")
                    self.selectedEmployeesID.append(data.id + ",")
                }
                emps.removeLast(1)
                self.selectedEmployeesID.removeLast(1)
                self.lbl_empNames.text = emps
            }
        }
        nc.selectedEmployeesID = selectedEmployeesID
        self.present(nc, animated: true, completion: nil)
    }
    
    @IBAction func btn_profileTap(_ sender: UIButton) {
        checkValuesForALert()
        if !showWarning {
            self.navigationController?.popViewController(animated: true)
        } else {
            showWarningAlert()
        }
    }
    
    @IBAction func btn_notificationTap(_ sender: UIButton) {
        navigateToNavigationView()
    }
    
    @IBAction func btn_dayTap(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            if arrDays.contains("1") {
                arrDays.remove(at: arrDays.firstIndex(of: "1")!)
                sender.setImage(#imageLiteral(resourceName: "radioUnchck"), for: .normal)
                txt_timmings.forEach { (btn) in
                    if btn.tag >= 0 && btn.tag <= 3 {
                        btn.isEnabled = false
                        btn.text = ""
                    }
                }
                schedule[1].day = "1"
                schedule[1].isOff = "1"
                schedule[1].oneTo = ""
                schedule[1].oneFrom = ""
                schedule[1].twoTo = ""
                schedule[1].twoFrom = ""
            } else {
                arrDays.append("1")
                sender.setImage(#imageLiteral(resourceName: "radioBtn"), for: .normal)
                txt_timmings.forEach { (btn) in
                    if btn.tag >= 0 && btn.tag <= 3 {
                        btn.isEnabled = true
                        schedule[1].day = "1"
                        schedule[1].isOff = "0"
                    }
                }
            }
        case 1:
            if arrDays.contains("2") {
                arrDays.remove(at: arrDays.firstIndex(of: "2")!)
                sender.setImage(#imageLiteral(resourceName: "radioUnchck"), for: .normal)
                txt_timmings.forEach { (btn) in
                    if btn.tag >= 4 && btn.tag <= 7 {
                        btn.isEnabled = false
                        btn.text = ""
                    }
                }
                schedule[2].day = "2"
                schedule[2].isOff = "1"
                schedule[2].oneTo = ""
                schedule[2].oneFrom = ""
                schedule[2].twoTo = ""
                schedule[2].twoFrom = ""
            } else {
                arrDays.append("2")
                sender.setImage(#imageLiteral(resourceName: "radioBtn"), for: .normal)
                txt_timmings.forEach { (btn) in
                    if btn.tag >= 4 && btn.tag <= 7 {
                        btn.isEnabled = true
                        schedule[2].day = "2"
                        schedule[2].isOff = "0"
                    }
                }
            }
        case 2:
            if arrDays.contains("3") {
                arrDays.remove(at: arrDays.firstIndex(of: "3")!)
                sender.setImage(#imageLiteral(resourceName: "radioUnchck"), for: .normal)
                txt_timmings.forEach { (btn) in
                    if btn.tag >= 8 && btn.tag <= 11 {
                        btn.isEnabled = false
                        btn.text = ""
                    }
                }
                schedule[3].day = "3"
                schedule[3].isOff = "1"
                schedule[3].oneTo = ""
                schedule[3].oneFrom = ""
                schedule[3].twoTo = ""
                schedule[3].twoFrom = ""
            } else {
                arrDays.append("3")
                sender.setImage(#imageLiteral(resourceName: "radioBtn"), for: .normal)
                txt_timmings.forEach { (btn) in
                    if btn.tag >= 8 && btn.tag <= 11 {
                        btn.isEnabled = true
                        schedule[3].day = "3"
                        schedule[3].isOff = "0"
                    }
                }
            }
        case 3:
            if arrDays.contains("4") {
                arrDays.remove(at: arrDays.firstIndex(of: "4")!)
                sender.setImage(#imageLiteral(resourceName: "radioUnchck"), for: .normal)
                txt_timmings.forEach { (btn) in
                    if btn.tag >= 12 && btn.tag <= 15 {
                        btn.isEnabled = false
                        btn.text = ""
                    }
                }
                schedule[4].day = "4"
                schedule[4].isOff = "1"
                schedule[4].oneTo = ""
                schedule[4].oneFrom = ""
                schedule[4].twoTo = ""
                schedule[4].twoFrom = ""
            } else {
                arrDays.append("4")
                sender.setImage(#imageLiteral(resourceName: "radioBtn"), for: .normal)
                txt_timmings.forEach { (btn) in
                    if btn.tag >= 12 && btn.tag <= 15 {
                        btn.isEnabled = true
                        schedule[4].day = "4"
                        schedule[4].isOff = "0"
                    }
                }
            }
        case 4:
            if arrDays.contains("5") {
                arrDays.remove(at: arrDays.firstIndex(of: "5")!)
                sender.setImage(#imageLiteral(resourceName: "radioUnchck"), for: .normal)
                txt_timmings.forEach { (btn) in
                    if btn.tag >= 16 && btn.tag <= 19 {
                        btn.isEnabled = false
                        btn.text = ""
                    }
                }
                schedule[5].day = "5"
                schedule[5].isOff = "1"
                schedule[5].oneTo = ""
                schedule[5].oneFrom = ""
                schedule[5].twoTo = ""
                schedule[5].twoFrom = ""
            } else {
                arrDays.append("5")
                sender.setImage(#imageLiteral(resourceName: "radioBtn"), for: .normal)
                txt_timmings.forEach { (btn) in
                    if btn.tag >= 16 && btn.tag <= 19 {
                        btn.isEnabled = true
                        schedule[5].day = "5"
                        schedule[5].isOff = "0"
                    }
                }
            }
        case 5:
            if arrDays.contains("6") {
                arrDays.remove(at: arrDays.firstIndex(of: "6")!)
                sender.setImage(#imageLiteral(resourceName: "radioUnchck"), for: .normal)
                txt_timmings.forEach { (btn) in
                    if btn.tag >= 20 && btn.tag <= 23 {
                        btn.isEnabled = false
                        btn.text = ""
                    }
                }
                schedule[6].day = "6"
                schedule[6].isOff = "1"
                schedule[6].oneTo = ""
                schedule[6].oneFrom = ""
                schedule[6].twoTo = ""
                schedule[6].twoFrom = ""
            } else {
                arrDays.append("6")
                sender.setImage(#imageLiteral(resourceName: "radioBtn"), for: .normal)
                txt_timmings.forEach { (btn) in
                    if btn.tag >= 20 && btn.tag <= 23 {
                        btn.isEnabled = true
                        schedule[6].day = "6"
                        schedule[6].isOff = "0"
                    }
                }
            }
        default:
            if arrDays.contains("0") {
                arrDays.remove(at: arrDays.firstIndex(of: "0")!)
                sender.setImage(#imageLiteral(resourceName: "radioUnchck"), for: .normal)
                txt_timmings.forEach { (btn) in
                    if btn.tag >= 24 && btn.tag <= 27 {
                        btn.isEnabled = false
                        btn.text = ""
                    }
                }
                schedule[0].day = "0"
                schedule[0].isOff = "1"
                schedule[0].oneTo = ""
                schedule[0].oneFrom = ""
                schedule[0].twoTo = ""
                schedule[0].twoFrom = ""
            } else {
                arrDays.append("0")
                sender.setImage(#imageLiteral(resourceName: "radioBtn"), for: .normal)
                txt_timmings.forEach { (btn) in
                    if btn.tag >= 24 && btn.tag <= 27 {
                        btn.isEnabled = true
                        schedule[0].day = "0"
                        schedule[0].isOff = "0"
                    }
                }
            }
        }
    }
    
    @IBAction func btn_addDaysTap(_ sender: UIButton) {
        if sender.tag == 0 {
            guard let nc = RetailerStoryBoard.Profile(controller: Controller.CustomDate) as? CustomDateVC else {return}
            if #available(iOS 13.0, *) {
                nc.isModalInPresentation = true
                nc.modalPresentationStyle = .overCurrentContext
            }
            nc.dateCompletion = { date, oneTo, onFrom, twoTo, twoFrom in
                let dateFormater = DateFormatter()
                dateFormater.dateFormat = "dd-MM-yyyy"
                let ndate = dateFormater.date(from: date)
                var dayNum = ""
                switch ndate?.dayOfWeek() {
                case "Monday":
                    dayNum = "1"
                case "Tuesday":
                    dayNum = "2"
                case "Wednesday":
                    dayNum = "3"
                case "Thursday":
                    dayNum = "4"
                case "Friday":
                    dayNum = "5"
                case "Saturday":
                    dayNum = "6"
                default:
                    dayNum = "0"
                }
                self.customDay.append((date: date, day: dayNum, oneTo: oneTo, oneFrom: onFrom, twoTo: twoTo, twoFrom: twoFrom))
                self.cnsCustomDaysTableHeight.constant = CGFloat(CGFloat(self.customDay.count) * self.tableCellHeight)
                self.tblView_customDays.reloadData()
            }
            self.present(nc, animated: true, completion: nil)
        } else {
            guard let nc = RetailerStoryBoard.Profile(controller: Controller.Offdays) as? OffDaysVC else {return}
            if #available(iOS 13.0, *) {
                nc.isModalInPresentation = true
                nc.modalPresentationStyle = .overCurrentContext
            }
            nc.dateCompletion = { date in
                let dateFormater = DateFormatter()
                dateFormater.dateFormat = "dd-MM-yyyy"
                let ndate = dateFormater.date(from: date)
                var dayNum = ""
                switch ndate?.dayOfWeek() {
                case "Monday":
                    dayNum = "1"
                case "Tuesday":
                    dayNum = "2"
                case "Wednesday":
                    dayNum = "3"
                case "Thursday":
                    dayNum = "4"
                case "Friday":
                    dayNum = "5"
                case "Saturday":
                    dayNum = "6"
                default:
                    dayNum = "0"
                }
                self.offDay.append((date: date, day: dayNum))
                self.cnsOffDaysTableHeight.constant = CGFloat(CGFloat(self.offDay.count) * self.tableCellHeight)
                self.tblView_offdays.reloadData()
            }
            self.present(nc, animated: true, completion: nil)
        }
    }
    
    @IBAction func btn_numberCheckTap(_ sender: UIButton) {
        numberingCheck += 1
        if numberingCheck%2 == 0 {
            sender.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
            txt_numbering.isEnabled = false
            //            btn_numbering.setTitle("", for: .normal)
        } else {
            sender.setImage(#imageLiteral(resourceName: "check"), for: .normal)
            txt_numbering.isEnabled = true
        }
    }
    
    @IBAction func btn_numberingTap(_ sender: UIButton) {
        //        dropDownNumbering.anchorView = btn_numbering
        //        dropDownNumbering.dataSource = numbering
        //        dropDownNumbering.bottomOffset = CGPoint(x: 0, y:(dropDownNumbering.anchorView?.plainView.bounds.height)!)
        //        dropDownNumbering.direction = .bottom
        //        dropDownNumbering.selectionAction = { (index, item) in
        //            print(index,item)
        //            sender.setTitle(item, for: .normal)
        //        }
        //        dropDownNumbering.show()
    }
    
    @IBAction func btn_limitTap(_ sender: UIButton) {
        //        dropDownNumbering.anchorView = btn_numbering
        //        dropDownNumbering.dataSource = numbering
        //        dropDownNumbering.bottomOffset = CGPoint(x: 0, y:(dropDownNumbering.anchorView?.plainView.bounds.height)!)
        //        dropDownNumbering.direction = .bottom
        //        dropDownNumbering.selectionAction = { (index, item) in
        //            print(index,item)
        //            sender.setTitle(item, for: .normal)
        //        }
        //        dropDownNumbering.show()
    }
    
    @IBAction func btn_timerTap(_ sender: UIButton) {
        //        dropDownNumbering.anchorView = btn_callTimer
        //        dropDownNumbering.dataSource = timer
        //        dropDownNumbering.bottomOffset = CGPoint(x: 0, y:(dropDownNumbering.anchorView?.plainView.bounds.height)!)
        //        dropDownNumbering.direction = .bottom
        //        dropDownNumbering.selectionAction = { (index, item) in
        //            print(index,item)
        //            sender.setTitle(item, for: .normal)
        //        }
        //        dropDownNumbering.show()
    }
    
    @IBAction func btn_callFuncTap(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            callFuncTap += 1
            if callFuncTap%2 == 0 {
                sender.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
                btn_callNexPerson.setTitle("", for: .normal)
                btn_callNexPlace.setTitle("", for: .normal)
                btn_callNexPerson.isEnabled = false
                btn_callNexPlace.isEnabled = false
            } else {
                sender.setImage(#imageLiteral(resourceName: "check"), for: .normal)
                btn_callNexPerson.isEnabled = true
                btn_callNexPlace.isEnabled = true
            }
        case 1:
            callPersonTap += 1
            if callPersonTap%2 == 0 {
                sender.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
                btn_persons.isEnabled = false
            } else {
                sender.setImage(#imageLiteral(resourceName: "check"), for: .normal)
                btn_persons.isEnabled = true
            }
        default:
            callPlaceTap += 1
            if callPlaceTap%2 == 0 {
                sender.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
                btn_places.isEnabled = false
            } else {
                sender.setImage(#imageLiteral(resourceName: "check"), for: .normal)
                btn_places.isEnabled = true
            }
        }
    }
    
    @IBAction func btn_personTap(_ sender: UIButton) {
        dropDownNumbering.anchorView = btn_persons
        dropDownNumbering.dataSource = timer
        dropDownNumbering.bottomOffset = CGPoint(x: 0, y:(dropDownNumbering.anchorView?.plainView.bounds.height)!)
        dropDownNumbering.direction = .bottom
        dropDownNumbering.selectionAction = { (index, item) in
            print(index,item)
            sender.setTitle(item, for: .normal)
        }
        dropDownNumbering.show()
    }
    
    @IBAction func btn_placeTap(_ sender: UIButton) {
        dropDownNumbering.anchorView = btn_places
        dropDownNumbering.dataSource = timer
        dropDownNumbering.bottomOffset = CGPoint(x: 0, y:(dropDownNumbering.anchorView?.plainView.bounds.height)!)
        dropDownNumbering.direction = .bottom
        dropDownNumbering.selectionAction = { (index, item) in
            print(index,item)
            sender.setTitle(item, for: .normal)
        }
        dropDownNumbering.show()
    }
    
    @IBAction func btn_cancelTap(_ sender: UIButton) {
        checkValuesForALert()
        if !showWarning {
            self.navigationController?.popViewController(animated: true)
        } else {
            showWarningAlert()
        }
    }
    
    @IBAction func btn_saveTap(_ sender: UIButton) {
        if validateInputs() {
            createQueue()
        }
    }
}

extension NewQueueVC: UITextFieldDelegate, UITextViewDelegate {
    
    func registerXib() {
        tblView_customDays.register(UINib(nibName: Cell.CustomDays, bundle: nil), forCellReuseIdentifier: Cell.CustomDays)
        tblView_offdays.register(UINib(nibName: Cell.OffDays, bundle: nil), forCellReuseIdentifier: Cell.OffDays)
    }
    
    func validateInputs() -> Bool {
        var msg = ""
        if txt_name.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            msg = "Please enter queue name"
        } else if txtView_descp.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            msg = "Please enter queue description"
        } else if schedule.count == 0 {
            msg = "Please select queue schedule"
        } else if numberingCheck%2 != 0 && txt_numbering.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            msg = "Please enter numbering"
        } else if txt_limit.text == nil {
            msg = "Please enter queue limit"
        } else if txt_calltimer.text == nil {
            msg = "Please select call timer"
        } else if txtView_ack.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            msg = "Please enter acknowledge message"
        } else if txtView_askUpdate.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            msg = "Please enter ask update message"
        } else if txtView_call.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            msg = "Please enter call message"
        } else if txt_noSHow.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            msg = "Please enter no show message"
        }
        if msg == "" {
            return true
        } else {
            self.func_Alert(message: msg)
            return false
        }
    }
    
    // TimePicker
    func createTimePicker(txtField: UITextField) {
        //toolbar
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        // bar button
        let flexButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let doneBtn = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donePressed))
        toolBar.setItems([flexButton, doneBtn], animated: true)
        
        //assign toolbar
        txtField.inputAccessoryView = toolBar
        //assign picker to textfield
        txtField.inputView = timePicker
        //timepicker mode
        timePicker.datePickerMode = .time
        timePicker.locale = Locale(identifier: "en_gb")
    }
    
    @objc func donePressed() {
        //formatter
        let formatter = DateFormatter()
        formatter.timeStyle = .short
        formatter.dateFormat = "HH:mm"
        currentTextField.text = formatter.string(from: timePicker.date)
        //        self.view.endEditing(true)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        currentTextField = textField
        createTimePicker(txtField: textField)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField.tag {
        case 0:
            schedule[1].oneTo = textField.text ?? ""
        case 1:
            schedule[1].oneFrom = textField.text ?? ""
        case 2:
            schedule[1].twoTo = textField.text ?? ""
        case 3:
            schedule[1].twoFrom = textField.text ?? ""
        case 4:
            schedule[2].oneTo = textField.text ?? ""
        case 5:
            schedule[2].oneFrom = textField.text ?? ""
        case 6:
            schedule[2].twoTo = textField.text ?? ""
        case 7:
            schedule[2].twoFrom = textField.text ?? ""
        case 8:
            schedule[3].oneTo = textField.text ?? ""
        case 9:
            schedule[3].oneFrom = textField.text ?? ""
        case 10:
            schedule[3].twoTo = textField.text ?? ""
        case 11:
            schedule[3].twoFrom = textField.text ?? ""
        case 12:
            schedule[4].oneTo = textField.text ?? ""
        case 13:
            schedule[4].oneFrom = textField.text ?? ""
        case 14:
            schedule[4].twoTo = textField.text ?? ""
        case 15:
            schedule[4].twoFrom = textField.text ?? ""
        case 16:
            schedule[5].oneTo = textField.text ?? ""
        case 17:
            schedule[5].oneFrom = textField.text ?? ""
        case 18:
            schedule[5].twoTo = textField.text ?? ""
        case 19:
            schedule[5].twoFrom = textField.text ?? ""
        case 20:
            schedule[6].oneTo = textField.text ?? ""
        case 21:
            schedule[6].oneFrom = textField.text ?? ""
        case 22:
            schedule[6].twoTo = textField.text ?? ""
        case 23:
            schedule[6].twoFrom = textField.text ?? ""
        case 24:
            schedule[0].oneTo = textField.text ?? ""
        case 25:
            schedule[0].oneFrom = textField.text ?? ""
        case 26:
            schedule[0].twoTo = textField.text ?? ""
        case 27:
            schedule[0].twoFrom = textField.text ?? ""
        default:
            break
        }
        print("This is current schedule --> \(schedule)")
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count
        return numberOfChars < 256    // 10 Limit Value
    }
}

extension NewQueueVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblView_customDays {
            return customDay.count
        } else {
            return offDay.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tblView_customDays {
            let cell = tableView.dequeueReusableCell(withIdentifier: Cell.CustomDays) as! CustomdaysCell
            cell.lbl_date.text = customDay[indexPath.row].date
            cell.lbl_timming.text = "\(customDay[indexPath.row].oneTo)h To \(customDay[indexPath.row].oneFrom)h And \(customDay[indexPath.row].twoTo)h To \(customDay[indexPath.row].twoFrom)"
            cell.didRemoveItem = {
                self.customDay.remove(at: indexPath.row)
                self.cnsCustomDaysTableHeight.constant = self.cnsCustomDaysTableHeight.constant - self.tableCellHeight
                self.tblView_customDays.reloadData()
            }
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: Cell.OffDays) as! OffDaysCell
            cell.lbl_offDate.text = offDay[indexPath.row].date
            cell.didRemoveItem = {
                self.offDay.remove(at: indexPath.row)
                self.cnsOffDaysTableHeight.constant = self.cnsOffDaysTableHeight.constant - self.tableCellHeight
                self.tblView_offdays.reloadData()
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableCellHeight
    }
}

extension NewQueueVC {
    func createQueue() {
        var authorization = ""
        var empID = ""
        var busId = ""
        var busCode = ""
        if let auth = LoginSession.shared.getUserModelDefault(keyName: userData)?.data?.login?.authorization, let id = LoginSession.shared.getUserModelDefault(keyName: userData)?.data?.login?.userid, let bid = LoginSession.shared.getUserModelDefault(keyName: userData)?.data?.login?.busid, let bCode = LoginSession.shared.getUserModelDefault(keyName: userData)?.data?.login?.busiCode {
            authorization = auth
            empID = id
            busId = bid
            busCode = bCode
        }
        var arrSchedule = [[String:AnyObject]]()
        for schdl in schedule {
            let dic = [
                "is_off": schdl.isOff,
                "day": schdl.day,
                "one_to": schdl.oneTo,
                "one_from": schdl.oneFrom,
                "two_to": schdl.twoTo,
                "two_from": schdl.twoFrom
            ]
            arrSchedule.append(dic as [String : AnyObject])
        }
        //        let schdlDict = JSON(arrSchedule)
        var arrCustomDay = [[String:AnyObject]]()
        for custDay in customDay {
            let dic = [
                "custom_date": custDay.date,
                "day": custDay.day,
                "one_to": custDay.oneTo,
                "one_from": custDay.oneFrom,
                "two_to": custDay.twoTo,
                "two_from": custDay.twoFrom
            ]
            arrCustomDay.append(dic as [String : AnyObject])
        }
        //        let custDict = JSON(arrCustomDay)
        var arrOffDay = [[String:AnyObject]]()
        for ofDay in offDay {
            let dic = [
                "custom_date": ofDay.date,
                "day": ofDay.day
            ]
            arrOffDay.append(dic as [String : AnyObject])
        }
        //        let offDayDict = JSON(arrOffDay)
        //        let schdl = convertToDictionary(text: schdlDict.rawString() ?? "")
        //        let custDays = convertToDictionary(text: custDict.rawString() ?? "")
        //        let offDays = convertToDictionary(text: offDayDict.rawString() ?? "")
        
        let header:[String:String] = [
            "Authorization":authorization,
            "device_type":"ios",
            "device_id":"111",
            "current_time_zone":TimeZone.current.identifier,
            "language":"en",
            "Content-Type":"application/json",
            "version":"1.0.0",
            "current_country":"India",
            "lat":"22.12541",
            "lng":"22.12541"
        ]
        let param:[String:Any] = [
            "que_name":txt_name.text ?? "",
            "que_desc":txtView_descp.text ?? "",
            "call_timer":txt_calltimer.text ?? "",
            "number_start":txt_numbering.text ?? "",
            "numbering":txt_numbering.text ?? "",
            "number_prefix":"",
            "capacity":txt_limit.text ?? "",
            "call_next_persons":btn_persons.titleLabel?.text ?? "",
            "call_next_places":btn_places.titleLabel?.text ?? "",
            "emp_id": empID,
            "busi_code":busCode,
            "busi_id":busId,
            "acknowledge_msg":txtView_ack.text ?? "",
            "askupdate_msg":txtView_askUpdate.text ?? "",
            "call_msg": txtView_call.text ?? "",
            "noshow_msg": txt_noSHow.text ?? "",
            "schedule":arrSchedule ,
            "custom_days":arrCustomDay ,
            "off_days":arrOffDay
        ]
        Loader.shared.showIndicator(view: self.view)
        K3Webservice.run(urlString: WebUrl.addQueue, method: HTTPMethod.post, Param: param, header: header , type: NewPasswordModel.self, viewController: self) { (response) in
            if let isResponse = response {
                Loader.shared.hideIndicator()
                if isResponse.status == 1 {
                    self.func_AlertWithOneOption(message: isResponse.message ?? "", optionOne: "Ok") {
                        self.didUpdateQueueList?()
                        self.navigationController?.popViewController(animated: true)
                    }
                } else {
                    self.func_Alert(message: isResponse.message ?? "")
                }
            }
        }
    }
}
