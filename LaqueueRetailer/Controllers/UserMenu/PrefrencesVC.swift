//
//  PrefrencesVC.swift
//  LaqueueRetailer
//
//  Created by Adityaraj Singh Gaharwar on 02/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire


class PrefrencesVC: UIViewController {
    
    @IBOutlet var btn_check: [UIButton]!
    @IBOutlet weak var btnProfile: UIButton!
    
    var isfirstCheck = 0
    var issecondCheck = 0
    var isthirdCheck = 0
    var isfourthCheck = 0
    var isfifthCheck = 0
    var issixthCheck = 0
    var isseventhCheck = 0
    var firstCheck = "0"
    var secondCheck = "0"
    var thirdCheck = "0"
    var fourthCheck = "0"
    var fifthCheck = "0"
    var sixthCheck = "0"
    var seventhCheck = "0"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        btnProfile.setProfileButtonImage()
    }
    
    private func setup() {
        getPrefrences()
        NotificationCenter.default.addObserver(self, selector: #selector(updateProfile(_:)), name: .updateUserProfile, object: nil)
    }
    
    @objc func updateProfile(_ notification:Notification) {
        btnProfile.setProfileButtonImage()
    }
    
    @IBAction func btn_notificationsTap(_ sender: UIButton) {
        navigateToNavigationView()
    }
    
    @IBAction func btn_profileTap(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btn_prefrenceTap(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            isfirstCheck += 1
            if isfirstCheck%2 != 0 {
                sender.setBackgroundImage(#imageLiteral(resourceName: "check"), for: .normal)
                firstCheck = "1"
            } else {
                sender.setBackgroundImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
                firstCheck = "0"
            }
        case 1:
            issecondCheck += 1
            if issecondCheck%2 != 0 {
                sender.setBackgroundImage(#imageLiteral(resourceName: "check"), for: .normal)
                secondCheck = "1"
            } else {
                sender.setBackgroundImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
                secondCheck = "0"
            }
        case 2:
            isthirdCheck += 1
            if isthirdCheck%2 != 0 {
                sender.setBackgroundImage(#imageLiteral(resourceName: "check"), for: .normal)
                thirdCheck = "1"
            } else {
                sender.setBackgroundImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
                thirdCheck = "0"
            }
        case 3:
            isfourthCheck += 1
            if isfourthCheck%2 != 0 {
                sender.setBackgroundImage(#imageLiteral(resourceName: "check"), for: .normal)
                fourthCheck = "1"
            } else {
                sender.setBackgroundImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
                fourthCheck = "0"
            }
        case 4:
            isfifthCheck += 1
            if isfifthCheck%2 != 0 {
                sender.setBackgroundImage(#imageLiteral(resourceName: "check"), for: .normal)
                fifthCheck = "1"
            } else {
                sender.setBackgroundImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
                fifthCheck = "0"
            }
        case 5:
            issixthCheck += 1
            if issixthCheck%2 != 0 {
                sender.setBackgroundImage(#imageLiteral(resourceName: "check"), for: .normal)
                sixthCheck = "1"
            } else {
                sender.setBackgroundImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
                sixthCheck = "0"
            }
        default:
            isseventhCheck += 1
            if isseventhCheck%2 != 0 {
                sender.setBackgroundImage(#imageLiteral(resourceName: "check"), for: .normal)
                seventhCheck = "1"
            } else {
                sender.setBackgroundImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
                seventhCheck = "0"
            }
        }
    }
    
    @IBAction func btn_saveTap(_ sender: UIButton) {
        updatePrefrences()
    }
}

extension PrefrencesVC {
    fileprivate func setPrefrenceChecks(_ isResponse: PrefrencesModel) {
        self.btn_check.forEach { (btn) in
            btn.setBackgroundImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
        }
        if isResponse.data?.prefrence?.ownerRcvNotifi == "1" {
            self.btn_check[0].setBackgroundImage(#imageLiteral(resourceName: "check"), for: .normal)
            isfirstCheck = 1
            firstCheck = "1"
        }
        if isResponse.data?.prefrence?.ownerRcvNotifiQueStart == "1" {
            self.btn_check[1].setBackgroundImage(#imageLiteral(resourceName: "check"), for: .normal)
            issecondCheck = 1
            secondCheck = "1"
        }
        if isResponse.data?.prefrence?.empRcvNotifiQueStart == "1" {
            self.btn_check[2].setBackgroundImage(#imageLiteral(resourceName: "check"), for: .normal)
            isthirdCheck = 1
            thirdCheck = "1"
        }
        if isResponse.data?.prefrence?.acknowledge == "1" {
            self.btn_check[3].setBackgroundImage(#imageLiteral(resourceName: "check"), for: .normal)
            isfourthCheck = 1
            fourthCheck = "1"
        }
        if isResponse.data?.prefrence?.askUpdate == "1" {
            self.btn_check[4].setBackgroundImage(#imageLiteral(resourceName: "check"), for: .normal)
            isfifthCheck = 1
            fifthCheck = "1"
        }
        if isResponse.data?.prefrence?.cancel == "1" {
            self.btn_check[5].setBackgroundImage(#imageLiteral(resourceName: "check"), for: .normal)
            issixthCheck = 1
            sixthCheck = "1"
        }
        if isResponse.data?.prefrence?.noshow == "1" {
            self.btn_check[6].setBackgroundImage(#imageLiteral(resourceName: "check"), for: .normal)
            isseventhCheck = 1
            seventhCheck = "1"
        }
    }
    
    func getPrefrences() {
        var authorization = ""
        if let auth = LoginSession.shared.getUserModelDefault(keyName: userData)?.data?.login?.authorization {
            authorization = auth
        }
        let header:[String:String] = [
            "Authorization":authorization,
            "device_type":"ios",
            "device_id":"111",
            "current_time_zone":TimeZone.current.identifier,
            "language":"en",
            "Content-Type":"application/json",
            "version":"1.0.0",
            "current_country":"India",
            "lat":"22.12541",
            "lng":"22.12541"
        ]
        Loader.shared.showIndicator(view: self.view)
        K3Webservice.run(urlString: WebUrl.getprefrence, method: HTTPMethod.get, Param: nil, header: header , type: PrefrencesModel.self, viewController: self) { (response) in
            if let isResponse = response {
                if isResponse.status == 1 {
                    Loader.shared.hideIndicator()
                    self.setPrefrenceChecks(isResponse)
                } else {
                    Loader.shared.hideIndicator()
                    self.func_Alert(message: isResponse.message ?? "")
                }
            }
        }
    }
    
    func updatePrefrences() {
        var authorization = ""
        if let auth = LoginSession.shared.getUserModelDefault(keyName: userData)?.data?.login?.authorization {
            authorization = auth
        }
        let header:[String:String] = [
            "Authorization":authorization,
            "device_type":"ios",
            "device_id":"111",
            "current_time_zone":TimeZone.current.identifier,
            "language":"en",
            "Content-Type":"application/json",
            "version":"1.0.0",
            "current_country":"India",
            "lat":"22.12541",
            "lng":"22.12541"
        ]
        let param:[String:Any] = [
            "push_notification" : "1",
            "owner_rcv_notifi" : firstCheck,
            "owner_rcv_notifi_que_start" : secondCheck,
            "emp_rcv_notifi_que_start" : thirdCheck,
            "acknowledge": fourthCheck,
            "ask_update": fifthCheck,
            "cancel": sixthCheck,
            "noshow": seventhCheck
        ]
        Loader.shared.showIndicator(view: self.view)
        K3Webservice.run(urlString: WebUrl.updatePrefrences, method: HTTPMethod.post, Param: param, header: header , type: NewPasswordModel.self, viewController: self) { (response) in
            if let isResponse = response {
                if isResponse.status == 1 {
                    Loader.shared.hideIndicator()
                    self.func_AlertWithOneOption(message: isResponse.message ?? "", optionOne: "Ok") {
                        self.getPrefrences()
                    }
                } else {
                    Loader.shared.hideIndicator()
                    self.func_Alert(message: isResponse.message ?? "")
                }
            }
        }
    }
}
