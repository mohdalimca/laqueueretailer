//
//  MyProfileVC.swift
//  LaqueueRetailer
//
//  Created by Adityaraj Singh Gaharwar on 03/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Kingfisher
import Alamofire

class MyProfileVC: UIViewController {
    
    @IBOutlet weak var img_profilePic: UIImageView!
    @IBOutlet weak var lbl_profileStatus: UILabel!
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var lbl_email: UILabel!
    @IBOutlet weak var lbl_profilePromopt1: UILabel!
    @IBOutlet weak var lbl_profilePrompt2: UILabel!
    @IBOutlet weak var txtView_descp: UITextView!
    @IBOutlet weak var txt_address: UITextView!
    @IBOutlet weak var txt_phone: UITextField!
    @IBOutlet weak var txt_country: UITextField!
    @IBOutlet weak var txt_city: UITextField!
    @IBOutlet weak var txt_businessCategory: UITextField!
    @IBOutlet weak var txt_retailCode: UITextField!
    @IBOutlet weak var txt_name: UITextField!
    @IBOutlet weak var txt_dialCode: UITextField!
    @IBOutlet weak var btn_activate: UIButton!
    @IBOutlet weak var btnProfile: UIButton!
    
    var imagePicker: ImagePicker!
    var isFromLogin = false
    var userPic = ""
    var email = ""
    var name = ""
    var userRole = ""
    var jsonArray: [Any] = []
    var arrCountry: [String] = []
    var countryList = CountryList()
    var selectedCountry: Country!
    var arrCategory = [CategoryData]()
    var selectedCategory: CategoryData!
    var profileUpdateType: ProfileUpdateType = .business
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        btnProfile.setProfileButtonImage()
    }
    
    private func setup() {
        selectedCountry = Country(countryCode: "FR", phoneExtension: "33")
        countryList.delegate = self
        txt_dialCode.text = countryCodeText()
        [txt_dialCode, txt_businessCategory, txt_country, txt_city, txt_name, txt_phone, txt_retailCode].forEach {
            $0?.delegate = self
        }
        loadCountryJsonList()
        getRetailerCategory()
        if isFromLogin {
            lbl_email.text = email
            img_profilePic.kf.setImage(with: URL(string: userPic), options: [.transition(.fade(0.5))])
            txt_name.text = name
            btn_activate.setTitle("Activate", for: .normal)
        } else {
            btn_activate.setTitle("Save", for: .normal)
            getProfileDetails()
        }
        NotificationCenter.default.addObserver(self, selector: #selector(updateProfile(_:)), name: .updateUserProfile, object: nil)
    }
    
    private func showPicker() {
        let picker: PickerViewController = RetailerStoryBoard.Profile(controller: Controller.Picker) as! PickerViewController
        picker.delegate = self
        picker.data = arrCategory.map { $0.catName ?? "" }
        picker.modalTransitionStyle = .crossDissolve
        picker.modalPresentationStyle = .overFullScreen
        self.present(picker, animated: true, completion: nil)
    }

    @objc func updateProfile(_ notification:Notification) {
        btnProfile.setProfileButtonImage()
    }

    
    // MARK:- Button Action
    @IBAction func btn_profileTap(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btn_notificationTap(_ sender: UIButton) {
        navigateToNavigationView()
    }
    
    @IBAction func btn_editTap(_ sender: UIButton) {
        enableEditing(enable: true)
    }
    
    @IBAction func btnMapAction(_ sender: UIButton) {
        OODLocationPicker.location.function_ShowPicker(self) { (isLoc, place) in
            if place != nil {
                print("picked address is \(place?.formattedAddress ?? "")")
                print("picked address coordinate \(String(describing: place?.coordinate))")
                self.txt_address.text = place?.formattedAddress
                self.txt_address.resignFirstResponder()
                self.view.endEditing(true)
            }
        }
    }
    
    @IBAction func btn_activateTap(_ sender: UIButton) {
        if validateInputs() {
            updateUserProfile()
        }
    }
    
    @IBAction func btn_profilePicTap(_ sender: UIButton) {
        imagePicker = ImagePicker(presentationController: self, delegate: self)
    }
    
    @IBAction func btnEyeAction(_ sender: UIButton) {
        sender.isSelected = (!sender.isSelected) ? true : false
    }
}

extension MyProfileVC: ImagePickerDelegate, UITextFieldDelegate, CountryListDelegate {
    func selectedCountry(country: Country) {
        selectedCountry = country
        txt_dialCode.text = countryCodeText()
        txt_country.text = selectedCountry.name
        txt_dialCode.resignFirstResponder()
    }
    
    func didSelect(image: UIImage?) {
        guard let img = image else { return }
        img_profilePic.image = img
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch textField {
        case txt_dialCode:
            present(countryList, animated: true, completion: nil)
        case txt_businessCategory:
            showPicker()
        case txt_businessCategory:
            txt_businessCategory.resignFirstResponder()
        default:
            break
        }
    }
    
    private func loadCountryJsonList() {
        guard let path = Bundle.main.path(forResource: "country_list", ofType:"json") else {
            debugPrint( "path not found")
            return
        }
        do{
            let data = try Data(contentsOf: URL(fileURLWithPath: path))
            let json = try JSONSerialization.jsonObject(with: data, options: [])
            if let object = json as? [String : Any]  {
                print(object)
            }
        } catch {
            print(error.localizedDescription)
        }
    }
    
    private func countryCodeText() -> String {
        var strCountry = ""
        strCountry.append(selectedCountry.flag! + " ")
        strCountry.append(selectedCountry.countryCode + " +")
        strCountry.append(selectedCountry.phoneExtension)
        return strCountry
    }
    
    func validateInputs() -> Bool {
        var msg = ""
        if txt_name.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            msg = "Please enter Retailer name"
        } else if txtView_descp.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            msg = "Please enter description"
        } else if txt_dialCode.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            msg = "Please enter dial code"
        } else if txt_phone.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            msg = "Please enter phone number"
        } else if txt_country.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            msg = "Please enter country"
        } else if txt_city.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            msg = "Please enter city"
        } else if txt_businessCategory.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            msg = "Please enter type"
        } else if txt_retailCode.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            msg = "Please enter retailer code"
        } else if txt_address.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            msg = "Please enter address"
        } else if img_profilePic.image == nil {
            msg = "Please select image"
        } else if selectedCategory == nil {
            msg = "Please select business category"
        }
        if msg == "" {
            return true
        } else {
            self.func_Alert(message: msg)
            return false
        }
    }
    
    fileprivate func enableEditing(enable:Bool) {
        self.txt_country.isEnabled = enable
        self.txt_city.isEnabled = enable
        self.txt_businessCategory.isEnabled = enable
        self.txt_retailCode.isEnabled = enable
        self.txtView_descp.isEditable = enable
        self.txt_name.isEnabled = enable
        self.txt_phone.isEnabled = enable
        self.txt_dialCode.isEnabled = enable
    }
    
    fileprivate func populateDetails(_ isResponse:ProfileModel) {
        UserStore.save(userPic: isResponse.data?.profile?.userPic)
        UserStore.save(businessPic: isResponse.data?.profile?.busiImg)
        self.lbl_profilePromopt1.isHidden = false
        self.lbl_profilePrompt2.isHidden = false
        self.img_profilePic.kf.indicatorType = .activity
        self.img_profilePic.kf.setImage(with: URL(string: isResponse.data?.profile?.busiImg ?? ""), options: [.transition(.fade(0.5))])
        
        self.lbl_name.text = "\(isResponse.data?.profile?.firstname ?? "") \(isResponse.data?.profile?.lastname ?? "")"
        self.lbl_email.text = isResponse.data?.profile?.userEmail ?? ""
        self.txt_phone.text = isResponse.data?.profile?.userPhone ?? ""
        if isResponse.data?.profile?.countryCode != "" {
            selectedCountry = Country(countryCode: "", phoneExtension: isResponse.data?.profile?.countryCode ?? "")
        }
        
        txt_dialCode.text = countryCodeText()
        self.txt_dialCode.text = self.countryCodeText()
        self.txt_name.text = isResponse.data?.profile?.busiName ?? ""
        self.txtView_descp.text = isResponse.data?.profile?.busiDescription ?? ""
        self.txt_retailCode.text = isResponse.data?.profile?.busiCode ?? ""
        self.txt_address.text = isResponse.data?.profile?.busiAddress ?? ""
        self.selectedCategory = CategoryData(catID: isResponse.data?.profile?.categaoryID ?? "", catName: isResponse.data?.profile?.categoryName ?? "")
        self.txt_businessCategory.text = self.selectedCategory.catName
        self.txt_city.text = isResponse.data?.profile?.city ?? ""
        self.txt_country.text = isResponse.data?.profile?.country ?? ""
        
        
        if isResponse.data?.profile?.adminStatus == "Approve" {
            self.lbl_profileStatus.text = "Active"
            self.lbl_profileStatus.backgroundColor = #colorLiteral(red: 0.2023587227, green: 0.7949876189, blue: 0.3206152022, alpha: 1)
            self.lbl_profilePromopt1.isHidden = true
            self.lbl_profilePrompt2.isHidden = true
        }
        self.enableEditing(enable: true)
        UserStore.save(businessCode: isResponse.data?.profile?.busiCode ?? "")

    }
}

extension MyProfileVC: GetPickerValueDelagate {
    func getPickerValue(_ value: String, _ index: Int) {
        if index != -1 {
            selectedCategory = arrCategory[index]
            txt_businessCategory.text = selectedCategory.catName?.capitalized
        }
    }
}

// MARK:- API Call
extension MyProfileVC {
    func getProfileDetails() {
        var authorization = ""
        if let auth = LoginSession.shared.getUserModelDefault(keyName: userData)?.data?.login?.authorization {
            authorization = auth
            
        }
        let header:[String:String] = [
            "Authorization":authorization,
            "device_type":"ios",
            "device_id":"111",
            "current_time_zone":TimeZone.current.identifier,
            "language":"en",
            "Content-Type":"application/json",
            "version":"1.0.0",
            "current_country":"India",
            "lat":"22.12541",
            "lng":"22.12541"
        ]
        Loader.shared.showIndicator(view: self.view)
        K3Webservice.run(urlString: WebUrl.userProfile, method: HTTPMethod.get, Param: nil, header: header , type: ProfileModel.self, viewController: self) { (response) in
            if let isResponse = response {
                if isResponse.status == 1 {
                    Loader.shared.hideIndicator()
                    self.populateDetails(isResponse)
                } else {
                    Loader.shared.hideIndicator()
                    self.func_Alert(message: isResponse.message ?? "")
                }
            }
        }
    }
    
    private func getRetailerCategory() {
        var authorization = ""
        if let auth = LoginSession.shared.getUserModelDefault(keyName: userData)?.data?.login?.authorization {
            authorization = auth
            
        }
        let header:[String:String] = [
            "Authorization":authorization,
            "device_type":"ios",
            "device_id":"111",
            "current_time_zone":"",
            "language":"en",
            "Content-Type":"application/json",
            "version":"1.0.0",
            "current_country":"India",
            "lat":"22.12541",
            "lng":"22.12541"
        ]
        Loader.shared.showIndicator(view: self.view)
        K3Webservice.run(urlString: WebUrl.categories, method: HTTPMethod.get, Param: nil, header: header , type: CategoryModel.self, viewController: self) { (response) in
            if let isResponse = response {
                if isResponse.status == 1 {
                    Loader.shared.hideIndicator()
                    self.arrCategory = isResponse.data ?? self.arrCategory
                } else {
                    Loader.shared.hideIndicator()
                    self.func_Alert(message: isResponse.message ?? "")
                }
            }
        }
    }
    
    func updateUserProfile() {
        var authorization = ""
        var userId = ""
        if let auth = LoginSession.shared.getUserModelDefault(keyName: userData)?.data?.login?.authorization, let userID = LoginSession.shared.getUserModelDefault(keyName: userData)?.data?.login?.userid {
            authorization = auth
            userId = userID
        }
        let header:[String:String] = [
            "Authorization":authorization,
            "device_type":"ios",
            "device_id":"111",
            "current_time_zone":TimeZone.current.identifier,
            "language":"en",
            "Content-Type":"application/json",
            "version":"1.0.0",
            "current_country":"India",
            "lat":"22.12541",
            "lng":"22.12541"
        ]
        let param:[String:Any] = [
            "firstname":"",
            "lastname":"",
            "country_code":selectedCountry.phoneExtension,
            "user_phone":txt_phone.text ?? "",
            "type":ProfileUpdateType.business.rawValue,
            "business_name": txt_name.text ?? "",
            "busi_description":txtView_descp.text ?? "",
            "address": txt_address.text ?? "",
            "lat":"22.12541",
            "lng":"22.12541",
            "country":txt_country.text ?? "",
            "city":txt_city.text ?? "",
            "waiting_time":"20",
            "cat_id":selectedCategory.catID ?? ""
        ]
        Loader.shared.showIndicator(view: self.view)
        K3Webservice.run(isMultipart: true, urlString: WebUrl.updateProfile, method: .post, Param: param, header: header, file: ["busi_img" : img_profilePic.image!], type: NewPasswordModel.self, viewController: self, fullAccess: true, encoding: K3encoding.JSON) { (response) in
            if let isResponse = response {
                if isResponse.status == 1 {
                    self.func_AlertWithOneOption(message: isResponse.message ?? "", optionOne: "Ok") {
                        if self.isFromLogin {
                            //                            LoginSession.shared.updateLoginStatus(false)
                            //                            LoginSession.shared.clearUserDefault()
                            //                            self.navigationController?.popViewController(animated: true)
                            //                            LoginSession.shared.getUserModelDefault(keyName: userData)?.data?.login?.userStatus = 1
                            UserDefaults.standard.set(1, forKey: "profileStatus")
                            self.navigationController?.pushViewController(RetailerStoryBoard.Authenticate(controller: Controller.MainContainer), animated: true)
                        } else {
//                            self.getProfileDetails()
                        }
                    }
                } else {
                    self.func_Alert(message: isResponse.message ?? "")
                }
            }
        }
    }
}
