//
//  PauseQueueVC.swift
//  LaqueueRetailer
//
//  Created by Adityaraj Singh Gaharwar on 05/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class PauseQueueVC: UIViewController {

    @IBOutlet weak var lbl_pauseDays: UILabel!
    @IBOutlet weak var lbl_descp: UILabel!
    @IBOutlet var lbl_timings: [UILabel]!
    @IBOutlet var btn_days: [UIButton]!
    @IBOutlet weak var tblView_customDays: UITableView!
    @IBOutlet weak var tblView_offDays: UITableView!
    @IBOutlet weak var lbl_empNames: UILabel!
    @IBOutlet weak var btn_check: UIButton!
    @IBOutlet weak var btn_startNum: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func btn_editTap(_ sender: UIButton) {
    }
    
    @IBAction func btn_activeTap(_ sender: UIButton) {
    }
    
    @IBAction func btn_removeTap(_ sender: UIButton) {
    }
    
}
