//
//  QueueStatusVC.swift
//  LaqueueRetailer
//
//  Created by Adityaraj Singh Gaharwar on 05/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire

class QueueStatusVC: UIViewController {
    
    @IBOutlet weak var lbl_queueName: UILabel!
    @IBOutlet weak var lblMondayScedule: UILabel!
    @IBOutlet weak var lblTuesdayScedule: UILabel!
    @IBOutlet weak var lblWednesdayScedule: UILabel!
    @IBOutlet weak var lblThursdayScedule: UILabel!
    @IBOutlet weak var lblFridayScedule: UILabel!
    @IBOutlet weak var lblSaturdayScedule: UILabel!
    @IBOutlet weak var lblSundayScedule: UILabel!
    
    @IBOutlet weak var txtView_descp: UITextView!
    @IBOutlet weak var btn_status: UIButton!
    @IBOutlet weak var btn_PauseActive: UIButton!
    @IBOutlet var btn_days: [UIButton]!
    @IBOutlet var lbl_dayTimings: [UILabel]!
    @IBOutlet weak var tblView_customDays: UITableView!
    @IBOutlet weak var tblView_offDays: UITableView!
    @IBOutlet weak var lbl_employee: UILabel!
    @IBOutlet weak var lbl_empNames: UILabel!
    @IBOutlet weak var btn_check: UIButton!
    @IBOutlet weak var btn_startNumber: UIButton!
    @IBOutlet weak var stck_buttons: UIStackView!
    
    @IBOutlet weak var viewSunday: UIView!
    @IBOutlet weak var viewMonday: UIView!
    @IBOutlet weak var viewTuesday: UIView!
    @IBOutlet weak var viewWednesday: UIView!
    @IBOutlet weak var viewThursday: UIView!
    @IBOutlet weak var viewFriday: UIView!
    @IBOutlet weak var viewSaturday: UIView!
    @IBOutlet weak var btnProfile: UIButton!
    @IBOutlet weak var cnsCustomDaysTableHeight: NSLayoutConstraint!
    @IBOutlet weak var cnsOffDaysTableHeight: NSLayoutConstraint!
    let tableCellHeight: CGFloat = 45
    
    
    var didUpdateQueueList: (() -> Void)?
    var queueStatus:QueueStatus!
    var queueID:String = ""
    var customDays = 0
    var offDays = 0
    var myQueue:Myqueue!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.populateQueueDetails()
        btnProfile.setProfileButtonImage()
    }
    
    private func setup() {
        self.registerXib()
        NotificationCenter.default.addObserver(self, selector: #selector(updateProfile(_:)), name: .updateUserProfile, object: nil)
    }

    func registerXib() {
        tblView_customDays.register(UINib(nibName: Cell.CustomDays, bundle: nil), forCellReuseIdentifier: Cell.CustomDays)
        tblView_offDays.register(UINib(nibName: Cell.OffDays, bundle: nil), forCellReuseIdentifier: Cell.OffDays)
    }
    
    @objc func updateProfile(_ notification:Notification) {
          btnProfile.setProfileButtonImage()
    }

    private func setStatusButtons() {
        switch queueStatus {
        case .Play:
            btn_status.setTitle("Active", for: .normal)
            btn_status.backgroundColor = #colorLiteral(red: 0.2, green: 0.7843137255, blue: 0.3176470588, alpha: 1)
            btn_PauseActive.setTitle("Pause", for: .normal)
            btn_PauseActive.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        case .Pause:
            btn_status.setTitle("Pause", for: .normal)
            btn_status.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
            btn_PauseActive.setTitle("Active", for: .normal)
            btn_PauseActive.backgroundColor = #colorLiteral(red: 0.2, green: 0.7843137255, blue: 0.3176470588, alpha: 1)
        default:
            break
        }
        btn_check.setImage(UIImage(named: "uncheck"), for: .normal)
        btn_startNumber.setTitle("-", for: .normal)

        if myQueue.numbering != nil && myQueue.numbering != "" && myQueue.numbering != "0" {
            btn_check.setImage(UIImage(named: "check"), for: .normal)
            btn_startNumber.setTitle(myQueue.numberStart ?? "-", for: .normal)
        }
    }
    
    private func updateViewAccordingToDay() {
        
        switch Date().currentDay {
        case 1:
            viewSunday.backgroundColor = #colorLiteral(red: 0.9058823529, green: 0.9019607843, blue: 0.9019607843, alpha: 1)
            lblSundayScedule.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            [ viewMonday, viewTuesday, viewWednesday, viewThursday, viewFriday, viewSaturday].forEach {
                $0?.backgroundColor = #colorLiteral(red: 0.9058823529, green: 0.9647058824, blue: 0.9725490196, alpha: 1)
            }
            [ lblMondayScedule, lblTuesdayScedule, lblWednesdayScedule, lblThursdayScedule, lblFridayScedule, lblSaturdayScedule].forEach {
                $0?.textColor = #colorLiteral(red: 0.2235294118, green: 0.8, blue: 0.8, alpha: 1)
            }
        case 2:
            viewMonday.backgroundColor = #colorLiteral(red: 0.9058823529, green: 0.9019607843, blue: 0.9019607843, alpha: 1)
            lblMondayScedule.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            [viewSunday, viewTuesday, viewWednesday, viewThursday, viewFriday, viewSaturday].forEach {
                $0?.backgroundColor = #colorLiteral(red: 0.9058823529, green: 0.9647058824, blue: 0.9725490196, alpha: 1)
            }
            [lblSundayScedule, lblTuesdayScedule, lblWednesdayScedule, lblThursdayScedule, lblFridayScedule, lblSaturdayScedule].forEach {
                $0?.textColor = #colorLiteral(red: 0.2235294118, green: 0.8, blue: 0.8, alpha: 1)
            }
            
        case 3:
            viewTuesday.backgroundColor = #colorLiteral(red: 0.9058823529, green: 0.9019607843, blue: 0.9019607843, alpha: 1)
            lblTuesdayScedule.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            [viewSunday, viewMonday, viewWednesday, viewThursday, viewFriday, viewSaturday].forEach {
                $0?.backgroundColor = #colorLiteral(red: 0.9058823529, green: 0.9647058824, blue: 0.9725490196, alpha: 1)
            }
            [lblSundayScedule, lblMondayScedule, lblWednesdayScedule, lblThursdayScedule, lblFridayScedule, lblSaturdayScedule].forEach {
                $0?.textColor = #colorLiteral(red: 0.2235294118, green: 0.8, blue: 0.8, alpha: 1)
            }
            
        case 4:
            viewWednesday.backgroundColor = #colorLiteral(red: 0.9058823529, green: 0.9019607843, blue: 0.9019607843, alpha: 1)
            lblWednesdayScedule.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            [viewSunday, viewMonday, viewTuesday, viewThursday, viewFriday, viewSaturday].forEach {
                $0?.backgroundColor = #colorLiteral(red: 0.9058823529, green: 0.9647058824, blue: 0.9725490196, alpha: 1)
            }
            [lblSundayScedule, lblMondayScedule, lblTuesdayScedule, lblThursdayScedule, lblFridayScedule, lblSaturdayScedule].forEach {
                $0?.textColor = #colorLiteral(red: 0.2235294118, green: 0.8, blue: 0.8, alpha: 1)
            }
            
        case 5:
            viewThursday.backgroundColor = #colorLiteral(red: 0.9058823529, green: 0.9019607843, blue: 0.9019607843, alpha: 1)
            lblThursdayScedule.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            [viewSunday, viewMonday, viewTuesday, viewWednesday, viewFriday, viewSaturday].forEach {
                $0?.backgroundColor = #colorLiteral(red: 0.9058823529, green: 0.9647058824, blue: 0.9725490196, alpha: 1)
            }
            [lblSundayScedule, lblMondayScedule, lblTuesdayScedule, lblWednesdayScedule, lblFridayScedule, lblSaturdayScedule].forEach {
                $0?.textColor = #colorLiteral(red: 0.2235294118, green: 0.8, blue: 0.8, alpha: 1)
            }
            
        case 6:
            viewFriday.backgroundColor = #colorLiteral(red: 0.9058823529, green: 0.9019607843, blue: 0.9019607843, alpha: 1)
            lblFridayScedule.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            [viewSunday, viewMonday, viewTuesday, viewWednesday, viewThursday, viewSaturday].forEach {
                $0?.backgroundColor = #colorLiteral(red: 0.9058823529, green: 0.9647058824, blue: 0.9725490196, alpha: 1)
            }
            [lblSundayScedule, lblMondayScedule, lblTuesdayScedule, lblWednesdayScedule, lblThursdayScedule, lblSaturdayScedule].forEach {
                $0?.textColor = #colorLiteral(red: 0.2235294118, green: 0.8, blue: 0.8, alpha: 1)
            }
            
        case 7:
            viewSaturday.backgroundColor = #colorLiteral(red: 0.9058823529, green: 0.9019607843, blue: 0.9019607843, alpha: 1)
            lblSaturdayScedule.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            [viewSunday, viewMonday, viewTuesday, viewWednesday, viewThursday, viewFriday].forEach {
                $0?.backgroundColor = #colorLiteral(red: 0.9058823529, green: 0.9647058824, blue: 0.9725490196, alpha: 1)
            }
            [lblSundayScedule, lblMondayScedule, lblTuesdayScedule, lblWednesdayScedule, lblThursdayScedule, lblFridayScedule].forEach {
                $0?.textColor = #colorLiteral(red: 0.2235294118, green: 0.8, blue: 0.8, alpha: 1)
            }
            
        default:
            break
        }
    }
    
    private func populateQueueDetails() {
        queueStatus = QueueStatus(rawValue: myQueue.status ?? "")
        setStatusButtons()
        queueID = myQueue.queID ?? ""
        lbl_queueName.text = myQueue.queName
        txtView_descp.text = myQueue.queDescription
        var strFrom = ""
        var strTo = ""
        var scehduleTitle = "--"
        var buttonImage: UIImage!
        self.updateViewAccordingToDay()
        if myQueue.schedule != nil {
            for schedule in myQueue.schedule! {
                if schedule.isOff != nil && schedule.isOff != "1" {
                    strFrom = schedule.oneTo ?? ""
                    strTo = schedule.oneFrom ?? ""
                    scehduleTitle = "\(strFrom) to \(strTo)"
                    buttonImage = #imageLiteral(resourceName: "radioBtn")
                } else {
                    scehduleTitle = "--"
                    buttonImage = #imageLiteral(resourceName: "radioUnchck")
                }
                
                switch schedule.days {
                case "1": // 0 is Sunday and onwards
                    lblMondayScedule.text = scehduleTitle
                    btn_days[0].setImage(buttonImage, for: .normal)
                case "2":
                    lblTuesdayScedule.text = scehduleTitle
                    btn_days[1].setImage(buttonImage, for: .normal)
                case "3":
                    lblWednesdayScedule.text = scehduleTitle
                    btn_days[2].setImage(buttonImage, for: .normal)
                case "4":
                    lblThursdayScedule.text = scehduleTitle
                    btn_days[3].setImage(buttonImage, for: .normal)
                case "5":
                    lblFridayScedule.text = scehduleTitle
                    btn_days[4].setImage(buttonImage, for: .normal)
                case "6":
                    lblSaturdayScedule.text = scehduleTitle
                    btn_days[5].setImage(buttonImage, for: .normal)
                case "0":
                    lblSundayScedule.text = scehduleTitle
                    btn_days[6].setImage(buttonImage, for: .normal)
                default:
                    strFrom = ""
                    strTo = ""
                }
            }
        }
        
        DispatchQueue.main.async {
            self.customDays = self.myQueue.customDays?.count ?? 0
            self.cnsCustomDaysTableHeight.constant =
                CGFloat(CGFloat(self.customDays) * self.tableCellHeight)
            self.tblView_customDays.reloadData()
            self.offDays = self.myQueue.offDays?.count ?? 0
            self.cnsOffDaysTableHeight.constant =
                CGFloat(CGFloat(self.offDays) * self.tableCellHeight)
            self.tblView_offDays.reloadData()
            if self.myQueue.empInfo != nil {
                var employees = [String]()
                for temp in self.myQueue.empInfo! {
                    employees.append(temp.empName ?? "")
                }
                self.lbl_empNames.text = employees.joined(separator: ",")
            } else {
                self.lbl_empNames.text = UserStore.username
            }
        }
    }
    
    private func reloadAfterChanges() {
        didUpdateQueueList?()
        switch queueStatus {
        case .Play:
            queueStatus = .Pause
        case .Pause:
            queueStatus = .Play
        default:
            break
        }
        DispatchQueue.main.async {
            self.setStatusButtons()
        }
    }
    
    private func dismissAfterRemove() {
        didUpdateQueueList?()
        DispatchQueue.main.async {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func btn_profileTap(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btn_notificationTap(_ sender: UIButton) {
        navigateToNavigationView()
    }
    
    @IBAction func btn_dayTap(_ sender: UIButton) {
    }
    
    @IBAction func btn_checkTap(_ sender: UIButton) {
    }
    
    @IBAction func btn_editTap(_ sender: UIButton) {
        let editQueueVC = RetailerStoryBoard.Profile(controller: Controller.EditQueueVC) as! EditQueueVC
        editQueueVC.didUpdateQueueList = didUpdateQueueList
        editQueueVC.myQueue = myQueue
        self.navigationController?.pushViewController(editQueueVC, animated: true)
    }
    
    @IBAction func btn_pauseTap(_ sender: UIButton) {
        self.changeQueueStatus()
    }
    
    @IBAction func btn_removeTap(_ sender: UIButton) {
        showConfirmationAlert()
    }
    
    private func showConfirmationAlert() {
        let alert = UIAlertController(title: "Laqueue", message: "Do you want to proceed?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "No", style: .destructive, handler: { (_) in }))
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (_) in
            self.removeQueue()
        }))
        present(alert, animated: true, completion: nil)
    }
}

extension QueueStatusVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblView_customDays {
            return customDays
        } else {
            return offDays
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tblView_customDays {
            //            return UITableViewCell()
            let cell = tableView.dequeueReusableCell(withIdentifier: Cell.CustomDays) as! CustomdaysCell
            cell.btnRemove.isHidden = true
            cell.lbl_date.text = myQueue.customDays?[indexPath.row].customDate
            cell.lbl_timming.text = "\(myQueue.customDays?[indexPath.row].oneTo ?? "")h to \(myQueue.customDays?[indexPath.row].oneFrom ?? "") And \(myQueue.customDays?[indexPath.row].twoTo ?? "") to \(myQueue.customDays?[indexPath.row].twoFrom ?? "")"
            return cell
        }
        else {
            //            return UITableViewCell()
            let cell = tableView.dequeueReusableCell(withIdentifier: Cell.OffDays) as! OffDaysCell
            cell.btnRemove.isHidden = true
            cell.lbl_offDate.text = myQueue.offDays?[indexPath.row].customDate
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension QueueStatusVC {
    func changeQueueStatus() {
        var authorization = ""
        if let auth = LoginSession.shared.getUserModelDefault(keyName: userData)?.data?.login?.authorization {
            authorization = auth
        }
        let header:[String:String] = [
            "Authorization":authorization,
            "device_type":"ios",
            "device_id":"111",
            "current_time_zone":TimeZone.current.identifier,
            "language":"en",
            "Content-Type":"application/json",
            "version":"1.0.0",
            "current_country":"India",
            "lat":"22.12541",
            "lng":"22.12541"
        ]
        
        var status = ""
        if queueStatus == .some(.Play) {
            status = "Pause"
        } else {
            status = "Play"
        }
        let param:[String:Any] = [
            "que_id": queueID,
            "status": status
        ]
        Loader.shared.showIndicator(view: self.view)
        K3Webservice.run(urlString: WebUrl.changeQueueStatus, method: HTTPMethod.post, Param: param, header: header  , type: ForgotPasswordModel.self, viewController: self) { (response) in
            if let isResponse = response {
                if isResponse.status == 1 {
                    Loader.shared.hideIndicator()
                    self.func_Alert(message: isResponse.message ?? "")
                    self.reloadAfterChanges()
                } else {
                    Loader.shared.hideIndicator()
                    self.func_Alert(message: isResponse.message ?? "")
                }
            }
        }
    }
    
    func removeQueue() {
        var authorization = ""
        if let auth = LoginSession.shared.getUserModelDefault(keyName: userData)?.data?.login?.authorization {
            authorization = auth
        }
        let header:[String:String] = [
            "Authorization":authorization,
            "device_type":"ios",
            "device_id":"111",
            "current_time_zone":TimeZone.current.identifier,
            "language":"en",
            "Content-Type":"application/json",
            "version":"1.0.0",
            "current_country":"India",
            "lat":"22.12541",
            "lng":"22.12541"
        ]
        
        let param:[String:Any] = [
            "que_id": queueID
        ]
        Loader.shared.showIndicator(view: self.view)
        K3Webservice.run(urlString: WebUrl.removeQueue, method: HTTPMethod.post, Param: param, header: header  , type: ForgotPasswordModel.self, viewController: self) { (response) in
            if let isResponse = response {
                if isResponse.status == 1 {
                    Loader.shared.hideIndicator()
//                    self.func_Alert(message: isResponse.message ?? "")
                    self.dismissAfterRemove()
                } else {
                    Loader.shared.hideIndicator()
                    self.func_Alert(message: isResponse.message ?? "")
                }
            }
        }
    }
}

