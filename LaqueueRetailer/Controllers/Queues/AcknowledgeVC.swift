//
//  AcknowledgeVC.swift
//  LaqueueRetailer
//
//  Created by Adityaraj Singh Gaharwar on 06/09/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class AcknowledgeVC: UIViewController {

    @IBOutlet weak var txt_mins: UITextField!
    var btnTime:((String) -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func btn_cancelTap(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btn_sendTap(_ sender: UIButton) {
        btnTime?(txt_mins.text ?? "")
        self.dismiss(animated: true, completion: nil)
    }
}
