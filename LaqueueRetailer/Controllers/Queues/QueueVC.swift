//
//  QueueVC.swift
//  LaqueueRetailer
//
//  Created by Adityaraj Singh Gaharwar on 24/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire
import Kingfisher

class QueueVC: UIViewController {
    
    @IBOutlet weak var lbl_queue: UILabel!
    @IBOutlet weak var btn_lock: UIButton!
    @IBOutlet weak var lbl_group: UILabel!
    @IBOutlet weak var cllctn_filter: UICollectionView!
    @IBOutlet weak var lbl_place: UILabel!
    @IBOutlet weak var tblView_queue: UITableView!
    
    var spinner: UIActivityIndicatorView!
    lazy var refresh: UIRefreshControl = {
        let control = UIRefreshControl()
        control.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        return control
    }()
    var queueId = ""
    var queueName: String!
    var selectedQueue: Myqueue!
    var arrPlaces = [Record]()
    var queueStatus:QueueStatus!
    var lockTap = 0
    var currentPageNo:Int = 0
    var totalCount:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.lbl_queue.text = selectedQueue.queName ?? "--"
        self.queueStatus = QueueStatus(rawValue: selectedQueue.status!)
        self.updateStausButton()
        self.getQueueBookings()
    }
    
    private func setup() {
        self.registerXib()
        self.tblView_queue.refreshControl = refresh
        self.addSpinner()
        self.tblView_queue.tableHeaderView?.frame.size = CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.width / 2)
    }
    
    private func updateStausButton() {
        switch queueStatus {
        case .Play:setUnlocked()
        case .Pause: setLocked()
        default:
            break
        }
    }
    
    private func addSpinner() {
        spinner = UIActivityIndicatorView(style: .gray)
        spinner.frame = CGRect(x: 0, y: 0, width: tblView_queue.bounds.size.width, height: 44)
        tblView_queue.tableFooterView = spinner
        tblView_queue.tableFooterView?.isHidden = true
    }
    
    @IBAction func btnlockTap(_ sender: UIButton) {
        lockTap += 1
        if lockTap%2 != 0 {
            changeQueueStatus(status: QueueStatus.Pause.rawValue)
        } else {
            changeQueueStatus(status: QueueStatus.Play.rawValue)
        }
    }
    
    @objc func refresh(_ control:UIRefreshControl) {
        self.currentPageNo = 0
        self.getQueueBookings()
    }
    
    private func loadMore() {
        currentPageNo += 1
        self.getQueueBookings()
    }
    
    private func didUpdateQueueList() {
        self.currentPageNo = 0
        self.getQueueBookings()
    }
    
    private func setBorder(_ btn:UIButton, _ color:UIColor = .lightGray) {
        btn.layer.borderWidth = 0.5
        btn.layer.borderColor = color.cgColor
        btn.layer.masksToBounds = true
    }
}

extension QueueVC: UITableViewDelegate, UITableViewDataSource {
    
    func registerXib() {
        tblView_queue.register(UINib(nibName: Cell.HomeQueue, bundle: nil), forCellReuseIdentifier: Cell.HomeQueue)
        cllctn_filter.register(UINib(nibName: Cell.Filter, bundle: nil), forCellWithReuseIdentifier: Cell.Filter)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrPlaces.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Cell.HomeQueue) as! HomeQueueCell
        var title = getTimeNDateFromTimeStamp(stamp: Int(arrPlaces[indexPath.row].bookingTime!) ?? 0, outFormat: "HH:mm")
        //        title = title + " " + "\((arrPlaces[indexPath.row].firstname ?? ""))" + " " + "\((arrPlaces[indexPath.row].lastname ?? ""))" + " " + "\((arrPlaces[indexPath.row].status ?? ""))"
        title = title + " " + "\((arrPlaces[indexPath.row].firstname ?? ""))" + " " + "\((arrPlaces[indexPath.row].lastname ?? ""))"
        cell.lbl_time.text = title
        cell.img_profile.kf.indicatorType = .activity
        cell.img_profile.kf.setImage(with: URL(string: arrPlaces[indexPath.row].userPic ?? ""), options: [.transition(.fade(0.5))])
        let bookingStatus = BookingStatus(rawValue: arrPlaces[indexPath.row].status!)
        cell.lbl_waiting.text = "Waiting: \(Date().timePassed(from: self.arrPlaces[indexPath.row].bookingTime?.toDouble.dateFromTimestamp() ?? Date()))"
        cell.btn_top.isUserInteractionEnabled = true
        cell.btn_bottom.isHidden = true
        switch bookingStatus {
        case .New:
            cell.btn_top.setTitle("Acknowledge", for: .normal)
            cell.btn_top.setTitleColor(.white, for: .normal)
            cell.btn_top.backgroundColor = #colorLiteral(red: 0.3489781618, green: 0.3490435183, blue: 0.3489740491, alpha: 1)
            cell.btn_bottom.setTitle("Cancel", for: .normal)
            cell.btn_bottom.backgroundColor = #colorLiteral(red: 0.9358869791, green: 0.1608723998, blue: 0.001649452839, alpha: 1)
            cell.btnTop = {
                guard let nc = RetailerStoryBoard.Main(controller: Controller.Acknowledge) as? AcknowledgeVC else {return}
                nc.btnTime = { (time) in
                    self.sendStatus(status: BookingStatus.Acknowledge.rawValue, time: time, bookId: self.arrPlaces[indexPath.row].bookingID ?? "")
                }
                nc.modalPresentationStyle = .overCurrentContext
                self.present(nc, animated: true, completion: nil)
            }
            cell.btnBottom = {
                self.sendStatus(status: BookingStatus.Cancel.rawValue, time: "", bookId: self.arrPlaces[indexPath.row].bookingID ?? "")
            }
        case .Acknowledge:
            cell.btn_top.setTitle("Call", for: .normal)
            cell.btn_top.backgroundColor = #colorLiteral(red: 0.9281743169, green: 0.6307636499, blue: 0.1039407775, alpha: 1)
            cell.btn_bottom.setTitle("Cancel", for: .normal)
            cell.btn_bottom.backgroundColor = #colorLiteral(red: 0.9358869791, green: 0.1608723998, blue: 0.001649452839, alpha: 1)
            cell.btnTop = {
                self.sendStatus(status: BookingStatus.Call.rawValue, time: "", bookId: self.arrPlaces[indexPath.row].bookingID ?? "")
            }
            cell.btnBottom = {
                self.sendStatus(status: BookingStatus.Cancel.rawValue, time: "", bookId: self.arrPlaces[indexPath.row].bookingID ?? "")
            }
        case .Update:
            cell.btn_top.setTitle("Update", for: .normal)
            cell.btn_top.backgroundColor = #colorLiteral(red: 0.3489781618, green: 0.3490435183, blue: 0.3489740491, alpha: 1)
            cell.btn_bottom.setTitle("Cancel", for: .normal)
            cell.btn_bottom.backgroundColor = #colorLiteral(red: 0.9358869791, green: 0.1608723998, blue: 0.001649452839, alpha: 1)
            cell.btnTop = {
                guard let nc = RetailerStoryBoard.Main(controller: Controller.Acknowledge) as? AcknowledgeVC else {return}
                nc.btnTime = { (time) in
                    self.sendStatus(status: BookingStatus.Update.rawValue, time: time, bookId: self.arrPlaces[indexPath.row].bookingID ?? "")
                }
                nc.modalPresentationStyle = .overCurrentContext
                self.present(nc, animated: true, completion: nil)
            }
            cell.btnBottom = {
                self.sendStatus(status: BookingStatus.Cancel.rawValue, time: "", bookId: self.arrPlaces[indexPath.row].bookingID ?? "")
            }
        case .Call:
//            cell.lbl_waiting.text = "Waiting: \(self.arrPlaces[indexPath.row].callTimer ?? "--") min"
            cell.btn_top.setTitle("Accept", for: .normal)
            cell.btn_top.backgroundColor = #colorLiteral(red: 0.2023587227, green: 0.7949876189, blue: 0.3206152022, alpha: 1)
            cell.btn_bottom.setTitle("No Show", for: .normal)
            cell.btn_bottom.backgroundColor = #colorLiteral(red: 0.9358869791, green: 0.1608723998, blue: 0.001649452839, alpha: 1)
            cell.btnTop = {
                self.sendStatus(status: BookingStatus.Accept.rawValue, time: "", bookId: self.arrPlaces[indexPath.row].bookingID ?? "")
            }
            cell.btnBottom = {
                self.sendStatus(status: BookingStatus.Noshow.rawValue, time: "", bookId: self.arrPlaces[indexPath.row].bookingID ?? "")
            }
        case .Cancel:
            cell.btn_top.isUserInteractionEnabled = false
            cell.btn_top.setTitle("Cancelled", for: .normal)
            cell.btn_top.setTitleColor(#colorLiteral(red: 0.9358869791, green: 0.1608723998, blue: 0.001649452839, alpha: 1), for: .normal)
            cell.btn_top.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            setBorder(cell.btn_top, #colorLiteral(red: 0.9358869791, green: 0.1608723998, blue: 0.001649452839, alpha: 1))
            cell.btn_bottom.setTitle("Cancelled", for: .normal)
            cell.btn_bottom.backgroundColor = #colorLiteral(red: 0.9358869791, green: 0.1608723998, blue: 0.001649452839, alpha: 1)
            cell.btnBottom = {
                self.sendStatus(status: BookingStatus.Cancel.rawValue, time: "", bookId: self.arrPlaces[indexPath.row].bookingID ?? "")
            }
        case .Noshow:
            cell.btn_top.isUserInteractionEnabled = false
            cell.btn_top.setTitle("No Show", for: .normal)
            cell.btn_top.setTitleColor(#colorLiteral(red: 0.9358869791, green: 0.1608723998, blue: 0.001649452839, alpha: 1), for: .normal)
            cell.btn_top.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            setBorder(cell.btn_top, #colorLiteral(red: 0.9358869791, green: 0.1608723998, blue: 0.001649452839, alpha: 1))
            cell.btn_bottom.setTitle("No Show", for: .normal)
            cell.btn_bottom.backgroundColor = #colorLiteral(red: 0.9358869791, green: 0.1608723998, blue: 0.001649452839, alpha: 1)
            cell.btnBottom = {
                self.sendStatus(status: BookingStatus.Noshow.rawValue, time: "", bookId: self.arrPlaces[indexPath.row].bookingID ?? "")
            }
        default: // Status is Accept
            cell.btn_top.isHidden = false
            cell.btn_top.isUserInteractionEnabled = false
            cell.btn_top.setTitle("Accepted", for: .normal)
            cell.btn_top.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            cell.btn_top.setTitleColor(#colorLiteral(red: 0.2023587227, green: 0.7949876189, blue: 0.3206152022, alpha: 1), for: .normal)
            setBorder(cell.btn_top, #colorLiteral(red: 0.2023587227, green: 0.7949876189, blue: 0.3206152022, alpha: 1))
        }
        cell.lbl_persons.text = "\(arrPlaces[indexPath.row].people ?? "") person"
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UIScreen.main.bounds.width / 4
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if arrPlaces.count < totalCount && indexPath.row == arrPlaces.count - 1 && totalCount > 10 {
            spinner.startAnimating()
            tableView.tableFooterView?.isHidden = false
            loadMore()
        } else {
            spinner.stopAnimating()
            tableView.tableFooterView?.isHidden = true
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let detailVC =  RetailerStoryBoard.Main(controller: Controller.PlaceDetail) as? PlaceDetailsVC {
            detailVC.detailRecord = arrPlaces[indexPath.row]
            detailVC.didUpdateQueueList = didUpdateQueueList
            self.navigationController?.pushViewController(detailVC, animated: true)
        }
    }
}

extension QueueVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Cell.Filter, for: indexPath) as! FilterCell
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 3, height: collectionView.frame.height)
    }
    
}

// MARK:- API Calls
extension QueueVC {
    func getQueueBookings() {
        var authorization = ""
        var busCode = ""
        if let auth = LoginSession.shared.getUserModelDefault(keyName: userData)?.data?.login?.authorization, let bCode = LoginSession.shared.getUserModelDefault(keyName: userData)?.data?.login?.busiCode {
            authorization = auth
            busCode = bCode
        }
        
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "dd-MM-yyyy"
        let ndate = Date()
        var dayNum = ""
        switch ndate.dayOfWeek() {
        case "Monday":
            dayNum = "1"
        case "Tuesday":
            dayNum = "2"
        case "Wednesday":
            dayNum = "3"
        case "Thursday":
            dayNum = "4"
        case "Friday":
            dayNum = "5"
        case "Saturday":
            dayNum = "6"
        default:
            dayNum = "0"
        }
        
//        dayNum = "6"
        
        let header:[String:String] = [
            "Authorization":authorization,
            "device_type":"ios",
            "device_id":"111",
            "current_time_zone":TimeZone.current.identifier,
            "language":"en",
            "Content-Type":"application/json",
            "version":"1.0.0",
            "current_country":"India",
            "lat":"22.12541",
            "lng":"22.12541"
        ]
        let param:[String:Any] = [
            "page_no":String(currentPageNo),
            "busi_code":busCode,
            "que_id":queueId,
            "day":dayNum
        ]
        Loader.shared.showIndicator(view: self.view)
        K3Webservice.run(urlString: WebUrl.queueBookings, method: HTTPMethod.post, Param: param, header: header , type: QueueBookingsModel.self, viewController: self) { (response) in
            if let isResponse = response {
                self.tblView_queue.refreshControl?.endRefreshing()
                if isResponse.status == 1 {
                    self.arrPlaces = isResponse.data?[0].record ?? []
                    self.totalCount = isResponse.data?[0].totalcount?.toInt ?? 0
                    var place = "place"
                    if self.arrPlaces.count > 1 {
                        place = "places"
                    }
                    self.lbl_place.text = "\(self.arrPlaces.count) \(place)"
                } else {
                    self.arrPlaces.removeAll()
                    self.lbl_place.text = "No place"
                    self.func_Alert(message: isResponse.message ?? "")
                }
                Loader.shared.hideIndicator()
                self.tblView_queue.reloadSections(IndexSet(integer: 0), with: .fade)
            }
        }
    }
    
    private func setLocked() {
        DispatchQueue.main.async {
            self.btn_lock.setTitle("Locked", for: .normal)
            self.btn_lock.setImage(UIImage(named: "Locked"), for: .normal)
            self.btn_lock.backgroundColor = #colorLiteral(red: 0.939852953, green: 0.1607407928, blue: 0.0004201453703, alpha: 1)
        }
    }
    
    private func setUnlocked() {
        DispatchQueue.main.async {
            self.btn_lock.setTitle("Unlocked", for: .normal)
            self.btn_lock.setImage(UIImage(named: "Unlock"), for: .normal)
            self.btn_lock.backgroundColor = #colorLiteral(red: 0.2071948349, green: 0.7949840426, blue: 0.3205820322, alpha: 1)
        }
    }
    
    
    func changeQueueStatus(status:String) {
        var authorization = ""
        if let auth = LoginSession.shared.getUserModelDefault(keyName: userData)?.data?.login?.authorization {
            authorization = auth
        }
        let header:[String:String] = [
            "Authorization":authorization,
            "device_type":"ios",
            "device_id":"111",
            "current_time_zone":TimeZone.current.identifier,
            "language":"en",
            "Content-Type":"application/json",
            "version":"1.0.0",
            "current_country":"India",
            "lat":"22.12541",
            "lng":"22.12541"
        ]
        
        let param:[String:Any] = [
            "que_id": queueId,
            "status": status
        ]
        Loader.shared.showIndicator(view: self.view)
        K3Webservice.run(urlString: WebUrl.changeQueueStatus, method: HTTPMethod.post, Param: param, header: header  , type: ForgotPasswordModel.self, viewController: self) { (response) in
            if let isResponse = response {
                if isResponse.status == 1 {
                    Loader.shared.hideIndicator()
                    (self.lockTap%2 != 0) ? self.setLocked() : self.setUnlocked()
                    self.func_Alert(message: isResponse.message ?? "")
                } else {
                    Loader.shared.hideIndicator()
                    self.func_Alert(message: isResponse.message ?? "")
                }
            }
        }
    }
    
    func sendStatus(status:String,time:String?,bookId:String) {
        var authorization = ""
        if let auth = LoginSession.shared.getUserModelDefault(keyName: userData)?.data?.login?.authorization {
            authorization = auth
        }
        let header:[String:String] = [
            "Authorization":authorization,
            "device_type":"ios",
            "device_id":"111",
            "current_time_zone":TimeZone.current.identifier,
            "language":"en",
            "Content-Type":"application/json",
            "version":"1.0.0",
            "current_country":"India",
            "lat":"22.12541",
            "lng":"22.12541"
        ]
        let param:[String:Any] = [
            "booking_id":bookId,
            "status":status,
            "waiting_time":time ?? "",
            "last_status_update":String(UInt64(Date().timeIntervalSince1970))
        ]
        Loader.shared.showIndicator(view: self.view)
        K3Webservice.run(urlString: WebUrl.bookingStatus, method: HTTPMethod.post, Param: param, header: header , type: ForgotPasswordModel.self, viewController: self) { (response) in
            if let isResponse = response {
                if isResponse.status == 1 {
                    Loader.shared.hideIndicator()
                    if status == "Update" {
                        self.sendStatus(status: "Acknowledge", time: time, bookId: bookId)
                        return
                    }
                    self.func_AlertWithOneOption(message: isResponse.message ?? "", optionOne: "OK") {
                        self.getQueueBookings()
                    }
                } else {
                    Loader.shared.hideIndicator()
                    self.func_Alert(message: isResponse.message ?? "")
                }
            }
        }
    }
}
