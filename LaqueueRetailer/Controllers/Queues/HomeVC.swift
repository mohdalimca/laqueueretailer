//
//  HomeVC.swift
//  LaqueueRetailer
//
//  Created by Adityaraj Singh Gaharwar on 24/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire

class HomeVC: UIViewController {

    @IBOutlet weak var img_profileStatus: UIImageView!
    @IBOutlet weak var lbl_retailerInfo: UILabel!
    @IBOutlet weak var lbl_infoOptFirst: UILabel!
    @IBOutlet weak var btn_firstOpt: UIButton!
    @IBOutlet weak var lbl_infoOptSecond: UILabel!
    @IBOutlet weak var btn_secondOpt: UIButton!
    @IBOutlet weak var lbl_infoOptThird: UILabel!
    @IBOutlet weak var btn_thirdOpt: UIButton!
    @IBOutlet weak var viewStatistics: UIView! {
        didSet {
            viewStatistics.isHidden = true
        }
    }
    
    var noOfQueues = 0
    var didUpdateInformation: ((Int) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    private func setup() {
        NotificationCenter.default.addObserver(self, selector: #selector(updateView), name: NSNotification.Name(rawValue: "UpdateUserDetails"), object: nil)
    }
    
    @objc func updateView(_ notification:Notification) {
        if let status = notification.userInfo?["filled"] as? String, status == "yes" {
            DispatchQueue.main.async {
                
            }
        }
    }
    
    func updateInformation(_ queues:Int) {
        self.noOfQueues = queues
        DispatchQueue.main.async {
            self.setInfoView()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        DispatchQueue.main.async {
            self.setInfoView()
        }
    }
    
    @IBAction func btn_addTap(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            let vc = RetailerStoryBoard.Profile(controller: Controller.NewEmployee) as! NewEmployeeVC
            vc.isEdit = true
            self.navigationController?.pushViewController(vc, animated: true)
        case 1: 
            self.navigationController?.pushViewController(RetailerStoryBoard.Profile(controller: Controller.MyProfile), animated: true)
        default:
            self.navigationController?.pushViewController(RetailerStoryBoard.Profile(controller: Controller.NewQueue), animated: true)
        }
    }
    
}

extension HomeVC {
    
    private func setOwnerLabels() {
        if UserStore.profileStatus == 0 || UserStore.profileStatus == nil {
            lbl_infoOptFirst.text = "Missing details of Owner."
            btn_firstOpt.isHidden = false
        } else {
            lbl_infoOptFirst.text = "Owner details is filled."
            btn_firstOpt.isHidden = true
        }
    }
    
    private func setQueueLabel() {
        if noOfQueues != 0 {
            lbl_infoOptThird.text = "\(noOfQueues) queue filled."
            btn_thirdOpt.isHidden = true
        } else {
            lbl_infoOptThird.text = "No queue defined."
            btn_thirdOpt.isHidden = false
        }
    }
    
    private func setInfoView() {
        if UserStore.retailDetails == 0 || UserStore.retailDetails == nil {
            img_profileStatus.backgroundColor = .red
            lbl_retailerInfo.text = "Retailer not visible for customer."
            lbl_infoOptSecond.text = "Missing details of Retail."
            btn_firstOpt.isHidden = false
            setOwnerLabels()
            setQueueLabel()
        } else {
            img_profileStatus.backgroundColor = .green
            lbl_retailerInfo.text = "Retail is visible to customers."
            lbl_infoOptSecond.text = "Retail detail is filled."
            btn_secondOpt.isHidden = true
            setOwnerLabels()
            setQueueLabel()
        }
        viewStatistics.isHidden = false
    }
}
