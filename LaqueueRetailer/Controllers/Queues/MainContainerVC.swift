//
//  MainContainerVC.swift
//  LaqueueRetailer
//
//  Created by Adityaraj Singh Gaharwar on 24/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class MainContainerVC: UIViewController {
    
    @IBOutlet weak var lblInfoTitle: UILabel!
    @IBOutlet weak var lblInfoOptionFirst: UILabel!
    @IBOutlet weak var lblInfoOptionSecond: UILabel!
    @IBOutlet weak var lblInfoOptionThird: UILabel!
    @IBOutlet weak var lbl_headerName: UILabel!
    
    @IBOutlet weak var btnOptionFirst: UIButton!
    @IBOutlet weak var btnOptionSecond: UIButton!
    @IBOutlet weak var btnOptionThird: UIButton!
    @IBOutlet weak var btn_profile: UIButton!
    @IBOutlet weak var imageCircle: UIView!
    @IBOutlet weak var viewInfo: UIView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var cllctn_tab: UICollectionView!
    
    let homeVC: HomeVC = RetailerStoryBoard.Authenticate(controller: Controller.Home) as! HomeVC
    let queueVC: QueueVC = RetailerStoryBoard.Authenticate(controller: Controller.Queue) as! QueueVC
    
    var selectedVC:UIViewController!
    var queueData = [Myqueue]()
    var selectedIndex: Int = 0
    var notificationQueueID: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
        getQueuelist()
        contentView.subviews.forEach { (view) in
            view.removeFromSuperview()
        }
        btn_profile.setProfileButtonImage()
        if selectedIndex == 0 {
            addHomeVC()
        } else {
            collectionView(cllctn_tab, didSelectItemAt: IndexPath(item: selectedIndex, section: 0))
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    private func setup() {
        registerXib()
        addHomeVC()
        btn_profile.setProfileButtonImage()
        NotificationCenter.default.addObserver(self, selector: #selector(updateProfile(_:)), name: .updateUserProfile, object: nil)
    }
    
    @objc func updateProfile(_ notification:Notification) {
        btn_profile.setProfileButtonImage()
    }
    
    private func addHomeVC() {
        addChild(homeVC)
        homeVC.view.frame = self.contentView.bounds
        contentView.addSubview(homeVC.view)
        selectedVC = homeVC
        homeVC.didMove(toParent: self)
    }
    
    private func addQueueVC(_ index:Int) {
        queueVC.queueStatus = QueueStatus(rawValue: queueData[index - 1].status!)
        queueVC.selectedQueue = queueData[index - 1]
        addChild(queueVC)
        queueVC.view.frame = self.contentView.bounds
        contentView.addSubview(queueVC.view)
        selectedVC = queueVC
        queueVC.didMove(toParent: self)
    }
    
    private func setInfoView() {
        if UserStore.profileStatus == 0 || UserStore.profileStatus == nil { //
            imageCircle.backgroundColor = .red
            lblInfoTitle.text = "Retailer not visible for customer"
            lblInfoOptionFirst.text = "Missing details of Owner"
            lblInfoOptionSecond.text = "Missing details of Retail"
            lblInfoOptionThird.text = "No queue defined"
        } else {
            imageCircle.backgroundColor = .green
            lblInfoTitle.text = "Retailer visible"
            lblInfoOptionFirst.text = "Owner details filled"
            lblInfoOptionSecond.text = "Retailer details filled"
            lblInfoOptionThird.text = "\(queueData.count) queue filled"
            [btnOptionFirst, btnOptionSecond, btnOptionThird].forEach {
                $0?.isHidden = true
            }
            Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: false)
        }
        [btnOptionFirst, btnOptionSecond, btnOptionThird].forEach {
            $0?.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
        }
        self.updateInfoView(hidden: false)
    }
    
    @objc func updateTimer() {
        self.updateInfoView(hidden: true)
    }
    
    
    private func updateInfoView(hidden:Bool) {
        if !hidden {
            self.viewInfo.fadeIn()
        } else {
            self.viewInfo.fadeOut()
        }
    }
    
    private func checkIsFromNotification() {
        if notificationQueueID != "" {
            selectedIndex = queueData.firstIndex(where: { $0.queID == notificationQueueID}) ?? selectedIndex
        }
    }

    // MARK:- Button Actions
    @objc func buttonPressed(_ sender:UIButton) {
        switch sender {
        case btnOptionFirst:
            self.navigationController?.pushViewController(RetailerStoryBoard.Profile(controller: Controller.MyProfile), animated: true)
        case btnOptionSecond:
            self.navigationController?.pushViewController(RetailerStoryBoard.Profile(controller: Controller.MyProfile), animated: true)
        case btnOptionThird:
            self.navigationController?.pushViewController(RetailerStoryBoard.Profile(controller: Controller.NewQueue), animated: true)
        default:
            break
        }
    }
    
    @IBAction func btn_profileTap(_ sender: UIButton) {
        self.navigationController?.pushViewController(RetailerStoryBoard.Profile(controller: Controller.Menu), animated: true)
    }
    
    @IBAction func btn_notificationTap(_ sender: UIButton) {
        navigateToNavigationView()
    }
}

extension MainContainerVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func registerXib() {
        cllctn_tab.register(UINib(nibName: Cell.Tab, bundle: nil), forCellWithReuseIdentifier: Cell.Tab)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return queueData.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Cell.Tab, for: indexPath) as! TabCell
        if indexPath.row == 0 {
            cell.btn_tab.setTitle("Home", for: .normal)
            cell.isSelected = true
//            cell.btn_tab.setBackgroundImage(#imageLiteral(resourceName: "navbar"), for: .normal)
            
        } else {
            cell.btn_tab.setTitle("\(queueData[indexPath.item - 1].queName ?? "")", for: .normal)
        }
        cell.btn_tab.setBackgroundImage(#imageLiteral(resourceName: "cancelBorder"), for: .normal)
        if indexPath.item == selectedIndex {
            cell.btn_tab.setBackgroundImage(#imageLiteral(resourceName: "navbar"), for: .normal)
        }
//        cell.btn_tab.setTitle("Queue \(indexPath.item + 1)", for: .normal)
//        if selectedIndex == indexPath.item {
//            cell.btn_tab.setBackgroundImage(#imageLiteral(resourceName: "navbar"), for: .normal)
//        } else {
//            cell.btn_tab.setBackgroundImage(#imageLiteral(resourceName: "cancelBorder"), for: .normal)
//        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        contentView.subviews.forEach { (view) in
            view.removeFromSuperview()
        }
        if indexPath.item == 0 {
            homeVC.noOfQueues = self.queueData.count
            addHomeVC()
        } else {
            queueVC.queueId = queueData[indexPath.row - 1].queID ?? ""
            addQueueVC(indexPath.item)
        }
        print(indexPath.row)
        selectedIndex = indexPath.item
        collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) as? TabCell {
            cell.btn_tab.setBackgroundImage(#imageLiteral(resourceName: "cancelBorder"), for: .normal)
            if cell.btn_tab.title(for: .normal) == "Home" {
                homeVC.willMove(toParent: nil)
                homeVC.view.removeFromSuperview()
                homeVC.removeFromParent()
            } else {
                queueVC.willMove(toParent: nil)
                queueVC.view.removeFromSuperview()
                queueVC.removeFromParent()
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width / 3.3, height: collectionView.frame.height)
    }
}


extension MainContainerVC {
    func getQueuelist() {
        var authorization = ""
        if let auth = LoginSession.shared.getUserModelDefault(keyName: userData)?.data?.login?.authorization {
            authorization = auth
            
        }
        let header:[String:String] = [
            "Authorization":authorization,
            "device_type":"ios",
            "device_id":"111",
            "current_time_zone":TimeZone.current.identifier,
            "language":"en",
            "Content-Type":"application/json",
            "version":"1.0.0",
            "current_country":"India",
            "lat":"22.12541",
            "lng":"22.12541"
        ]
        if selectedIndex == 0 {
            Loader.shared.showIndicator(view: self.view)
        }
        K3Webservice.run(urlString: WebUrl.getQueues, method: .get, Param: nil, header: header , type: QueueListModel.self, viewController: self) { (response) in
            if let isResponse = response {
                if isResponse.status == 1 {
                    Loader.shared.hideIndicator()
                    self.queueData = isResponse.data?[0].myqueue ?? []
                    self.homeVC.noOfQueues = self.queueData.count
                    self.homeVC.updateInformation(self.queueData.count)
                    self.checkIsFromNotification()
                    let indexPath = IndexPath(item: self.selectedIndex, section: 0)
                    self.cllctn_tab.selectItem(at: indexPath, animated: true, scrollPosition: .left) // <--
                    self.cllctn_tab.delegate?.collectionView?(self.cllctn_tab, didSelectItemAt: indexPath)
                    self.cllctn_tab.reloadData()
                } else {
                    Loader.shared.hideIndicator()
                    self.func_Alert(message: isResponse.message ?? "")
                }
            }
        }
    }
}
