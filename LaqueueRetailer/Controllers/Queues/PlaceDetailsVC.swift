//
//  PlaceDetailsVC.swift
//  LaqueueRetailer
//
//  Created by Adityaraj Singh Gaharwar on 11/09/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire
import Kingfisher

class PlaceDetailsVC: UIViewController {
    
    @IBOutlet weak var lbl_custName: UILabel!
    @IBOutlet weak var lbl_queueName: UILabel!
    @IBOutlet weak var img_profile: UIImageView!
    @IBOutlet weak var lbl_queueStatus: UILabel!
    @IBOutlet weak var lbl_joined: UILabel!
    @IBOutlet weak var lbl_dateTime: UILabel!
    @IBOutlet weak var lbl_places: UILabel!
    @IBOutlet weak var lbl_descp: UILabel!
    @IBOutlet weak var lbl_msg: UILabel!
    @IBOutlet weak var lbl_timer: UILabel!
    @IBOutlet weak var lblWaitingTime: UILabel!
    @IBOutlet weak var view_timer: UIView!
    @IBOutlet weak var btnLeft: UIButton!
    @IBOutlet weak var btnRight: UIButton!
    @IBOutlet weak var btnProfile: UIButton!
    
    var bookingId = ""
    var timeSeconds = 0
    var timer:Timer?
    var waitingTime:String?
    var bookingStatus: BookingStatus!
    var detailRecord: Record!
    var didUpdateQueueList: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        self.btnProfile.setProfileButtonImage()
        self.view_timer.isHidden = true
    }
    
    private func setup() {
        if bookingId != "" {
            getPlaceData()
        } else {
            self.populateDetails()
            NotificationCenter.default.addObserver(self, selector: #selector(updateProfile(_:)), name: .updateUserProfile, object: nil)
        }
    }
    
    @objc func updateProfile(_ notification:Notification) {
        btnProfile.setProfileButtonImage()
    }
    
    func runTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(self.updateTimer)), userInfo: nil, repeats: true)
    }
    
    @objc func updateTimer() {
        timeSeconds -= 1     //This will decrement(count down)the timeSeconds.
        var strTime = ""
        let hours = timeSeconds / 3600
        let minutes = timeSeconds / 60 % 60
        let seconds = timeSeconds % 60
        if hours > 0 {
            strTime = String(format:" %02i:%02i:%02i hr", hours, minutes, seconds)
        } else {
            strTime = String(format:" %02i:%02i min", minutes, seconds)
        }
        lbl_timer.text = strTime //This will update the label.
        if timeSeconds <= 0 {
            lbl_timer.text = "00 : 00 min"
            timer?.invalidate()
            timer = nil
            timeSeconds = 30
        }
    }
    
    private func startTimer(_ waitingTime:String?, _ lastStatusUpdate:String? ) {
        timeSeconds = (waitingTime ?? "0").toInt
        timeSeconds = timeSeconds * 60
        let fromDate = (lastStatusUpdate ?? "0").toDouble.dateFromTimestamp()
        print("from date \(fromDate)")
        let toDate = (Date().timeIntervalSince1970).dateFromTimestamp()
        print("to date \(toDate)")
        let difference = Calendar.current.dateComponents([.second], from: fromDate, to: toDate)
        print("seconds difference \(difference)")
        if (difference.second ?? 0) > 0 {
            timeSeconds = timeSeconds - difference.second!
        }
        self.runTimer()
    }
    
    private func populateDetails() {
        btnLeft.titleEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        btnRight.titleEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        lbl_custName.text = "\(detailRecord.firstname ?? "") \(detailRecord.lastname ?? "")"
        lbl_queueName.text = detailRecord.queName
        lbl_dateTime.text = detailRecord.bookingTime?.toDouble.placeDetailDate
        var person = ""
        if (detailRecord.people?.toInt ?? 0) > 0 {
            person = "\(detailRecord.people ?? "") Persons"
        } else {
            person = "\(detailRecord.people ?? "") Person"
        }
        img_profile.kf.setImage(with: URL(string: detailRecord.userPic ?? ""), options: [.transition(.fade(0.5))])
        lblWaitingTime.text = "Waiting: \(Date().timePassed(from: detailRecord.bookingTime?.toDouble.dateFromTimestamp() ?? Date()))"
        lbl_places.text = "Place \((detailRecord.totalQueueNo?.toInt ?? 0) + 1) -  \(person)"
        lbl_descp.text = detailRecord.queueDescription
        bookingStatus = BookingStatus(rawValue: detailRecord.status!)
        
        lbl_queueStatus.backgroundColor = #colorLiteral(red: 0.231372549, green: 0.2705882353, blue: 0.3098039216, alpha: 1)
        lbl_timer.isHidden = true
        btnRight.isHidden = false
        
        switch bookingStatus {
        case .Acknowledge:
            lbl_queueStatus.text = "Acknowledge"
            lbl_msg.text = "Call: \(lbl_custName.text ?? "") so they show-up."
            btnLeft.backgroundColor = #colorLiteral(red: 1, green: 0.7529411765, blue: 0, alpha: 1)
            btnRight.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.1607843137, blue: 0, alpha: 1)
            btnLeft.setTitle("Call", for: .normal)
            btnRight.setTitle("Cancel", for: .normal)
            startTimer(detailRecord.waitingTime, detailRecord.lastStatusUpdate)
            
        case .Update:
            lbl_queueStatus.text = "Ask Update"
            lbl_msg.text = "Call: \(lbl_custName.text ?? "") so they show-up."
            btnLeft.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.631372549, blue: 0.1019607843, alpha: 1)
            btnRight.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.1607843137, blue: 0, alpha: 1)
            btnLeft.setTitle("Update", for: .normal)
            btnRight.setTitle("Cancel", for: .normal)
            startTimer(detailRecord.waitingTime, detailRecord.lastStatusUpdate)
            
        case .Cancel:
            lbl_queueStatus.text = "Cancelled"
            lbl_queueStatus.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.1607843137, blue: 0, alpha: 1)
            lbl_msg.text = "Cancel: You have cancelled \(lbl_custName.text ?? "") place at \(detailRecord.lastStatusUpdate?.toDouble.placeDetailDate ?? "")."
            btnRight.isHidden = true
            btnLeft.isHidden = true
            lbl_timer.isHidden = true
            
        case .Noshow:
            lbl_queueStatus.text = "Noshow"
            lbl_queueStatus.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.1607843137, blue: 0, alpha: 1)
            lbl_msg.text = "Noshow: You have signal as Noshow to \(lbl_custName.text ?? "") place at \(detailRecord.lastStatusUpdate?.toDouble.placeDetailDate ?? "")."
            btnRight.isHidden = true
            btnLeft.isHidden = true
            lbl_timer.isHidden = true
            
        case .Call:
            lbl_queueStatus.text = "Call"
            lbl_queueStatus.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.631372549, blue: 0.1019607843, alpha: 1)
            lbl_msg.text = "You have called \(lbl_custName.text ?? "")"
            btnLeft.backgroundColor = #colorLiteral(red: 0.2, green: 0.7843137255, blue: 0.3176470588, alpha: 1)
            btnRight.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.1607843137, blue: 0, alpha: 1)
            btnLeft.setTitle("Accept", for: .normal)
            btnRight.setTitle("NoShow", for: .normal)
            lbl_timer.isHidden = false
            startTimer(detailRecord.callTimer, detailRecord.lastStatusUpdate)
            
        case .Accept:
            lbl_queueStatus.text = "Accepted"
            lbl_queueStatus.backgroundColor = #colorLiteral(red: 0.2, green: 0.7843137255, blue: 0.3176470588, alpha: 1)
            lbl_msg.text = "You have accepted \(lbl_custName.text ?? "") at \(detailRecord.lastStatusUpdate?.toDouble.placeDetailDate ?? "")."
            btnRight.isHidden = true
            btnLeft.isHidden = true
            lbl_timer.isHidden = true
            
        default: // New
            lbl_queueStatus.text = "New"
            lbl_queueStatus.backgroundColor = #colorLiteral(red: 0.231372549, green: 0.2705882353, blue: 0.3098039216, alpha: 1)
            lbl_msg.text = "Acknowledge: \(lbl_custName.text ?? "") so they have an approximately time to wait."
            btnLeft.backgroundColor = #colorLiteral(red: 0.231372549, green: 0.2705882353, blue: 0.3098039216, alpha: 1)
            btnRight.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.1607843137, blue: 0, alpha: 1)
            btnLeft.setTitle("Acknowledge", for: .normal)
            btnRight.setTitle("Cancel", for: .normal)
        }
    }
    
    private func showData(_ isResponse: QueuePlaceModel) {
        btnLeft.titleEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        btnRight.titleEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        lbl_custName.text = "\(isResponse.data?.firstname ?? "") \(isResponse.data?.lastname ?? "")"
        lbl_queueName.text = isResponse.data?.queName
        lbl_dateTime.text = isResponse.data?.bookingTime?.toDouble.placeDetailDate
        var person = ""
        
        if (isResponse.data?.people?.toInt ?? 0) > 0 {
            person = "\(isResponse.data?.people ?? "") Persons"
        } else {
            person = "\(isResponse.data?.people ?? "") Person"
        }
        img_profile.kf.setImage(with: URL(string: isResponse.data?.userPic ?? ""), options: [.transition(.fade(0.5))])
        lblWaitingTime.text = "Waiting: \(Date().timePassed(from: isResponse.data?.bookingTime?.toDouble.dateFromTimestamp() ?? Date()))"
        lbl_places.text = "Place \((isResponse.data?.totalQueueNo?.toInt ?? 0) + 1) -  \(person)"
        lbl_descp.text = isResponse.data?.queName
        bookingStatus = BookingStatus(rawValue: (isResponse.data?.status!)!)
    
        lbl_queueStatus.backgroundColor = #colorLiteral(red: 0.231372549, green: 0.2705882353, blue: 0.3098039216, alpha: 1)
        lbl_timer.isHidden = true
        btnRight.isHidden = false
        self.view_timer.isHidden = false
        
        switch bookingStatus {
        case .Acknowledge:
            lbl_queueStatus.text = "Acknowledge"
            lbl_msg.text = "Call: \(lbl_custName.text ?? "") so they show-up."
            btnLeft.backgroundColor = #colorLiteral(red: 1, green: 0.7529411765, blue: 0, alpha: 1)
            btnRight.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.1607843137, blue: 0, alpha: 1)
            btnLeft.setTitle("Call", for: .normal)
            btnRight.setTitle("Cancel", for: .normal)
            startTimer(isResponse.data?.waitingTime, isResponse.data?.lastStatusUpdate)
            
        case .Update:
            lbl_queueStatus.text = "Ask Update"
            lbl_msg.text = "Call: \(lbl_custName.text ?? "") so they show-up."
            btnLeft.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.631372549, blue: 0.1019607843, alpha: 1)
            btnRight.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.1607843137, blue: 0, alpha: 1)
            btnLeft.setTitle("Update", for: .normal)
            btnRight.setTitle("Cancel", for: .normal)
            startTimer(isResponse.data?.waitingTime, isResponse.data?.lastStatusUpdate)
            
        case .Cancel:
            lbl_queueStatus.text = "Cancelled"
            lbl_queueStatus.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.1607843137, blue: 0, alpha: 1)
            lbl_msg.text = "Cancel: You have cancelled \(lbl_custName.text ?? "") place at \(isResponse.data?.lastStatusUpdate?.toDouble.placeDetailDate ?? "")."
            btnRight.isHidden = true
            btnLeft.isHidden = true
            lbl_timer.isHidden = true

        case .Noshow:
            lbl_queueStatus.text = "Noshow"
            lbl_queueStatus.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.1607843137, blue: 0, alpha: 1)
            lbl_msg.text = "Noshow: You have signal as Noshow to \(lbl_custName.text ?? "") place at \(isResponse.data?.lastStatusUpdate?.toDouble.placeDetailDate ?? "")."
            btnRight.isHidden = true
            btnLeft.isHidden = true
            lbl_timer.isHidden = true

        case .Call:
            lbl_queueStatus.text = "Call"
            lbl_queueStatus.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.631372549, blue: 0.1019607843, alpha: 1)
            lbl_msg.text = "You have called \(lbl_custName.text ?? "")"
            btnLeft.backgroundColor = #colorLiteral(red: 0.2, green: 0.7843137255, blue: 0.3176470588, alpha: 1)
            btnRight.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.1607843137, blue: 0, alpha: 1)
            btnLeft.setTitle("Accept", for: .normal)
            btnRight.setTitle("NoShow", for: .normal)
            lbl_timer.isHidden = false
            startTimer(isResponse.data?.callTimer, isResponse.data?.lastStatusUpdate)
            
        case .Accept:
            lbl_queueStatus.text = "Accepted"
            lbl_queueStatus.backgroundColor = #colorLiteral(red: 0.2, green: 0.7843137255, blue: 0.3176470588, alpha: 1)
            lbl_msg.text = "You have accepted \(lbl_custName.text ?? "") at \(isResponse.data?.lastStatusUpdate?.toDouble.placeDetailDate ?? "")."
            btnRight.isHidden = true
            btnLeft.isHidden = true
            lbl_timer.isHidden = true
            
        default: // New
            lbl_queueStatus.text = "New"
            lbl_queueStatus.backgroundColor = #colorLiteral(red: 0.231372549, green: 0.2705882353, blue: 0.3098039216, alpha: 1)
            lbl_msg.text = "Acknowledge: \(lbl_custName.text ?? "") so they have an approximately time to wait."
            btnLeft.backgroundColor = #colorLiteral(red: 0.231372549, green: 0.2705882353, blue: 0.3098039216, alpha: 1)
            btnRight.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.1607843137, blue: 0, alpha: 1)
            btnLeft.setTitle("Acknowledge", for: .normal)
            btnRight.setTitle("Cancel", for: .normal)
        }
    }
    
    private func confirmationAlert() {
        let alert = UIAlertController(title: "LaQueueu", message: "Do you want to proceed?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: { (_) in
            self.updateStatus(status: BookingStatus.Cancel.rawValue)
        }))
        alert.addAction(UIAlertAction(title: "No", style: .default, handler: { (_) in }))
        present(alert, animated: true, completion: nil)
    }
    
    private func checkForMainContainerAndPopVC() {
        let vc = (RetailerStoryBoard.Authenticate(controller: Controller.MainContainer) as? MainContainerVC)!
        if !(self.navigationController!.viewControllers.contains(vc)) {
            var vcs = navigationController?.viewControllers
            vcs![0] = vc
            navigationController?.viewControllers = vcs!
            navigationController?.popViewController(animated: true)
        } else {
            navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func btn_profileTap(_ sender: UIButton) {
        self.checkForMainContainerAndPopVC()
    }
    
    @IBAction func btn_notiTap(_ sender: UIButton) {
    }
    
    @IBAction func btn_bellTap(_ sender: UIButton) {
    }
    
    @IBAction func btn_rightTap(_ sender: UIButton) {
        switch bookingStatus {
        case .Acknowledge, .Update:
            confirmationAlert()
        case .Call:
            updateStatus(status: BookingStatus.Noshow.rawValue)
        default: // New
            confirmationAlert()
        }
    }
    
    private func acknowledgeAction() {
        guard let nc = RetailerStoryBoard.Main(controller: Controller.Acknowledge) as? AcknowledgeVC else {return}
        nc.btnTime = { (time) in
            self.waitingTime = time
            self.updateStatus(status: BookingStatus.Acknowledge.rawValue)
        }
        nc.modalPresentationStyle = .overCurrentContext
        self.present(nc, animated: true, completion: nil)
    }
    
    private func updateAction() {
        guard let nc = RetailerStoryBoard.Main(controller: Controller.Acknowledge) as? AcknowledgeVC else {return}
        nc.btnTime = { (time) in
            self.waitingTime = time
            self.updateStatus(status: BookingStatus.Update.rawValue)
        }
        nc.modalPresentationStyle = .overCurrentContext
        self.present(nc, animated: true, completion: nil)
    }
    
    @IBAction func btn_leftTap(_ sender: UIButton) {
        switch bookingStatus {
        case .Acknowledge:
            updateStatus(status: BookingStatus.Call.rawValue)
        case .Update:
            updateAction()
        case .Call:
            updateStatus(status: BookingStatus.Accept.rawValue)
        default: // New
            acknowledgeAction()
        }
    }
}

// MARK:- API Calls
extension PlaceDetailsVC {
    
    func getPlaceData() {
        var authorization = ""
        if let auth = LoginSession.shared.getUserModelDefault(keyName: userData)?.data?.login?.authorization {
            authorization = auth
        }
        let header:[String:String] = [
            "Authorization":authorization,
            "device_type":"ios",
            "device_id":"111",
            "current_time_zone":"",
            "language":"en",
            "Content-Type":"application/json",
            "version":"1.0.0",
            "current_country":"India",
            "lat":"22.12541",
            "lng":"22.12541"
        ]
        let param = [
            "booking_id":bookingId
        ]
        Loader.shared.showIndicator(view: self.view)
        K3Webservice.run(urlString: WebUrl.placeInQueue, method: HTTPMethod.post, Param: param, header: header , type: QueuePlaceModel.self, viewController: self) { (response) in
            if let isResponse = response {
                if isResponse.status == 1 {
                    Loader.shared.hideIndicator()
                    self.showData(isResponse)
                } else {
                    Loader.shared.hideIndicator()
                    self.func_Alert(message: isResponse.message ?? "")
                }
            }
        }
    }
    
    func updateStatus(status:String) {
        var authorization = ""
        if let auth = LoginSession.shared.getUserModelDefault(keyName: userData)?.data?.login?.authorization {
            authorization = auth
        }
        let header:[String:String] = [
            "Authorization":authorization,
            "device_type":"ios",
            "device_id":"111",
            "current_time_zone":TimeZone.current.identifier,
            "language":"en",
            "Content-Type":"application/json",
            "version":"1.0.0",
            "current_country":"India",
            "lat":"22.12541",
            "lng":"22.12541"
        ]
        
        var id = bookingId
        if detailRecord != nil {
            id = detailRecord.bookingID!
        }
        let param:[String:Any] = [
            "booking_id":id,
            "status":status,
            "waiting_time": waitingTime ?? "",
            "last_status_update":String(UInt64(Date().timeIntervalSince1970))
        ]
        Loader.shared.showIndicator(view: self.view)
        K3Webservice.run(urlString: WebUrl.bookingStatus, method: HTTPMethod.post, Param: param, header: header , type: ForgotPasswordModel.self, viewController: self) { (response) in
            if let isResponse = response {
                if isResponse.status == 1 {
                    if status == "Update" {
                        self.updateStatus(status: BookingStatus.Acknowledge.rawValue)
                        return
                    }
                    Loader.shared.hideIndicator()
                    self.didUpdateQueueList?()
                    self.func_AlertWithOneOption(message: isResponse.message ?? "", optionOne: "OK") {
                        self.navigationController?.popViewController(animated: true)
                    }
                } else {
                    Loader.shared.hideIndicator()
                    self.func_Alert(message: isResponse.message ?? "")
                }
            }
        }
    }
}
