//
//  StaticContentVC.swift
//  LaqueueRetailer
//
//  Created by Adityaraj Singh Gaharwar on 27/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import WebKit
import Alamofire

class StaticContentVC: UIViewController, WKNavigationDelegate {

    @IBOutlet weak var lbl_headerTitle: UILabel!
    @IBOutlet weak var webView_Static: WKWebView!
    
    var headerTitle = ""
    var urlToLoad = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lbl_headerTitle.text = headerTitle
        loadStaticPage()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btn_backTap(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension StaticContentVC {
    func openUrlInWKWebview(url: String) {
        Loader.shared.showIndicator(view: self.view)
        let url = URL(string: url)
        let request = URLRequest(url: url!)
        webView_Static.navigationDelegate = self
        webView_Static.load(request)
        Loader.shared.hideIndicator()
    }
    
    func loadStaticPage() {
        let header:[String:String] = [
            "device_type":"ios",
            "device_id":"111",
            "current_time_zone":TimeZone.current.identifier,
            "language":"en",
            "Content-Type":"application/json",
            "version":"1.0.0",
            "current_country":"India",
            "lat":"22.12541",
            "lng":"22.12541"
        ]
        Loader.shared.showIndicator(view: self.view)
        K3Webservice.run(urlString: urlToLoad, method: HTTPMethod.get, Param: nil, header: header , type: StaticContentModel.self, viewController: self) { (response) in
            if let isResponse = response {
                if isResponse.status == 1 {
                    Loader.shared.hideIndicator()
                    self.openUrlInWKWebview(url: isResponse.data?.url ?? "")
                } else {
                    Loader.shared.hideIndicator()
                    self.func_Alert(message: isResponse.message ?? "")
                }
            }
        }
    }
}
