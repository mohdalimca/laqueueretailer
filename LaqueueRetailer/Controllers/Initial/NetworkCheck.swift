//
//  NetworkCheck.swift
//  LaqueueRetailer
//
//  Created by Adityaraj Singh Gaharwar on 27/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class NetworkCheck: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //checking whether the user is logged in
        if LoginSession.shared.checkLoginStatus() {
            // Go to home user is logged in
            self.navigationController?.pushViewController(RetailerStoryBoard.Authenticate(controller: Controller.MainContainer), animated: true)
        } else {
            // Go to Login screen user is not logged in
            let nc = RootController.init(rootViewController: RetailerStoryBoard.Authenticate(controller: Controller.Login))
            nc.setNavigationBarHidden(true, animated: false)
            if #available(iOS 13.0, *) {
                nc.isModalInPresentation = true
                nc.modalPresentationStyle = .fullScreen
            }
            self.navigationController?.present(nc, animated: true, completion: nil)
        }
    }
    
}
