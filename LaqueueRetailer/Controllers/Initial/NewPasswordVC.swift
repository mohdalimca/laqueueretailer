//
//  NewPasswordVC.swift
//  LaqueueRetailer
//
//  Created by Adityaraj Singh Gaharwar on 27/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire

class NewPasswordVC: UIViewController {

    @IBOutlet weak var txt_password: UITextField!
    @IBOutlet weak var txt_confirmPassword: UITextField!
    
    var userid = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    // MARK:- Button Actions
    @IBAction func btn_resetTap(_ sender: UIButton) {
        if validateInputs() {
            setNewPassword()
        }
    }
    
    @IBAction func btn_cancelTap(_ sender: UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func btnEyeNewAction(_ sender: UIButton) {
        sender.isSelected = (!sender.isSelected) ? true : false
        txt_password.isSecureTextEntry = !sender.isSelected
    }
    
    @IBAction func btnEyeConfirmAction(_ sender: UIButton) {
        sender.isSelected = (!sender.isSelected) ? true : false
        txt_confirmPassword.isSecureTextEntry = !sender.isSelected
    }
}

extension NewPasswordVC: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.becomeFirstResponder()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == " " {
            return false
        }
        return true
    }
}

extension NewPasswordVC {
    
    func validateInputs() -> Bool {
        var msg = ""
        if txt_password.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            msg = "Please enter new password"
        } else if txt_confirmPassword.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            msg = "Please enter confirm password"
        } else if txt_confirmPassword.text != txt_password.text {
            msg = "Passwords doesn't match"
        }
        if msg == "" {
            return true
        } else {
            self.func_Alert(message: msg)
            return false
        }
    }
    
    func setNewPassword() {
        let header:[String:String] = [
            "device_type":"ios",
            "device_id":"111",
            "current_time_zone":TimeZone.current.identifier,
            "language":"en",
            "Content-Type":"application/json",
            "version":"1.0.0",
            "current_country":"India",
            "lat":"22.12541",
            "lng":"22.12541"
        ]
        let param:[String:Any] = [
            "user_id": userid,
            "new_password": txt_confirmPassword.text ?? ""
        ]
        Loader.shared.showIndicator(view: self.view)
        K3Webservice.run(urlString: WebUrl.newPassword, method: HTTPMethod.post, Param: param, header: header , type: NewPasswordModel.self, viewController: self) { (response) in
            if let isResponse = response {
                if isResponse.status == 1 {
                    Loader.shared.hideIndicator()
                    self.func_AlertWithOneOption(message: isResponse.message ?? "", optionOne: "Ok") {
                        self.navigationController?.popToRootViewController(animated: true)
                    }
                } else {
                    Loader.shared.hideIndicator()
                    self.func_Alert(message: isResponse.message ?? "")
                }
            }
        }
    }
}
