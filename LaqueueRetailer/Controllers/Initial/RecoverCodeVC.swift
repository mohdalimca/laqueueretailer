//
//  RecoverCodeVC.swift
//  LaqueueRetailer
//
//  Created by Adityaraj Singh Gaharwar on 27/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire

class RecoverCodeVC: UIViewController {

    @IBOutlet weak var lblTimer: UILabel!
    @IBOutlet weak var txt_code: UITextField!
    @IBOutlet weak var btn_resend: UIButton!
    @IBOutlet weak var btn_recover: UIButton!
    
    var isOtpVerify = false
    var userId = ""
    var seconds = 30
    var timer:Timer?
    var isTimerRunning = false
    let inactiveImage = UIImage(named: "cancelBorder")
    let activeImage = UIImage(named: "btn")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    private func setup() {
        runTimer()
        if isOtpVerify {
            btn_recover.setTitle("Verify", for: .normal)
        }
    }

    @IBAction func btn_resendTap(_ sender: UIButton) {
        resendOTP()
    }
    
    @IBAction func btn_recoverTap(_ sender: UIButton) {
        if validateInputs() {
            verifyUserEmail()
        }
    }
    
    @IBAction func btn_cancelTap(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btn_backTap(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension RecoverCodeVC {
    
    private func runTimer() {
         timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(self.updateTimer)), userInfo: nil, repeats: true)
        btn_resend.isUserInteractionEnabled = false
        DispatchQueue.main.async {
            self.btn_resend.setBackgroundImage(self.inactiveImage, for: .normal)
        }
    }
    
    @objc func updateTimer() {
        seconds -= 1     //This will decrement(count down)the seconds.
        lblTimer.text = "\(seconds)" //This will update the label.
        if seconds <= 0 {
            lblTimer.text = "--"
            timer?.invalidate()
            timer = nil
            seconds = 30
            btn_resend.isUserInteractionEnabled = true
            DispatchQueue.main.async {
                self.btn_resend.setBackgroundImage(self.activeImage, for: .normal)
            }
        }
    }
    
    func validateInputs() -> Bool {
        var msg = ""
        if txt_code.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            msg = "Please enter OTP code"
        }
        if msg == "" {
            return true
        } else {
            self.func_Alert(message: msg)
            return false
        }
    }
    
    func resendOTP() {
            let header:[String:String] = [
                "device_type":"ios",
                "device_id":"111",
                "current_time_zone":"",
                "language":"en",
                "Content-Type":"application/json",
                "version":"1.0.0",
                "current_country":"India",
                "lat":"22.12541",
                "lng":"22.12541"
            ]
            let param:[String:Any] = [
                "user_id": userId
            ]
            Loader.shared.showIndicator(view: self.view)
            K3Webservice.run(urlString: WebUrl.resendOTP, method: HTTPMethod.post, Param: param, header: header , type: ResendOTPModel.self, viewController: self) { (response) in
                if let isResponse = response {
                    if isResponse.status == 1 {
                        Loader.shared.hideIndicator()
    //                    self.func_Alert(message: "\(isResponse.message ?? ""). OTP is \(isResponse.data?.resendOtp?.otp ?? "")")
                        self.func_AlertWithOneOption(message: "\(isResponse.message ?? ""). OTP is \(isResponse.data?.resendOtp?.otp ?? "")", optionOne: "Ok") {
                            self.runTimer()
                        }
                    } else {
                        Loader.shared.hideIndicator()
                        self.func_Alert(message: isResponse.message ?? "")
                    }
                }
            }
        }
    
    func verifyUserEmail() {
        let header:[String:String] = [
            "device_type":"ios",
            "device_id":"111",
            "current_time_zone":TimeZone.current.identifier,
            "language":"en",
            "Content-Type":"application/json",
            "version":"1.0.0",
            "current_country":"India",
            "lat":"22.12541",
            "lng":"22.12541"
        ]
        let param:[String:Any] = [
            "user_id": userId,
            "otp": txt_code.text ?? ""
        ]
        Loader.shared.showIndicator(view: self.view)
        K3Webservice.run(urlString: WebUrl.emailVerify, method: HTTPMethod.post, Param: param, header: header , type: EmailVerifyModel.self, viewController: self) { (response) in
            if let isResponse = response {
                if isResponse.status == 1 {
                    Loader.shared.hideIndicator()
                    self.func_AlertWithOneOption(message: isResponse.message ?? "", optionOne: "Ok") {
                        if self.isOtpVerify {
                            self.navigationController?.popToRootViewController(animated: true)
                        } else {
                            guard let vc = RetailerStoryBoard.Authenticate(controller: Controller.NewPassword) as? NewPasswordVC else {return}
                            vc.userid = isResponse.data?.otp?.userID ?? ""
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }
                } else {
                    Loader.shared.hideIndicator()
                    self.func_Alert(message: isResponse.message ?? "")
                }
            }
        }
    }
}
