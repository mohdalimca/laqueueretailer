//
//  SignUpVC.swift
//  LaqueueRetailer
//
//  Created by Adityaraj Singh Gaharwar on 27/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire
import CoreLocation

class SignUpVC: UIViewController {
    
    @IBOutlet weak var txt_email: UITextField!
    @IBOutlet weak var txt_password: UITextField!
    @IBOutlet weak var txt_businessName: UITextField!
    @IBOutlet weak var txt_businessPhone: UITextField!
    @IBOutlet weak var txt_address: UITextView!
    @IBOutlet weak var txt_dialCode: UITextField!
    
    var isCheckedCount = 0
    var jsonArray: [Any] = []
    var arrCountry: [String] = []
    var countryList = CountryList()
    var selectedCountry: Country!
    var currentLocation:CLLocation!
    var placemark:CLPlacemark!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    private func setup() {
        txt_dialCode.delegate = self
        txt_address.delegate = self
        selectedCountry = Country(countryCode: "FR", phoneExtension: "33")
        txt_dialCode.text = countryCodeText()
        countryList.delegate = self
        loadCountryJsonList()
        getCurrentLocation()
        getAddressFromCurrentLocation()
    }
    
    
    private func getCurrentLocation() {
        LocationManager.shared.getLocation { (location, error) in
            if error != nil {
                print(error?.localizedDescription ?? "")
                return
            }
            guard let location = location else { return }
            self.currentLocation = location

            print("Latitude: \(self.currentLocation.coordinate.latitude) Longitude: \(self.currentLocation.coordinate.longitude)")
        }
    }

    private func getAddressFromCurrentLocation() {

       LocationManager.shared.getCurrentReverseGeoCodedLocation { (location:CLLocation?, placemark:CLPlacemark?, error:NSError?) in

           if error != nil {
               print(error?.localizedDescription ?? "")
               return
           }
           guard let location = location, let placemark = placemark else {
               return
           }
           //We get the complete placemark and can fetch anything from CLPlacemark
        self.currentLocation = location
        self.placemark = placemark
        let address = "\(placemark.subThoroughfare ?? ""), \(placemark.thoroughfare ?? ""), \(placemark.locality ?? ""), \(placemark.subLocality ?? ""), \(placemark.administrativeArea ?? ""), \(placemark.postalCode ?? ""), \(placemark.country ?? "")"
        print(address)
        self.txt_address.text = address
       }
    }
    
    
    // MARK:- Button Actions
    @IBAction func btnMapAction(_ sender: UIButton) {
        OODLocationPicker.location.function_ShowPicker(self) { (isLoc, place) in
            if place != nil {
                print("picked address is \(place?.formattedAddress ?? "")")
                print("picked address coordinate \(String(describing: place?.coordinate))")
                self.currentLocation = CLLocation(latitude: place?.coordinate.latitude ?? 0, longitude: place?.coordinate.longitude ?? 0)
                self.txt_address.text = place?.formattedAddress
                self.txt_address.resignFirstResponder()
                self.view.endEditing(true)
            }
        }
    }
    
    @IBAction func btn_checkbox(_ sender: UIButton) {
        isCheckedCount += 1
        if isCheckedCount%2 == 0 {
            sender.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
        } else {
            sender.setImage(#imageLiteral(resourceName: "check"), for: .normal)
        }
    }
    
    @IBAction func btn_tncTap(_ sender: UIButton) {
        guard let vc = RetailerStoryBoard.Authenticate(controller: Controller.StaticContent) as? StaticContentVC else {return}
        vc.urlToLoad = WebUrl.terms
        vc.headerTitle = "Terms & Conditions"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btn_privacyTap(_ sender: UIButton) {
        guard let vc = RetailerStoryBoard.Authenticate(controller: Controller.StaticContent) as? StaticContentVC else {return}
        vc.urlToLoad = WebUrl.privacy
        vc.headerTitle = "Privacy Policy"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btn_signUpTap(_ sender: UIButton) {
        if validateInputs() {
            registerUsers()
        }
    }
    
    @IBAction func btn_loginTap(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btn_backTap(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnEyeAction(_ sender: UIButton) {
        sender.isSelected = (!sender.isSelected) ? true : false
        txt_password.isSecureTextEntry = !sender.isSelected
    }
}

extension SignUpVC: UITextFieldDelegate, UITextViewDelegate, CountryListDelegate {
    func selectedCountry(country: Country) {
        selectedCountry = country
        txt_dialCode.text = countryCodeText()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        present(countryList, animated: true, completion: nil)
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.becomeFirstResponder()
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        self.txt_address.resignFirstResponder()
    }
    
    private func loadCountryJsonList() {
        guard let path = Bundle.main.path(forResource: "country_list", ofType:"json") else {
            debugPrint( "path not found")
            return
        }
        do{
            let data = try Data(contentsOf: URL(fileURLWithPath: path))
            let json = try JSONSerialization.jsonObject(with: data, options: [])
            if let object = json as? [String : Any]  {
                print(object)
            }
        } catch {
            print(error.localizedDescription)
        }
    }
    
    private func countryCodeText() -> String {
        var strCountry = ""
        strCountry.append(selectedCountry.flag! + " ")
        strCountry.append(selectedCountry.countryCode + " +")
        strCountry.append(selectedCountry.phoneExtension)
        return strCountry
    }
    func validateInputs() -> Bool {
        var msg = ""
        if txt_email.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            msg = "Please enter an email address"
        } else if self.validate(YourEMailAddress: txt_email.text!) == false {
            msg = "Please enter a valid email address"
        } else if txt_password.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            msg = "Please enter your password"
        } else if txt_businessName.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            msg = "Please enter your business name"
        } else if txt_dialCode.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            msg = "Please enter your dial code"
        } else if txt_businessPhone.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            msg = "Please enter your business phone number"
        } else if txt_address.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            msg = "Please enter your business address"
        } else if isCheckedCount%2 == 0 {
            msg = "Please check Terms & Conditions and Privacy Policy"
        } else if currentLocation == nil {
            msg = "Please enable location from settings in order to register user. Unable to get user's location."
        }
        if msg == "" {
            return true
        } else {
            self.func_Alert(message: msg)
            return false
        }
    }
    
    func registerUsers() {
        let header:[String:String] = [
            "device_type":"ios",
            "device_id":"111",
            "current_time_zone":TimeZone.current.identifier,
            "language":"en",
            "Content-Type":"application/json",
            "version":"1.0.0",
            "current_country":"India",
            "lat":"22.12541",
            "lng":"22.12541",
        ]
        
        let param:[String:Any] = [
            "email": txt_email.text ?? "",
            "password": txt_password.text ?? "",
            "type": "1",
            "business_name":txt_businessName.text ?? "",
            "address":txt_address.text ?? "",
            "lat":"\(currentLocation.coordinate.latitude)",
            "lng":"\(currentLocation.coordinate.longitude)",
//            "country":"India",
            "country":placemark.country ?? "",
//            "city":"Indore",
            "city":placemark.subAdministrativeArea ?? "",
            "country_code":selectedCountry.phoneExtension,
            "user_phone":txt_businessPhone.text ?? ""
        ]
        Loader.shared.showIndicator(view: self.view)
        K3Webservice.run(urlString: WebUrl.registration, method: HTTPMethod.post, Param: param, header: header , type: SignUpModel.self, viewController: self) { (response) in
            if let isResponse = response {
                if isResponse.status == 1 {
                    Loader.shared.hideIndicator()
                    self.func_AlertWithOneOption(message: "\(isResponse.message ?? "") OTP is \(isResponse.data?.register?.otp ?? "")", optionOne: "Ok") {
                        guard let vc = RetailerStoryBoard.Authenticate(controller: Controller.RecoverCode) as? RecoverCodeVC else { return }
                        vc.isOtpVerify = true
                        vc.userId = isResponse.data?.register?.userID ?? ""
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                } else {
                    Loader.shared.hideIndicator()
                    self.func_Alert(message: isResponse.message ?? "")
                }
            }
        }
    }
}
