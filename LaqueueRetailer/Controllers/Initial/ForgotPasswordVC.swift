//
//  ForgotPasswordVC.swift
//  LaqueueRetailer
//
//  Created by Adityaraj Singh Gaharwar on 27/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire

class ForgotPasswordVC: UIViewController {

    @IBOutlet weak var txt_email: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func btn_backTap(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btn_recoverTap(_ sender: UIButton) {
        if validateInputs() {
            getRecoverCodeOnEmail()
        }
    }
    
    @IBAction func btn_cancelTap(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension ForgotPasswordVC {
    
    func validateInputs() -> Bool {
        var msg = ""
        if txt_email.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            msg = "Please enter an email address"
        } else if self.validate(YourEMailAddress: txt_email.text!) == false {
            msg = "Please enter valid email address"
        }
        if msg == "" {
            return true
        } else {
            self.func_Alert(message: msg)
            return false
        }
    }
    
    func getRecoverCodeOnEmail() {
        let header:[String:String] = [
            "device_type":"ios",
            "device_id":"111",
            "current_time_zone":TimeZone.current.identifier,
            "language":"en",
            "Content-Type":"application/json",
            "version":"1.0.0",
            "current_country":"India",
            "lat":"22.12541",
            "lng":"22.12541"
        ]
        let param:[String:Any] = [
            "email": txt_email.text ?? ""
        ]
        Loader.shared.showIndicator(view: self.view)
        K3Webservice.run(urlString: WebUrl.forgotPassword, method: HTTPMethod.post, Param: param, header: header , type: ForgotPasswordModel.self, viewController: self) { (response) in
            if let isResponse = response {
                if isResponse.status == 1 {
                    Loader.shared.hideIndicator()
                    self.func_AlertWithOneOption(message: "\(isResponse.message ?? "") OTP is \(isResponse.data?.forgot?.otp ?? "") ", optionOne: "Ok") {
                        guard let vc = RetailerStoryBoard.Authenticate(controller: Controller.RecoverCode) as? RecoverCodeVC else { return }
                        vc.userId = isResponse.data?.forgot?.userID ?? ""
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                } else {
                    Loader.shared.hideIndicator()
                    self.func_Alert(message: isResponse.message ?? "")
                }
            }
        }
    }
}
