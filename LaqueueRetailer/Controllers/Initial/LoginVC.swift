//
//  LoginVC.swift
//  LaqueueRetailer
//
//  Created by Adityaraj Singh Gaharwar on 27/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire

class LoginVC: UIViewController {
    
    @IBOutlet weak var txt_email: UITextField!
    @IBOutlet weak var txt_password: UITextField!
    @IBOutlet weak var viewSplash: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    private func setup() {
        self.checkUserLogin()
        [txt_email, txt_password].forEach {
            $0?.autocorrectionType = .no
            $0?.delegate = self
        }
        
        LocationManager.shared.getLocation { (location, error) in
            if error != nil {
                print(error?.localizedDescription ?? "")
                return
            }
            guard let location = location else {
                return
            }
            print("Latitude: \(location.coordinate.latitude) Longitude: \(location.coordinate.longitude)")
        }
    }
    
    private func checkUserLogin() {
        //checking whether the user is logged in
        if LoginSession.shared.checkLoginStatus() {
            // Go to home user is logged in
            if UserDefaults.standard.value(forKey: "profileStatus") as? Int == 0 {
                //                guard let vc = RetailerStoryBoard.Profile(controller: Controller.MyProfile) as? MyProfileVC else {return}
                //                vc.isFromLogin = true
                //                vc.userPic = LoginSession.shared.getUserModelDefault(keyName: userData)?.data?.login?.userPic ?? ""
                //                vc.email = LoginSession.shared.getUserModelDefault(keyName: userData)?.data?.login?.userEmail ?? ""
                //                vc.name = "\(LoginSession.shared.getUserModelDefault(keyName: userData)?.data?.login?.firstname ?? "") \(LoginSession.shared.getUserModelDefault(keyName: userData)?.data?.login?.lastname ?? "")"
                //                vc.userRole = LoginSession.shared.getUserModelDefault(keyName: userData)?.data?.login?.userRole ?? ""
                //                self.navigationController?.pushViewController(vc, animated: true)
                self.navigationController?.pushViewController(RetailerStoryBoard.Authenticate(controller: Controller.MainContainer), animated: true)
            } else {
                self.navigationController?.pushViewController(RetailerStoryBoard.Authenticate(controller: Controller.MainContainer), animated: true)
            }
        } else {
            
        }
    }
    
    // MARK:- Button Actions
    @IBAction func btnEyeAction(_ sender: UIButton) {
        sender.isSelected = (!sender.isSelected) ? true : false
        txt_password.isSecureTextEntry = !sender.isSelected
    }
    
    @IBAction func btn_backTap(_ sender: UIButton) {
    }
    
    @IBAction func btn_forgotPasswordTap(_ sender: UIButton) {
        endEditing()
        self.navigationController?.pushViewController(RetailerStoryBoard.Authenticate(controller: Controller.ForgotPassword), animated: true)
    }
    
    @IBAction func btn_loginTap(_ sender: UIButton) {
        endEditing()
        if validateInputs() {
            loginUser()
        }
    }
    
    @IBAction func btn_signUpTap(_ sender: UIButton) {
        endEditing()
        self.navigationController?.pushViewController(RetailerStoryBoard.Authenticate(controller: Controller.SignUp), animated: true)
    }
    
    private func setProfileVisibility(_ response: Login?) {
        UserStore.save(role: (response?.userRole)!)
        UserStore.save(userID: response?.userid)
        UserStore.save(businessCode: response?.busiCode)
        UserStore.save(businessId: response?.busid)
        
        if response?.firstname != "" && response?.lastname != "" && response?.userPic != "" {
            UserStore.save(username: (response?.firstname ?? "") + (response?.lastname ?? ""))
            UserStore.save(ownerDetail: 1)
        } else {
            UserStore.save(noQueue: 0)
            UserStore.save(ownerDetail: 0)
            UserStore.save(retailDetail: 0)
            UserStore.save(profileStatus: 0)
        }
    }
}

extension LoginVC {
    func validateInputs() -> Bool {
        var msg = ""
        if txt_email.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            msg = "Please enter an email address"
        } else if self.validate(YourEMailAddress: txt_email.text!) == false {
            msg = "Please enter a valid email address"
        } else if txt_password.text?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            msg = "Please enter your password"
        }
        if msg == "" {
            return true
        } else {
            self.func_Alert(message: msg)
            return false
        }
    }
}

extension LoginVC: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.becomeFirstResponder()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == " " {
            return false
        }
        return true
    }
}

// MARK:- API Call
extension LoginVC {
    func loginUser() {
        let header:[String:String] = [
            "device_type":"ios",
            "device_id":UserStore.fcmtoken ?? "",
            "current_time_zone":TimeZone.current.identifier,
            "language":"en",
            "Content-Type":"application/json",
            "version":"1.0.0",
            "current_country":"India",
            "lat":"22.12541",
            "lng":"22.12541"
        ]
        let param:[String:Any] = [
            "email": txt_email.text ?? "",
            "password": txt_password.text ?? ""
        ]
        Loader.shared.showIndicator(view: self.view)
        K3Webservice.run(urlString: WebUrl.login, method: HTTPMethod.post, Param: param, header: header , type: LoginModel.self, viewController: self) { (response) in
            if let isResponse = response {
                if isResponse.status == 1 {
                    Loader.shared.hideIndicator()
                    self.setProfileVisibility(isResponse.data?.login)
                    UserStore.save(authorization: isResponse.data?.login?.authorization)
                    UserStore.save(userPic: isResponse.data?.login?.userPic)
                    self.getProfileDetails()
                    self.func_AlertWithOneOption(message: isResponse.message ?? "", optionOne: "OK") {
                        if isResponse.data?.login?.otp != nil {
                            guard let vc = RetailerStoryBoard.Authenticate(controller: Controller.RecoverCode) as? RecoverCodeVC else { return }
                            vc.isOtpVerify = true
                            vc.userId = isResponse.data?.login?.userid ?? ""
                            self.navigationController?.pushViewController(vc, animated: true)
                        } else {
                            LoginSession.shared.updateLoginStatus(true)
                            LoginSession.shared.saveUserDefault(userModel: isResponse, keyName: userData)
                            self.navigationController?.pushViewController(RetailerStoryBoard.Authenticate(controller: Controller.MainContainer), animated: true)
                        }
                    }
                } else {
                    Loader.shared.hideIndicator()
                    self.func_Alert(message: isResponse.message ?? "")
                }
            }
        }
    }
    
    func getProfileDetails() {
        let header:[String:String] = [
            "Authorization":UserStore.authorization ?? "",
            "device_type":"ios",
            "device_id":UserStore.pushToken ?? "",
            "current_time_zone":TimeZone.current.identifier,
            "language":"en",
            "Content-Type":"application/json",
            "version":"1.0.0",
            "current_country":"India",
            "lat":"22.12541",
            "lng":"22.12541"
        ]
        K3Webservice.run(urlString: WebUrl.userProfile, method: HTTPMethod.get, Param: nil, header: header , type: ProfileModel.self, viewController: self) { (response) in
            if let isResponse = response {
                if isResponse.status == 1 {
                    
                    UserStore.save(retailDetail: 0)
                    UserStore.save(profileStatus: 0)
                    // set retailer visible details
                    if (isResponse.data?.profile?.busiDescription != nil &&
                        isResponse.data?.profile?.busiDescription != "") &&
                        (isResponse.data?.profile?.categoryName != nil &&
                            isResponse.data?.profile?.categoryName != "")  {
                        UserStore.save(retailDetail: 1)
                    }
                    
                    if (isResponse.data?.profile?.firstname != nil &&
                        isResponse.data?.profile?.firstname != "") &&
                        (isResponse.data?.profile?.lastname != nil &&
                            isResponse.data?.profile?.lastname != "")   {
                        UserStore.save(profileStatus: 1)
                    }
                }
            }
        }
    }
}
