//
//  RetailerStoryBoard.swift
//  LaqueueRetailer
//
//  Created by Adityaraj Singh Gaharwar on 27/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation
import AVKit

class RetailerStoryBoard {
    class func Authenticate(controller: String) -> UIViewController {
        return UIStoryboard.init(name: "Authenticate", bundle: Bundle.main).instantiateViewController(withIdentifier: controller)
    }
    
    class func Main(controller: String) -> UIViewController {
        return UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: controller)
    }
    
    class func Profile(controller: String) -> UIViewController {
        return UIStoryboard.init(name: "Profile", bundle: Bundle.main).instantiateViewController(withIdentifier: controller)
    }
}
